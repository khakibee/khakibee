package logger

import (
	"net/http"
	"os"

	"github.com/labstack/echo/v4"
	gommonLog "github.com/labstack/gommon/log"
	"github.com/neko-neko/echo-logrus/log"
	"github.com/sirupsen/logrus"
)

//Logger logger
var Logger *logrus.Logger

//EchoLogger logger
var EchoLogger *log.MyLogger

func init() {
	// Logger
	log.Logger().SetOutput(os.Stdout)
	log.Logger().SetLevel(gommonLog.INFO)
	log.Logger().SetFormatter(&logrus.TextFormatter{
		FullTimestamp:          true,
		DisableSorting:         true,
		QuoteEmptyFields:       true,
		DisableLevelTruncation: true,
	})
	EchoLogger = log.Logger()

	Logger = logrus.New()

	Logger.SetLevel(logrus.InfoLevel)
	Logger.SetFormatter(&logrus.TextFormatter{
		FullTimestamp:          true,
		DisableSorting:         true,
		QuoteEmptyFields:       true,
		DisableLevelTruncation: true,
	})
}

//EchoHTTPError logging error and creates a new echo HTTPError instance
func EchoHTTPError(httpStatusCode int, errMsg string, fields ...logrus.Fields) *echo.HTTPError {

	// Print specific error message
	tempLog := Logger.WithFields(fields[0])

	if httpStatusCode >= http.StatusInternalServerError {
		//In case of Internal error override response error message
		tempLog.Error(errMsg)
		errMsg = "Internal server error"
	} else {
		tempLog.Warning(errMsg)
	}

	return echo.NewHTTPError(httpStatusCode, errMsg)
}
