package misc

import (
	"fmt"
	"strings"
)

// NullString return *string's value or literal string "NULL"
type NullString struct {
	Value *string
}

// NullStringEsc return *string's pqsl escaped value or literal string "NULL"
type NullStringEsc struct {
	Value *string
}

// NullInt return *int's value as a string or literal string "NULL"
type NullInt struct {
	Value *int
}

// NullFloat64 return *float64's value as a string or literal string "NULL"
type NullFloat64 struct {
	Value *float64
}

const sqlNull = "NULL"
const printQuetedString = "'%s'"
const printQuetedStringEsc = "E'%s'"
const printDigit = "%d"
const printFloat = "%.2f"

func (os NullString) String() string {
	if os.Value == nil {
		return sqlNull
	}

	return fmt.Sprintf(printQuetedString, *os.Value)
}

func (os NullStringEsc) String() string {
	if os.Value == nil {
		return sqlNull
	}

	valueEsc := strings.Replace(*os.Value, "'", "\\'", -1)

	return fmt.Sprintf(printQuetedStringEsc, valueEsc)
}

func (os NullInt) String() string {
	if os.Value == nil {
		return sqlNull
	}

	return fmt.Sprintf(printDigit, *os.Value)
}

func (os NullFloat64) String() string {
	if os.Value == nil {
		return sqlNull
	}

	return fmt.Sprintf(printFloat, *os.Value)
}
