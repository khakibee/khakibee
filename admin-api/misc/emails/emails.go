package emails

import (
	"bytes"
	"database/sql"
	"fmt"
	"html/template"
	"khakibee/admin-api/config"
	"khakibee/admin-api/misc"
	"khakibee/admin-api/misc/logger"
	"net/smtp"
	"strings"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

// EmailBookingDetails model
type EmailBookingDetails struct {
	Name     string
	Email    string
	Phone    string
	Date     string
	Players  int
	Helps    string
	TeamName string
	LearnUs  string
	RoomName string
	Level    string
	Notes    string
	Age      string

	PlayRoom1 string
	EscRoom1  string
	RoomID    int
	Lang      string

	Src string
}

// Email model
type Email struct {
	from    string
	to      []string
	subject string
	body    string
	auth    smtp.Auth
}

var log = logger.Logger

// NewEmail creates new request
func NewEmail(from string, to []string, subject, body string, auth smtp.Auth) *Email {
	return &Email{
		from:    from,
		to:      to,
		subject: subject,
		body:    body,
		auth:    auth,
	}
}

// ParseTemplate parses template
func ParseTemplate(templateFileName string, bkngData interface{}) (html string, err error) {

	t := template.New("html")
	t, err = t.Parse(templateFileName)
	if err != nil {
		log.WithField("Parse emails template error:", err).Info()
		return
	}
	buf := new(bytes.Buffer)
	err = t.Execute(buf, bkngData)
	html = buf.String()
	return
}

func SendEmail(db *sql.DB, emailData EmailBookingDetails, emailType string, receiver string, lang string) error {
	cfg, err := config.NewConfig("./")
	if err != nil {
		log.Fatalf("error opening config file: %v\n", err)
	}

	var from = cfg.Email.Username
	var pass = cfg.Email.Password
	var host = cfg.Email.Host

	var sendTo = []string{receiver}
	emailInfo, err := GetEmailContent(db, emailType, lang)
	if err != nil {
		log.Fatalf("GetEmailContent error: %v\n", err)
		return err
	}

	subject := emailInfo.subject
	body, err := ParseTemplate(emailInfo.body, emailData)
	if err != nil {
		log.Errorf("ParseTemplate error: %v\n", err)
		return err
	}

	sub := fmt.Sprintf(`%s - %s`, subject, emailData.RoomName)
	smtpContent := smtp.PlainAuth("", from, pass, host)
	e := NewEmail(from, sendTo, sub, body, smtpContent)

	ok, err := e.Send()
	if err != nil {
		log.Fatalf("Send Email error: %v\n", err)
	}

	emailMsg := fmt.Sprintf("[ %[1]v ],   Type ===> %[2]s,   Receiver ===> %[3]s", ok, emailType, receiver)
	log.WithField("Email status:", emailMsg).Info()

	return nil
}

// Send sends email
func (r *Email) Send() (bool, error) {
	to := strings.Join(r.to, " ")
	mime := "MIME-version: 1.0;\nContent-Type: text/html; charset=\"UTF-8\";\n\n"
	subject := "From: Paradox Project \r\n" +
		"To: " + to + "\r\n" +
		"Subject: " + r.subject + "\n"
	msg := []byte(subject + mime + "\n" + r.body)
	addr := "smtp.gmail.com:587"

	if err := smtp.SendMail(addr, r.auth, r.from, r.to, msg); err != nil {
		return false, err
	}
	return true, nil
}

func GetEmailContent(db *sql.DB, emailCode string, Lang string) (emailInfo Email, err error) {

	query := fmt.Sprintf(`select 
								subject, body 
						  from email 
						  where code = '%s' 
						  and lcl = '%s'`, emailCode, Lang)

	row := db.QueryRow(query)
	err = row.Scan(&emailInfo.subject, &emailInfo.body)

	if err == sql.ErrNoRows {
		err = nil
	} else if err != nil {
		log.WithFields(logrus.Fields{"Query": misc.Unspace(query), "package": "misc.emails", "method": "GetEmailContent"}).Error(err)
		err = errors.Wrap(err, "database query error:")
	}

	return
}
