package misc

import (
	"fmt"
	"khakibee/admin-api/config"
	"log"
	"net/http"
	"strings"
)

func Broadcast(msg string) {
	cfg, err := config.NewConfig("./")
	if err != nil {
		log.Fatalf("error opening config file: %v\n", err)
	}

	url := cfg.WS + "/ws/broadcast"
	method := "POST"

	payload := strings.NewReader(fmt.Sprintf("{\"msg\":\"%s\"}", msg))

	client := &http.Client{}
	req, err := http.NewRequest(method, url, payload)
	if err != nil {
		fmt.Println(err)
	}

	req.Header.Add("Content-Type", "application/json")

	res, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
	}
	defer res.Body.Close()
}
