package misc

import (
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/golang-jwt/jwt"
)

// Unspace (might choose a better name) removes tabs and newlines from a given string
func Unspace(str string, args ...interface{}) (result string) {
	result = strings.ReplaceAll(str, "\n", " ")
	result = strings.ReplaceAll(result, "\t", "")
	for index := len(args) - 1; index >= 0; index-- {
		value := args[index]
		valueString := "NULL"
		// Reverse replacement
		toReplace := "$" + strconv.Itoa(index+1)
		switch value.(type) {
		case *string:
			if value != (*string)(nil) {
				valueString = "'" + *value.(*string) + "'"
			}
		case string:
			valueString = "'" + value.(string) + "'"
		case *bool:
			if value != (*bool)(nil) {
				valueString = strconv.FormatBool(*value.(*bool))
			}
		case bool:
			valueString = strconv.FormatBool(value.(bool))
		case *int:
			if value != (*int)(nil) {
				valueString = strconv.Itoa(*value.(*int))
			}
		case int:
			valueString = strconv.Itoa(value.(int))
		case *float64:
			if value != (*float64)(nil) {
				valueString = fmt.Sprintf("%.2f", *value.(*float64))
			}
		case float64:
			valueString = fmt.Sprintf("%.2f", value.(float64))
		case []int, []string:
			valueString = fmt.Sprintf("{%s}", strings.Trim(strings.Join(strings.Fields(fmt.Sprint(value)), ","), "[]"))
		default:
			valueString = value.(string)
		}
		result = strings.ReplaceAll(result, toReplace, valueString)
	}
	return
}

// TokenGenerate return token JWT (simulating authentication)
func TokenGenerate(issuer string, subject string, rmID *string) (signedToken string, err error) {
	var rId string
	if rmID != nil {
		rId = *rmID
	}

	// Create the Claims
	claims := &jwt.StandardClaims{
		ExpiresAt: time.Now().Add(time.Hour * 24 * 365).Unix(),
		Issuer:    issuer,
		Subject:   subject,
		Id:        rId,
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	signedToken, err = token.SignedString([]byte("secret"))
	return
}
