package utils

import (
	"errors"
	"strconv"
	"strings"

	"github.com/labstack/echo/v4"
)

// GetCompanyIDs returns the user id, role id and room id
// stored in the given context (from the JWT token)
func GetCompanyIDs(c echo.Context, key string) (string, int, error) {
	if key == "" {
		return "", -1, errors.New("MISSING COMPANY IDs CONTEXT KEY")
	}
	userIDs := c.Get(key).(string)

	IDs := strings.Split(userIDs, ",")
	if len(IDs) != 2 {
		return "", -1, errors.New("MISSING COMPANY IDs")
	}

	IDs1, err := strconv.Atoi(IDs[1])
	if err != nil {
		return "", -2, errors.New("MISSING COMPANY IDs")
	}

	return IDs[0], IDs1, nil
}
