package middlewares

import (
	"database/sql"
	"net/http"

	"khakibee/admin-api/config"
	"khakibee/admin-api/misc/logger"
	"khakibee/admin-api/models"

	jwt "github.com/golang-jwt/jwt"
	echo "github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
)

//Common :defines application/json as default Content-Type
func Common(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		if err := next(c); err != nil {
			c.Error(err)
		}

		// get conf from context
		conf := c.Get(config.ConfContextKey).(*config.Config)

		c.Response().Header().Add("Content-Type", "application/json")
		c.Response().Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
		c.Response().Header().Set("Version", conf.Version)
		return nil
	}
}

//Auth : Authentication middleware
func Auth(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		// get conf from context
		conf := c.Get(config.ConfContextKey).(*config.Config)

		user := c.Get("user")
		if user == nil {
			c.Set(conf.UserIDsKey, "null")
			return next(c)
		}

		claims := user.(*jwt.Token).Claims.(*jwt.StandardClaims)

		userIDs := claims.Issuer
		if userIDs == "" {
			return logger.EchoHTTPError(http.StatusBadRequest, "Invalid token", logrus.Fields{"path": c.Request().URL.Path, "jwt": claims, "package": "middlewares", "method": "Auth"})
		}

		roleID := claims.Subject
		roomID := claims.Id

		c.Set(conf.UserIDsKey, userIDs)
		c.Set(conf.RoleIDKey, roleID)
		c.Set(conf.RoomIDKey, roomID)
		return next(c)
	}
}

// AuthWhitelistSkipper : Skipping Authentication middleware for selected cases
func AuthWhitelistSkipper(c echo.Context) bool {
	// get conf from context
	conf := c.Get(config.ConfContextKey).(*config.Config)
	r := c.Request()

	if r.URL.Path == conf.ContextPath+"/api/auth" || r.Method == http.MethodOptions {
		return true
	}

	return false
}

//APIUser : Authentication middleware
func APIUser(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		// get conf from context
		conf := c.Get(config.ConfContextKey).(*config.Config)

		token := c.Request().Header.Get("Authorization")

		// get db from context
		db := c.Get(models.DBContextKey).(*sql.DB)

		apiUser, err := models.AuthToken(c.Request().Context(), db, token)
		if err != nil {
			return logger.EchoHTTPError(http.StatusInternalServerError, err.Error(), logrus.Fields{"path": c.Request().URL.Path, "package": "middlewares", "method": "APIUser"})
		} else if apiUser == nil {
			return logger.EchoHTTPError(http.StatusUnauthorized, "Unauthorized", logrus.Fields{"path": c.Request().URL.Path, "package": "middlewares", "method": "APIUser"})
		}

		c.Set(conf.APIUsr, *apiUser)
		return next(c)
	}
}
