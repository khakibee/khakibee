package main

import (
	"context"
	"database/sql"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	jwt "github.com/golang-jwt/jwt"
	echo "github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"

	"khakibee/admin-api/bindings"
	"khakibee/admin-api/config"
	cr "khakibee/admin-api/cron"
	"khakibee/admin-api/handlers"
	"khakibee/admin-api/middlewares"
	"khakibee/admin-api/misc/logger"
	"khakibee/admin-api/models"

	_ "github.com/go-sql-driver/mysql"
	cron "github.com/robfig/cron/v3"

	echoLogrusMidd "github.com/neko-neko/echo-logrus/v2"
)

var version = "Undefined"
var log = logger.Logger

func main() {
	// Echo instance
	e := echo.New()

	e.Logger = logger.EchoLogger
	log.WithField("Version", version).Info()

	e.Validator = new(bindings.Validator)

	conf, err := config.ReadConf()
	if err != nil {
		log.Fatalf("error opening config file: %v\n", err)
	}
	conf.Version = version

	db := connectDB(conf.DB)
	defer db.Close()
	log.WithField("Database", "Connected").Info()

	e.Use(func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			c.Set(config.ConfContextKey, conf)
			c.Set(models.DBContextKey, db)
			return next(c)
		}
	})

	// Middleware
	e.Use(echoLogrusMidd.Logger())
	e.Use(middleware.Recover())
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{conf.Alloworigin},
	}))
	e.Use(middlewares.Common)

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  ADMIN  APIs  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	api := e.Group(conf.ContextPath + "/api")

	api.Use(middleware.JWTWithConfig(middleware.JWTConfig{
		Claims:     &jwt.StandardClaims{},
		SigningKey: []byte("secret"),
		Skipper:    middlewares.AuthWhitelistSkipper,
	}))
	api.Use(middlewares.Auth)

	api.POST("/auth", handlers.Authenticate)

	api.GET("/sessions", handlers.GetSessions)
	api.POST("/sessions", handlers.EditSessions)

	api.GET("/bookings", handlers.GetBookings)
	api.POST("/booking", handlers.BookSession)
	api.PUT("/booking", handlers.EditBooking)
	api.POST("/booking/:id", handlers.RemoveBooking)

	api.GET("/rooms", handlers.GetRooms)
	api.POST("/rooms", handlers.EditRoom)
	// api.GET("/rooms/{id}/prices", handlers.clientGetPrices)

	api.POST("/notifications", handlers.GetNotifications)
	api.POST("/notifications/update", handlers.UpdateNotifications)

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  I-FRAME APIs  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	papi := e.Group(conf.ContextPath + "/api/if")

	papi.GET("/rooms", handlers.PubGetRooms)
	papi.GET("/regions", handlers.PubGetRegions)
	papi.GET("/sessions", handlers.PubGetSessions)
	papi.GET("/bookings/:id", handlers.PubGetBookings)
	papi.POST("/booking", handlers.PubBookSession)
	// papi.GET("/rooms/{id}/prices", handlers.clientGetPrices)
	// papi.GET("/offer/{oc}", handlers.clientGetOffer)

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  PUBLIC  APIs  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	api_pub := e.Group(conf.ContextPath + "/api/pub")
	api_pub.Use(middlewares.APIUser)

	api_pub.GET("/rooms/:id/bookings", handlers.GetAvailableBookings)
	api_pub.POST("/rooms/:id/bookings", handlers.NewBooking)
	api_pub.DELETE("/rooms/:id/bookings", handlers.DeleteBooking)

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  CRON JOBS  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	// Send reminder emails every 15 minutes 3 hours before start the session.
	if conf.Cron.Reminder3H.Run {
		log.WithField("Cron Reminder(3 Hours Before)", "Enabled").Info()
		l, _ := time.LoadLocation("Europe/Athens")
		c := cron.New(cron.WithLocation(l))
		crConf := cr.Config{
			DB:         db,
			Version:    conf.Version,
			AdminEmail: conf.Email.Username,
		}
		c.AddFunc(conf.Cron.Reminder3H.Expr, func() {
			cr.SendEmail3HoursBefore(crConf, "3HB")
		})
		c.Start()
	}

	// Send reminder emails every 60 minutes 1 day before start the session.
	if conf.Cron.Reminder1D.Run {
		log.WithField("Cron Reminder(1 Day Before)", "Enabled").Info()
		l, _ := time.LoadLocation("Europe/Athens")
		c := cron.New(cron.WithLocation(l))
		crConf := cr.Config{
			DB:         db,
			Version:    conf.Version,
			AdminEmail: conf.Email.Username,
		}
		c.AddFunc(conf.Cron.Reminder1D.Expr, func() {
			cr.SendEmail24HoursBefore(crConf, "24HB")
		})
		c.Start()
	}

	// Send review mails every 60 minutes.
	if conf.Cron.Review.Run {
		log.WithField("Cron Review", "Enabled").Info()
		l, _ := time.LoadLocation("Europe/Athens")
		c := cron.New(cron.WithLocation(l))
		crConf := cr.Config{
			DB:      db,
			Version: conf.Version,
		}
		c.AddFunc(conf.Cron.Review.Expr, func() {
			cr.SendEmail24HoursAfter(crConf, "24HA")
		})
		c.Start()
	}

	// Start server
	go func() {
		if err := e.Start(fmt.Sprintf(":%d", conf.Port)); err != nil {
			e.Logger.Info(err.Error())
		}
	}()

	// Wait for interrupt signal to gracefully shutdown the server with a timeout of 10 seconds.
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, os.Interrupt, syscall.SIGTERM)
	<-sigs
	e.Logger.Info("Gracefully shutting down server")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	if err := e.Shutdown(ctx); err != nil {
		e.Logger.Fatal(err)
	}
}

func connectDB(c config.DBConf) *sql.DB {
	pgConString := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s", c.Username, c.Password, c.Host, c.Port, c.DBName)

	// db.SetMaxIdleConns(0)   // this is the root problem! set it to 0 to remove all idle connections
	// db.SetMaxOpenConns(100) // or whatever is appropriate for your setup.

	// Open database connection and add database to context
	db, err := sql.Open("mysql", pgConString)
	if err != nil {
		log.Fatalf("error opening database: %v\n", err)
	}

	// Check if connection is ok
	err = db.Ping()
	if err != nil {
		log.Fatalf("error pinging database: %v\n", err)
	}

	return db
}
