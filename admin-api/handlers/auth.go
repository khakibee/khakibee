package handlers

import (
	"database/sql"
	"fmt"
	"net/http"
	"strings"

	"khakibee/admin-api/bindings"
	"khakibee/admin-api/misc"
	"khakibee/admin-api/misc/logger"
	models "khakibee/admin-api/models"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
)

// AuthenticateResponse is the response to a login or Tableau Server request
type AuthenticateResponse struct {
	Token string
	User  string
	Role  string
	Room  *string
}

// Authenticate authenticates
func Authenticate(c echo.Context) error {
	req := new(bindings.AuthRequest)
	logFields := logrus.Fields{"package": "handlers.auth", "method": "Authenticate"}

	if err := c.Bind(req); err != nil {
		return logger.EchoHTTPError(http.StatusBadRequest, "Unable to bind request for auth", logFields)
	}
	if err := c.Validate(req); err != nil {
		return logger.EchoHTTPError(http.StatusBadRequest, fmt.Sprintf("Validation error:%s", err.Error()), logFields)
	}

	// get db from context
	db := c.Get(models.DBContextKey).(*sql.DB)

	res, userID, roleID, roomID, err := models.CheckUser(db, req.Username, req.Password)
	if err != nil {
		return logger.EchoHTTPError(http.StatusInternalServerError, err.Error(), logFields)
	}

	if !res {
		return logger.EchoHTTPError(http.StatusUnauthorized, "Admin user account not found or wrong credentials", logFields)
	}

	signedToken, err := misc.TokenGenerate(strings.Join([]string{userID, roleID}, ","), userID, roomID)
	if err != nil {
		return logger.EchoHTTPError(http.StatusInternalServerError, err.Error(), logFields)
	}

	resp := AuthenticateResponse{}
	resp.Token = signedToken
	resp.User = userID
	resp.Role = roleID
	resp.Room = roomID

	return c.JSON(http.StatusOK, resp)
}
