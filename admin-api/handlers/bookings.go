package handlers

import (
	"database/sql"
	"fmt"
	"net/http"

	"khakibee/admin-api/bindings"
	"khakibee/admin-api/config"
	eu "khakibee/admin-api/misc/echo-utils"
	"khakibee/admin-api/misc/logger"
	"khakibee/admin-api/models"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
)

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~  ADMIN  HANDLERS  ~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// GetBookings handler returns all bookings or bookings by id.
func GetBookings(c echo.Context) error {
	logFields := logrus.Fields{"package": "handlers.bookings", "method": "GetBookings"}

	// req := new(bindings.GetBookingsRequest)
	// if err := c.Bind(req); err != nil {
	// 	return logger.EchoHTTPError(http.StatusBadRequest, "Unable to bind request of getting bookings", logFields)
	// }
	// if err := c.Validate(req); err != nil {
	// 	return logger.EchoHTTPError(http.StatusBadRequest, fmt.Sprintf("Validation error:%s", err.Error()), logFields)
	// }

	conf := c.Get(config.ConfContextKey).(*config.Config)
	userID, roleID, err := eu.GetCompanyIDs(c, conf.UserIDsKey)
	if err != nil {
		return logger.EchoHTTPError(http.StatusInternalServerError, err.Error(), logFields)
	}

	// get db from context
	db := c.Get(models.DBContextKey).(*sql.DB)

	permitedRoom, err := models.UserRoleLicense(c.Request().Context(), db, userID, roleID)
	if err != nil {
		return logger.EchoHTTPError(http.StatusInternalServerError, err.Error(), logrus.Fields{"package": "handlers.notifications", "method": "GetNotifications"})
	}

	bookings, err := models.GetBookings(c.Request().Context(), db, permitedRoom)
	if err != nil {
		return logger.EchoHTTPError(http.StatusInternalServerError, err.Error(), logFields)
	}

	return c.JSON(http.StatusOK, bookings)
}

// BookSession handler book an available session.
func BookSession(c echo.Context) error {
	logFields := logrus.Fields{"package": "handlers.bookings", "method": "BookSession"}
	conf := c.Get(config.ConfContextKey).(*config.Config)

	req := new(bindings.BookSessionRequest)
	if err := c.Bind(req); err != nil {
		return logger.EchoHTTPError(http.StatusBadRequest, "Unable to bind request of booking a session", logFields)
	}
	if err := c.Validate(req); err != nil {
		return logger.EchoHTTPError(http.StatusBadRequest, fmt.Sprintf("Validation error:%s", err.Error()), logFields)
	}

	// get db from context
	db := c.Get(models.DBContextKey).(*sql.DB)

	success, err := models.BookSession(db, *req, conf.Email.Username)
	if err != nil {
		return logger.EchoHTTPError(http.StatusInternalServerError, err.Error(), logFields)
	}
	if !success {
		c.NoContent(http.StatusNotFound)
	}

	return c.NoContent(http.StatusOK)
}

// EditBooking handler book an available session.
func EditBooking(c echo.Context) error {
	logFields := logrus.Fields{"package": "handlers.bookings", "method": "BookSession"}
	conf := c.Get(config.ConfContextKey).(*config.Config)

	req := new(bindings.BookSessionRequest)
	if err := c.Bind(req); err != nil {
		return logger.EchoHTTPError(http.StatusBadRequest, "Unable to bind request of booking a session", logFields)
	}
	if err := c.Validate(req); err != nil {
		return logger.EchoHTTPError(http.StatusBadRequest, fmt.Sprintf("Validation error:%s", err.Error()), logFields)
	}

	// get db from context
	db := c.Get(models.DBContextKey).(*sql.DB)

	err := models.EditBookSession(db, *req, conf.Email.Username)
	if err != nil {
		return logger.EchoHTTPError(http.StatusInternalServerError, err.Error(), logFields)
	}

	return c.NoContent(http.StatusOK)
}

// RemoveBooking handler remove selected booking by id.
func RemoveBooking(c echo.Context) error {
	logFields := logrus.Fields{"package": "handlers.bookings", "method": "RemoveBooking"}
	conf := c.Get(config.ConfContextKey).(*config.Config)

	req := new(bindings.BookingIdRequest)
	if err := c.Bind(req); err != nil {
		return logger.EchoHTTPError(http.StatusBadRequest, "Unable to bind request of removing a booking", logFields)
	}
	if err := c.Validate(req); err != nil {
		return logger.EchoHTTPError(http.StatusBadRequest, fmt.Sprintf("Validation error:%s", err.Error()), logFields)
	}

	// get db from context
	db := c.Get(models.DBContextKey).(*sql.DB)

	updated, err := models.RemoveBooking(c.Request().Context(), db, conf.Email.Username, req.RoomID)
	if err != nil {
		return logger.EchoHTTPError(http.StatusInternalServerError, err.Error(), logFields)
	} else if !updated {
		return logger.EchoHTTPError(http.StatusNotFound, "Booking not found!", logFields)
	}

	return c.NoContent(http.StatusOK)
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~  I-FRAME HANDLERS  ~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// PubGetBookings handler returns bookings by id.
func PubGetBookings(c echo.Context) error {
	logFields := logrus.Fields{"package": "handlers.bookings", "method": "PubGetBookings"}
	req := new(bindings.BookingIdRequest)

	if err := c.Bind(req); err != nil {
		return logger.EchoHTTPError(http.StatusBadRequest, "Unable to bind request of get bookings", logFields)
	}
	if err := c.Validate(req); err != nil {
		return logger.EchoHTTPError(http.StatusBadRequest, fmt.Sprintf("Validation error:%s", err.Error()), logFields)
	}

	// get db from context
	db := c.Get(models.DBContextKey).(*sql.DB)

	bookings, err := models.PubGetBookings(c.Request().Context(), db, req.RoomID)
	if err != nil {
		return logger.EchoHTTPError(http.StatusInternalServerError, err.Error(), logFields)
	}

	if bookings == nil {
		bookings = []models.PubBooking{}
	}

	return c.JSON(http.StatusOK, bookings)
}

// PubBookSession handler book an available session.
func PubBookSession(c echo.Context) error {
	logFields := logrus.Fields{"package": "handlers.bookings", "method": "PubBookSession"}
	conf := c.Get(config.ConfContextKey).(*config.Config)

	req := new(bindings.BookSessionRequest)
	if err := c.Bind(req); err != nil {
		fmt.Println("Bind error: ", err)
		return logger.EchoHTTPError(http.StatusBadRequest, "Unable to bind request of booking a session", logFields)
	}
	if err := c.Validate(req); err != nil {
		return logger.EchoHTTPError(http.StatusBadRequest, fmt.Sprintf("Validation error:%s", err.Error()), logFields)
	}

	// get db from context
	db := c.Get(models.DBContextKey).(*sql.DB)

	success, err := models.PubBookSession(c.Request().Context(), db, *req, conf.Email.Username)
	if err != nil {
		return logger.EchoHTTPError(http.StatusInternalServerError, err.Error(), logFields)
	}
	if !success {
		c.NoContent(http.StatusNotFound)
	}

	return c.NoContent(http.StatusOK)
}
