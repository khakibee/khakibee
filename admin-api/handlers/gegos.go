package handlers

import (
	"database/sql"
	"fmt"
	"khakibee/admin-api/bindings"
	"khakibee/admin-api/config"
	"khakibee/admin-api/misc"
	"khakibee/admin-api/misc/emails"
	"khakibee/admin-api/misc/logger"
	models "khakibee/admin-api/models"
	"net/http"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
)

var room_names = []string{"", "The Mansion", "The Bookstore", "Music Academy", "The Mansion Paros Edition"}

// NewBooking handler book an available session.
func NewBooking(c echo.Context) error {
	logFields := logrus.Fields{"package": "handlers.bookings", "method": "NewBooking"}
	conf := c.Get(config.ConfContextKey).(*config.Config)
	apiUser := c.Get(conf.APIUsr).(string)

	nbr := new(bindings.NewBookindRequest)
	if err := c.Bind(nbr); err != nil {
		fmt.Println("Bind error: ", err)
		return logger.EchoHTTPError(http.StatusBadRequest, "Unable to bind request of booking a session", logFields)
	}
	if err := c.Validate(nbr); err != nil {
		return logger.EchoHTTPError(http.StatusBadRequest, fmt.Sprintf("Validation error:%s", err.Error()), logFields)
	}

	// get db from context
	db := c.Get(models.DBContextKey).(*sql.DB)

	dt, err := time.Parse("2006-01-02", nbr.Date)
	if err != nil {
		return logger.EchoHTTPError(http.StatusInternalServerError, err.Error(), logFields)
	}
	wkDay := int(dt.Weekday())

	sessID, err := models.SelectSessionID(c.Request().Context(), db, nbr.RoomID, wkDay, nbr.Time)
	if err != nil {
		return logger.EchoHTTPError(http.StatusInternalServerError, err.Error(), logFields)
	} else if sessID == nil {
		return c.NoContent(http.StatusNotFound)
	}

	ok, err := models.InsertBooking(c.Request().Context(), db, *nbr, apiUser, *sessID)
	if err != nil {
		return logger.EchoHTTPError(http.StatusInternalServerError, err.Error(), logFields)
	} else if !ok {
		return c.NoContent(http.StatusConflict)
	}

	emailTemplateAdmin := "bkng_admin"
	adminUsername := conf.Email.Username
	lng := "en"

	// Fill struct with needed email details
	emailDetails := emails.EmailBookingDetails{}

	dateParse, _ := time.Parse("2006-01-02 15:04", nbr.Date+" "+nbr.Time)
	dateFormat := dateParse.Format("02/01/2006 15:04")

	emailDetails.Date = dateFormat
	emailDetails.Name = nbr.Nme
	emailDetails.Email = nbr.Email
	emailDetails.Phone = nbr.Phone
	emailDetails.Players = nbr.PlrNum

	emailDetails.RoomName = room_names[nbr.RoomID]
	emailDetails.Level = nbr.PlrLvl
	emailDetails.Notes = *nbr.Notes
	emailDetails.TeamName = *nbr.TeamNme
	emailDetails.RoomID = nbr.RoomID
	emailDetails.Lang = nbr.Lang

	emailDetails.Helps = "NORMAL"
	if nbr.Diff == "H" {
		emailDetails.Helps = "HARD"
	}

	emailDetails.PlayRoom1 = "ΟΧΙ"
	if nbr.PlayRoom1 != nil && *nbr.PlayRoom1 {
		emailDetails.PlayRoom1 = "ΝΑΙ"
	}

	emailDetails.EscRoom1 = "ΟΧΙ"
	if nbr.EscRoom1 != nil && *nbr.EscRoom1 {
		emailDetails.EscRoom1 = "ΝΑΙ"
	}

	emailDetails.Age = "ΟΧΙ"
	if nbr.Age {
		emailDetails.Age = "ΝΑΙ"
	}

	emailDetails.LearnUs = apiUser

	emails.SendEmail(db, emailDetails, emailTemplateAdmin, adminUsername, lng)

	go misc.Broadcast("DIRTY")
	return c.NoContent(http.StatusOK)
}

// DeleteBooking handler returns all bookings or bookings by id.
func DeleteBooking(c echo.Context) error {
	logFields := logrus.Fields{"package": "handlers.bookings", "method": "DeleteBooking"}
	req := new(bindings.DeleteBookingsRequest)

	conf := c.Get(config.ConfContextKey).(*config.Config)
	apiUser := c.Get(conf.APIUsr).(string)

	if err := c.Bind(req); err != nil {
		return logger.EchoHTTPError(http.StatusBadRequest, "Unable to bind request of get bookings", logFields)
	}
	if err := c.Validate(req); err != nil {
		return logger.EchoHTTPError(http.StatusBadRequest, fmt.Sprintf("Validation error:%s", err.Error()), logFields)
	}

	// get db from context
	db := c.Get(models.DBContextKey).(*sql.DB)

	ok, err := models.DeleteBooking(c.Request().Context(), db, *req, apiUser)
	if err != nil {
		return logger.EchoHTTPError(http.StatusInternalServerError, err.Error(), logFields)
	} else if ok {
		return c.NoContent(http.StatusOK)
	}

	return c.NoContent(http.StatusNotFound)
}

// GetAvailableBookings handler returns all bookings or bookings by id.
func GetAvailableBookings(c echo.Context) error {
	logFields := logrus.Fields{"package": "handlers.bookings", "method": "GetAvailableBookings"}
	req := new(bindings.AvailableBookingsRequest)

	if err := c.Bind(req); err != nil {
		return logger.EchoHTTPError(http.StatusBadRequest, "Unable to bind request of get bookings", logFields)
	}
	if err := c.Validate(req); err != nil {
		return logger.EchoHTTPError(http.StatusBadRequest, fmt.Sprintf("Validation error:%s", err.Error()), logFields)
	}

	// get db from context
	db := c.Get(models.DBContextKey).(*sql.DB)

	bkgs, err := models.SelectBookingsByRoom(c.Request().Context(), db, req.RoomID)
	if err != nil {
		return logger.EchoHTTPError(http.StatusInternalServerError, err.Error(), logFields)
	}

	ssns, err := models.SelectSessionsByRoom(c.Request().Context(), db, req.RoomID)
	if err != nil {
		return logger.EchoHTTPError(http.StatusInternalServerError, err.Error(), logFields)
	}

	cdate := time.Now().Add(3 * time.Hour)
	cwkd := int(cdate.Weekday())

	AvailableBookings := map[string][]string{}

	for _, s := range ssns {
		wkd := s.WkD - cwkd
		if wkd < 0 {
			wkd += 7
		}

		date := cdate.AddDate(0, 0, wkd)
		dt := date.Format("2006-01-02")
		if (wkd != 0 || (wkd == 0 && date.Format("15:04") < s.TmStart)) && isBkngFree(dt, s.TmStart, bkgs) {
			AvailableBookings[dt] = append(AvailableBookings[dt], s.TmStart)
		}

		date = cdate.AddDate(0, 0, wkd+7)
		dt = date.Format("2006-01-02")
		if isBkngFree(dt, s.TmStart, bkgs) {
			AvailableBookings[dt] = append(AvailableBookings[dt], s.TmStart)
		}

		date = cdate.AddDate(0, 0, wkd+14)
		dt = date.Format("2006-01-02")
		if isBkngFree(dt, s.TmStart, bkgs) {
			AvailableBookings[dt] = append(AvailableBookings[dt], s.TmStart)
		}
	}

	return c.JSON(http.StatusOK, AvailableBookings)
}

func isBkngFree(dt string, tm string, bkgs []models.Booking) bool {
	for _, b := range bkgs {
		if b.Dte == dt && b.TmStart == tm {
			return false
		}
	}
	return true
}
