package handlers

import (
	"database/sql"
	"fmt"
	"khakibee/admin-api/bindings"
	"khakibee/admin-api/misc/logger"
	"khakibee/admin-api/models"
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
)

// PubGetRegions handler returns all active regions.
func PubGetRegions(c echo.Context) error {
	logFields := logrus.Fields{"package": "handlers.regions", "method": "PubGetRegions"}
	req := new(bindings.PubGeRegionsRequest)

	if err := c.Bind(req); err != nil {
		return logger.EchoHTTPError(http.StatusBadRequest, "Unable to bind request of getting regions", logFields)
	}
	if err := c.Validate(req); err != nil {
		return logger.EchoHTTPError(http.StatusBadRequest, fmt.Sprintf("Validation error:%s", err.Error()), logFields)
	}

	// get db from context
	db := c.Get(models.DBContextKey).(*sql.DB)

	regions, err := models.PubSelectRegions(c.Request().Context(), db, req.Lang)
	if err != nil {
		return logger.EchoHTTPError(http.StatusInternalServerError, err.Error(), logFields)
	}

	if regions == nil {
		return logger.EchoHTTPError(http.StatusNotFound, "Regions not found!", logFields)
	}

	return c.JSON(http.StatusOK, regions)
}
