package handlers

import (
	"database/sql"
	"fmt"
	"khakibee/admin-api/bindings"
	"khakibee/admin-api/config"
	eu "khakibee/admin-api/misc/echo-utils"
	"khakibee/admin-api/misc/logger"
	"khakibee/admin-api/models"
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
)

// GetNotifications handler returns all notifications or notifications per role/filter.
func GetNotifications(c echo.Context) error {
	req := new(bindings.GetNotificationsRequest)
	if err := c.Bind(req); err != nil {
		return logger.EchoHTTPError(http.StatusBadRequest, "Unable to bind request of getting notifications", logrus.Fields{"package": "handlers.notifications", "method": "GetNotifications"})
	}
	if err := c.Validate(req); err != nil {
		return logger.EchoHTTPError(http.StatusBadRequest, fmt.Sprintf("Validation error:%s", err.Error()), logrus.Fields{"package": "handlers.notifications", "method": "GetNotifications"})
	}

	conf := c.Get(config.ConfContextKey).(*config.Config)
	userID, roleID, err := eu.GetCompanyIDs(c, conf.UserIDsKey)
	if err != nil {
		return logger.EchoHTTPError(http.StatusInternalServerError, err.Error(), logrus.Fields{"package": "handlers.notifications", "method": "GetNotifications"})
	}

	// get db from context
	db := c.Get(models.DBContextKey).(*sql.DB)

	NotifLimit := conf.Notification.Limit
	if NotifLimit <= 0 {
		NotifLimit = 4
	}

	permitedRoom, err := models.UserRoleLicense(c.Request().Context(), db, userID, roleID)
	if err != nil {
		return logger.EchoHTTPError(http.StatusInternalServerError, err.Error(), logrus.Fields{"package": "handlers.notifications", "method": "GetNotifications"})
	}

	notificationsResp, err := models.SelectNotifications(c.Request().Context(), db, permitedRoom, req.Offset, NotifLimit)
	if err != nil {
		return logger.EchoHTTPError(http.StatusInternalServerError, err.Error(), logrus.Fields{"package": "handlers.notifications", "method": "GetNotifications"})
	}

	return c.JSON(http.StatusOK, notificationsResp)
}

// UpdateNotifications handler update all notifications or notifications per role/filter.
func UpdateNotifications(c echo.Context) error {
	req := new(bindings.UpdateNotificationsRequest)
	if err := c.Bind(req); err != nil {
		return logger.EchoHTTPError(http.StatusBadRequest, "Unable to bind request to update notifications", logrus.Fields{"package": "handlers.notifications", "method": "UpdateNotifications"})
	}

	// conf := c.Get(config.ConfContextKey).(*config.Config)
	// userID, roleID, err := eu.GetCompanyIDs(c, conf.UserIDsKey)
	// if err != nil {
	// 	return logger.EchoHTTPError(http.StatusInternalServerError, err.Error(), logrus.Fields{"package": "handlers.notifications", "method": "UpdateNotifications"})
	// }

	// get db from context
	db := c.Get(models.DBContextKey).(*sql.DB)

	// permitedRoom, err := models.UserRoleLicense(c.Request().Context(), db, userID, roleID)
	// if err != nil {
	// 	return logger.EchoHTTPError(http.StatusInternalServerError, err.Error(), logrus.Fields{"package": "handlers.notifications", "method": "GetNotifications"})
	// }

	err := models.UpdateNotifications(c.Request().Context(), db, req.BkngID, req.Viewed)
	if err != nil {
		return logger.EchoHTTPError(http.StatusInternalServerError, err.Error(), logrus.Fields{"package": "handlers.notifications", "method": "UpdateNotifications"})
	}

	return c.NoContent(http.StatusOK)
}
