package handlers

import (
	"database/sql"
	"fmt"
	"khakibee/admin-api/bindings"
	"khakibee/admin-api/config"
	eu "khakibee/admin-api/misc/echo-utils"
	"khakibee/admin-api/misc/logger"
	"khakibee/admin-api/models"
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
)

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~  ADMIN  HANDLERS  ~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// GetRooms handler returns all rooms or rooms per role.
func GetRooms(c echo.Context) error {
	logFields := logrus.Fields{"package": "handlers.rooms", "method": "GetRooms"}

	// req := new(bindings.GeRoomsRequest)
	// if err := c.Bind(req); err != nil {
	// 	return logger.EchoHTTPError(http.StatusBadRequest, "Unable to bind request of getting rooms", logFields)
	// }
	// if err := c.Validate(req); err != nil {
	// 	return logger.EchoHTTPError(http.StatusBadRequest, fmt.Sprintf("Validation error:%s", err.Error()), logFields)
	// }

	conf := c.Get(config.ConfContextKey).(*config.Config)
	userID, roleID, err := eu.GetCompanyIDs(c, conf.UserIDsKey)
	if err != nil {
		return logger.EchoHTTPError(http.StatusInternalServerError, err.Error(), logFields)
	}

	// get db from context
	db := c.Get(models.DBContextKey).(*sql.DB)

	permitedRoom, err := models.UserRoleLicense(c.Request().Context(), db, userID, roleID)
	if err != nil {
		return logger.EchoHTTPError(http.StatusInternalServerError, err.Error(), logrus.Fields{"package": "handlers.notifications", "method": "GetNotifications"})
	}

	rooms, err := models.SelectRooms(c.Request().Context(), db, permitedRoom)
	if err != nil {
		return logger.EchoHTTPError(http.StatusInternalServerError, err.Error(), logFields)
	}

	if rooms == nil {
		return logger.EchoHTTPError(http.StatusNotFound, "Rooms not found!", logFields)
	}

	return c.JSON(http.StatusOK, rooms)
}

// EditRoom handler update the activeness of each room.
func EditRoom(c echo.Context) error {
	req := new(bindings.EditRoomsRequest)
	logFields := logrus.Fields{"package": "handlers.rooms", "method": "EditRoom"}

	if err := c.Bind(req); err != nil {
		return logger.EchoHTTPError(http.StatusBadRequest, "Unable to bind request of getting rooms", logFields)
	}
	if err := c.Validate(req); err != nil {
		return logger.EchoHTTPError(http.StatusBadRequest, fmt.Sprintf("Validation error:%s", err.Error()), logFields)
	}

	// get db from context
	db := c.Get(models.DBContextKey).(*sql.DB)

	err := models.EditRoom(c.Request().Context(), db, req.Active, req.RoomID)
	if err != nil {
		return logger.EchoHTTPError(http.StatusInternalServerError, err.Error(), logFields)
	}

	return c.NoContent(http.StatusOK)
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~ I-FRAME  HANDLERS  ~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// PubGetRooms handler returns all active rooms.
func PubGetRooms(c echo.Context) error {
	logFields := logrus.Fields{"package": "handlers.rooms", "method": "PubGetRooms"}
	req := new(bindings.PubGeRoomsRequest)

	if err := c.Bind(req); err != nil {
		return logger.EchoHTTPError(http.StatusBadRequest, "Unable to bind request of getting rooms", logFields)
	}
	if err := c.Validate(req); err != nil {
		return logger.EchoHTTPError(http.StatusBadRequest, fmt.Sprintf("Validation error:%s", err.Error()), logFields)
	}

	// get db from context
	db := c.Get(models.DBContextKey).(*sql.DB)

	rooms, err := models.PubSelectRooms(c.Request().Context(), db, req.Lang)
	if err != nil {
		return logger.EchoHTTPError(http.StatusInternalServerError, err.Error(), logFields)
	}

	if rooms == nil {
		return logger.EchoHTTPError(http.StatusNotFound, "Rooms not found!", logFields)
	}

	return c.JSON(http.StatusOK, rooms)
}
