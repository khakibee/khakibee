package handlers

import (
	"database/sql"

	"khakibee/admin-api/bindings"
	"khakibee/admin-api/config"
	eu "khakibee/admin-api/misc/echo-utils"
	"khakibee/admin-api/misc/logger"
	"khakibee/admin-api/models"

	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
)

// WkSessions model
type WkSessions struct {
	Monday    []models.Session `json:"1"`
	Tuesday   []models.Session `json:"2"`
	Wednesday []models.Session `json:"3"`
	Thursday  []models.Session `json:"4"`
	Friday    []models.Session `json:"5"`
	Saturday  []models.Session `json:"6"`
	Sunday    []models.Session `json:"0"`
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~  ADMIN  HANDLERS  ~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// GetSessions handler returns all sessions or sessions by id.
func GetSessions(c echo.Context) error {
	logFields := logrus.Fields{"package": "handlers.sessions", "method": "GetSessions"}

	// req := new(bindings.GeSessionsRequest)
	// if err := c.Bind(req); err != nil {
	// 	return logger.EchoHTTPError(http.StatusBadRequest, "Unable to bind request of getting sessions", logFields)
	// }
	// if err := c.Validate(req); err != nil {
	// 	return logger.EchoHTTPError(http.StatusBadRequest, fmt.Sprintf("Validation error:%s", err.Error()), logFields)
	// }

	conf := c.Get(config.ConfContextKey).(*config.Config)
	userID, roleID, err := eu.GetCompanyIDs(c, conf.UserIDsKey)
	if err != nil {
		return logger.EchoHTTPError(http.StatusInternalServerError, err.Error(), logFields)
	}

	// get db from context
	db := c.Get(models.DBContextKey).(*sql.DB)

	permitedRoom, err := models.UserRoleLicense(c.Request().Context(), db, userID, roleID)
	if err != nil {
		return logger.EchoHTTPError(http.StatusInternalServerError, err.Error(), logrus.Fields{"package": "handlers.notifications", "method": "GetNotifications"})
	}

	sessions, err := models.SelectSessions(c.Request().Context(), db, permitedRoom)
	if err != nil {
		return logger.EchoHTTPError(http.StatusInternalServerError, err.Error(), logFields)
	}

	if sessions == nil {
		return logger.EchoHTTPError(http.StatusNotFound, "Sessions not found!", logFields)
	}

	return c.JSON(http.StatusOK, fillWkSessionsResponse(sessions))
}

// EditSessions handler update the activeness of each session.
func EditSessions(c echo.Context) error {
	req := new(bindings.EditSessionsRequest)
	logFields := logrus.Fields{"package": "handlers.sessions", "method": "EditSessions"}

	if err := c.Bind(req); err != nil {
		return logger.EchoHTTPError(http.StatusBadRequest, "Unable to bind request of getting sessions", logFields)
	}

	// get db from context
	db := c.Get(models.DBContextKey).(*sql.DB)

	err := models.EditSession(c.Request().Context(), db, req.Active, req.SessionID)
	if err != nil {
		return logger.EchoHTTPError(http.StatusInternalServerError, err.Error(), logFields)
	}

	return c.NoContent(http.StatusOK)
}

func fillWkSessionsResponse(sessions []models.Session) (resp WkSessions) {

	for _, ses := range sessions {
		if ses.WkDay == 1 {
			resp.Monday = append(resp.Monday, ses)
		} else if ses.WkDay == 2 {
			resp.Tuesday = append(resp.Tuesday, ses)
		} else if ses.WkDay == 3 {
			resp.Wednesday = append(resp.Wednesday, ses)
		} else if ses.WkDay == 4 {
			resp.Thursday = append(resp.Thursday, ses)
		} else if ses.WkDay == 5 {
			resp.Friday = append(resp.Friday, ses)
		} else if ses.WkDay == 6 {
			resp.Saturday = append(resp.Saturday, ses)
		} else if ses.WkDay == 0 {
			resp.Sunday = append(resp.Sunday, ses)
		}
	}

	return
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~  I-FRAME HANDLERS  ~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// PubGetSessions handler returns all sessions or sessions by id.
func PubGetSessions(c echo.Context) error {
	logFields := logrus.Fields{"package": "handlers.sessions", "method": "PubGetRooms"}

	// get db from context
	db := c.Get(models.DBContextKey).(*sql.DB)

	sessions, err := models.PubSelectSessions(c.Request().Context(), db)
	if err != nil {
		return logger.EchoHTTPError(http.StatusInternalServerError, err.Error(), logFields)
	}

	if sessions == nil {
		return logger.EchoHTTPError(http.StatusNotFound, "Sessions not found!", logFields)
	}

	return c.JSON(http.StatusOK, fillWkSessionsResponse(sessions))
}
