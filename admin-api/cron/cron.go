package reminders

import (
	"database/sql"
	"fmt"
	"khakibee/admin-api/misc/logger"
	"khakibee/admin-api/models"
	"net/http"
	"time"

	emails "khakibee/admin-api/misc/emails"

	"github.com/sirupsen/logrus"
)

// Config configuration
type Config struct {
	Version    string
	DB         *sql.DB
	AdminEmail string
}

// Date-Time format
const (
	layoutISO = "02/01/2006 15:04"
)

func SendEmail3HoursBefore(conf Config, cronTime string) error {
	logFields := logrus.Fields{"package": "cron.reminders", "method": "SendEmail3HoursBefore"}

	bkngs, err := models.GetReminderBookingDetails(conf.DB, cronTime)
	if err != nil {
		return logger.EchoHTTPError(http.StatusInternalServerError, err.Error(), logFields)
	}

	for _, bkng := range bkngs {
		dateParse, _ := time.Parse("2006-01-02 15:04:05", fmt.Sprintf("%v", bkng.Date))
		bkng.Date = dateParse.Format(layoutISO)

		if bkng.Src == "admin" || bkng.Src == "client" {
			emails.SendEmail(conf.DB, bkng, "reminder", bkng.Email, bkng.Lang)
		}
		emails.SendEmail(conf.DB, bkng, "reminder_rm_admin", conf.AdminEmail, "en")
	}

	return nil
}

func SendEmail24HoursBefore(conf Config, cronTime string) error {
	logFields := logrus.Fields{"package": "cron.reminders", "method": "SendEmail24HoursBefore"}

	bkngs, err := models.GetReminderBookingDetails(conf.DB, cronTime)
	if err != nil {
		return logger.EchoHTTPError(http.StatusInternalServerError, err.Error(), logFields)
	}

	for _, bkng := range bkngs {
		dateParse, _ := time.Parse("2006-01-02 15:04:05", fmt.Sprintf("%v", bkng.Date))
		bkng.Date = dateParse.Format(layoutISO)

		if bkng.Src == "admin" || bkng.Src == "client" {
			emails.SendEmail(conf.DB, bkng, "reminder", bkng.Email, bkng.Lang)
		}
		emails.SendEmail(conf.DB, bkng, "reminder_rm_admin", conf.AdminEmail, "en")
	}

	return nil
}

func SendEmail24HoursAfter(conf Config, cronTime string) error {
	logFields := logrus.Fields{"package": "cron.reminders", "method": "SendEmail24HoursAfter"}

	bkngs, err := models.GetReminderBookingDetails(conf.DB, cronTime)
	if err != nil {
		return logger.EchoHTTPError(http.StatusInternalServerError, err.Error(), logFields)
	}

	for _, bkng := range bkngs {
		dateParse, _ := time.Parse("2006-01-02 15:04:05", fmt.Sprintf("%v", bkng.Date))
		bkng.Date = dateParse.Format(layoutISO)

		if bkng.Src == "admin" || bkng.Src == "client" {
			var emailCode = "review"
			if bkng.RoomID == 4 {
				emailCode = "review_paros"
			}

			emails.SendEmail(conf.DB, bkng, emailCode, bkng.Email, "en")
		}
	}

	return nil
}
