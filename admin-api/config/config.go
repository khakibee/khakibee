package config

import (
	"fmt"
	"log"
	"strings"

	"github.com/spf13/viper"
)

// Config :configuration data type
type Config struct {
	DB           DBConf
	Port         int
	Alloworigin  string
	Version      string
	UserIDsKey   string
	RoleIDKey    string
	RoomIDKey    string
	APIUsr       string
	Email        EmailConf
	Notification NotificationConf
	ContextPath  string `mapstructure:"context_path"`
	WS           string
	Cron         Cron
}

// DBConf configuration
type DBConf struct {
	Host     string
	Port     int
	Username string
	Password string
	DBName   string
}

// EmialConf configuration
type EmailConf struct {
	Host     string
	Username string
	Password string
}

// NotificationConf configuration
type NotificationConf struct {
	Limit int
}

// Cron configuration
type Cron struct {
	Reminder3H CronDtls
	Reminder1D CronDtls
	Review     CronDtls
}

// CronDtls configuration
type CronDtls struct {
	Run  bool
	Expr string
}

// NewConfig returns a new configuration
func NewConfig(configPath string) (cfg *Config, err error) {
	viper.AddConfigPath(configPath)
	viper.SetConfigName("config")
	viper.SetConfigType("json")

	viper.SetEnvPrefix("ADMIN")
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))

	// DB defaults
	dbDefaults := map[string]interface{}{
		"db.host":     "127.0.0.1",
		"db.port":     3306,
		"db.user":     "mysql",
		"db.password": "",
		"db.dbname":   "mysql",
	}

	for k, v := range dbDefaults {
		viper.SetDefault(k, v)
	}

	// Admin API defaults
	defaults := map[string]interface{}{
		"port":        3010,
		"assetspath":  "./tmp",
		"alloworigin": "http://localhost:3000",
		"ws":          "http://localhost:3020",
	}

	for k, v := range defaults {
		viper.SetDefault(k, v)
	}

	viper.AutomaticEnv()

	// Find and read the config file
	if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			// Config file not found; ignore error if desired
			// return nil, err
		} else {
			// Config file was found but another error was produced
			return nil, err
		}
	}

	if err != nil { // Handle errors reading the config file
		panic(fmt.Errorf("FATAL ERROR CONFIG FILE: %s", err))
	}

	cfg = new(Config)

	if err := viper.Unmarshal(cfg); err != nil {
		return nil, err
	}

	cfg.UserIDsKey = "userIDsKey"
	cfg.RoleIDKey = "roleIDKey"
	cfg.RoomIDKey = "roomIDKey"
	cfg.APIUsr = "apisrc"

	return
}

func ReadConf() (*Config, error) {
	cfg, err := NewConfig("./")
	if err != nil {
		log.Fatalf("error opening config file: %v\n", err)
	}

	return cfg, err
}
