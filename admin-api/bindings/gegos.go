package bindings

import (
	"regexp"
	"time"
)

type AvailableBookingsRequest struct {
	RoomID int `param:"id"`
}

type DeleteBookingsRequest struct {
	RoomID int    `param:"id"`
	Date   string `json:"dte"`
	Time   string `json:"tme"`
}

type NewBookindRequest struct {
	RoomID int    `param:"id"`
	Date   string `json:"dte"`
	Time   string `json:"tme"`

	Nme     string  `json:"nme"`
	Phone   string  `json:"phone"`
	Email   string  `json:"email"`
	TeamNme *string `json:"team_nme"`
	Notes   *string `json:"notes"`

	PlrNum int    `json:"plr_num"`
	Age    bool   `json:"under_aged"`
	Diff   string `json:"diff"`
	PlrLvl string `json:"plr_lvl"`
	Lang   string `json:"lng"`

	LearnedUs string `json:"learned_us"`

	PlayRoom1 *bool `json:"played_room1"`
	EscRoom1  *bool `json:"escaped_room1"`

	Code *string `json:"code"`

	GDPR bool `json:"consent"`
}

// Validate validates the bookings request
func (abr AvailableBookingsRequest) Validate() error {
	errs := new(RequestErrors)

	if abr.RoomID < 1 {
		errs.Append(ErrRoomID)
	}

	if errs.Len() == 0 {
		return nil
	}

	return errs
}

// Validate validates the bookings request
func (dbr DeleteBookingsRequest) Validate() error {
	errs := new(RequestErrors)

	if dbr.RoomID < 1 {
		errs.Append(ErrRoomID)
	}

	_, err := time.Parse("2006-01-02", dbr.Date)
	if err != nil {
		errs.Append(err)
	}

	_, err = time.Parse("15:04", dbr.Time)
	if err != nil {
		errs.Append(err)
	}

	if errs.Len() == 0 {
		return nil
	}

	return errs
}

// Validate validates the bookings request
func (nbr NewBookindRequest) Validate() error {
	errs := new(RequestErrors)

	if nbr.RoomID < 1 {
		errs.Append(ErrRoomID)
	}

	_, err := time.Parse("2006-01-02", nbr.Date)
	if err != nil {
		errs.Append(err)
	}

	_, err = time.Parse("15:04", nbr.Time)
	if err != nil {
		errs.Append(err)
	}

	if nbr.PlrNum < 2 && nbr.PlrNum > 7 {
		errs.Append(ErrPlrNum)
	}

	if len(nbr.Nme) < 2 {
		errs.Append(ErrPlayerNme)
	}

	phoneRegex := regexp.MustCompile(`^[0-9]{10,20}$`)
	if !phoneRegex.MatchString(nbr.Phone) {
		errs.Append(ErrPhone)
	}

	emailRegex := regexp.MustCompile(`[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}`)
	if !emailRegex.MatchString(nbr.Email) {
		errs.Append(ErrRoomID)
	}

	if nbr.Notes != nil && len(*nbr.Notes) > 500 {
		errs.Append(ErrNotes)
	}

	// teamNmeRegex := regexp.MustCompile(`^[α-ωΑ-Ω-ωίϊΐόάέύϋΰήώa-zA-Z !^%,.(){}'@#&*-_+=:|?;0-9]{0,100}$`)
	// if nbr.TeamNme != nil && !teamNmeRegex.MatchString(*nbr.TeamNme) {
	// 	errs.Append(ErrTeamNme)
	// }

	codeRegex := regexp.MustCompile(`^[\w-]{0,10}$`)
	if nbr.Code != nil && !codeRegex.MatchString(*nbr.Code) {
		errs.Append(ErrCode)
	}

	if nbr.Diff != "N" && nbr.Diff != "H" {
		errs.Append(ErrDiff)
	}

	plrlvlRegex := regexp.MustCompile(`^(0-1)|(2-5)|(6-10)|(11-20)|(21-35)|(36-50)|(51-100)|(101-200)|(201\+)$`)
	if !plrlvlRegex.MatchString(nbr.PlrLvl) {
		errs.Append(ErrPlrLvl)
	}

	lrndUsRegex := regexp.MustCompile(`^(fb)|(go)|(ta)|(fr)|(mn)|(bo)|(co)|(si)|(lf)$`)
	if !lrndUsRegex.MatchString(nbr.LearnedUs) {
		errs.Append(ErrPlrLvl)
	}

	if nbr.RoomID == 2 && nbr.PlayRoom1 == nil {
		errs.Append(ErrPlayRoom1)
	}

	if nbr.RoomID == 2 && nbr.EscRoom1 == nil {
		errs.Append(ErrEscRoom1)
	}

	if nbr.Lang != "en" && nbr.Lang != "el" {
		errs.Append(ErrLang)
	}

	if !nbr.GDPR {
		errs.Append(ErrGDPR)
	}

	if errs.Len() == 0 {
		return nil
	}

	return errs
}
