package bindings

// AuthRequest is the request body of an authentication request
type AuthRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

// Validate validates the authentication request
func (ar AuthRequest) Validate() error {
	errs := new(RequestErrors)

	if ar.Username == "" {
		errs.Append(ErrUsername)
	}

	if ar.Password == "" {
		errs.Append(ErrPassword)
	}

	if errs.Len() == 0 {
		return nil
	}

	return errs
}
