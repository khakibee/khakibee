package bindings

import (
	"errors"
	"strings"
)

var (
	//***************  Login Errors  *****************//
	ErrUsername = errors.New("USERNAME IS EMPTY")
	ErrPassword = errors.New("PASSWORD IS EMPTY")
	ErrRoomID   = errors.New("INVALID ROOM ID")

	//--------------  Book Session  -----------------//
	ErrSessionID     = errors.New("INVALID SESSION ID")
	ErrRoomName      = errors.New("INVALID ROOM NAME")
	ErrPlrNum        = errors.New("INVALID PLAYER NUMBER")
	ErrPlayerNme     = errors.New("INVALID PLAYER NAME")
	ErrPhone         = errors.New("INVALID PHONE")
	ErrLang          = errors.New("INVALID LANGUAGE")
	ErrNotes         = errors.New("INVALID NOTES")
	ErrTeamNme       = errors.New("INVALID TEAM NAME")
	ErrCode          = errors.New("INVALID CODE")
	ErrPlayRoom1     = errors.New("ROOM ID 2 REQUIRES FIELD 'played_room1'")
	ErrEscRoom1      = errors.New("ROOM ID 2 REQUIRES FIELD 'escaped_room1'")
	ErrAge           = errors.New("INVALID AGE")
	ErrDiff          = errors.New("INVALID DIFFICALTY")
	ErrPlrLvl        = errors.New("INVALID PLAYER LEVEL")
	ErrStatusID      = errors.New("INVALID STATUS ID")
	ErrGDPR          = errors.New("INVALID GDPR STATUS")
	ErrActiveID      = errors.New("INVALID ACTIVE STATUS")
	ErrBookingSource = errors.New("INVALID BOOKING SOURCE")

	//--------------  Notifications  ----------------//
	ErrOffset = errors.New("INVALID OFFSET")
)

// RequestErrors contains a list of all errors
type RequestErrors struct {
	errs []error
}

// Append adds errors to the request errors list
func (re *RequestErrors) Append(err error) {
	re.errs = append(re.errs, err)
}

// Len returns the length of the request error list
func (re *RequestErrors) Len() int {
	return len(re.errs)
}

// Error returns a string containing all errors in the error list
func (re *RequestErrors) Error() string {
	errstr := []string{}
	for _, e := range re.errs {
		errstr = append(errstr, e.Error())
	}
	return strings.Join(errstr, ", ")
}
