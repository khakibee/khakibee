package bindings

type GetNotificationsRequest struct {
	Offset *int `json:"offset"`
}

type UpdateNotificationsRequest struct {
	Viewed int `json:"viewed"`
	BkngID int `json:"bkng_id"`
}

// Validate validates the notifications request
func (gnr GetNotificationsRequest) Validate() error {
	errs := new(RequestErrors)

	if gnr.Offset != nil && *gnr.Offset < 0 {
		errs.Append(ErrOffset)
	}

	if errs.Len() == 0 {
		return nil
	}

	return errs
}
