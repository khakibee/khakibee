package bindings

import "errors"

// Validatable interface
type Validatable interface {
	Validate() error
}

// ErrNotValidatable error
var ErrNotValidatable = errors.New("TYPE IS NOT VALIDATABLE")

// Validator type
type Validator struct{}

// Validate method of Validatable interface
func (v *Validator) Validate(i interface{}) error {
	if validatable, ok := i.(Validatable); ok {
		return validatable.Validate()
	}
	return ErrNotValidatable
}
