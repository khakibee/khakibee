package bindings

// GeSessionsRequest is the request body of getting sessions request
type GeSessionsRequest struct {
	RoomID *int `json:"rm"`
}

// EditSessionsRequest is the request body of editing sessions
type EditSessionsRequest struct {
	SessionID int `json:"session_id"`
	Active    int `json:"active"`
}

// Validate validates the sessions request
func (csr GeSessionsRequest) Validate() error {
	errs := new(RequestErrors)

	if csr.RoomID != nil && *csr.RoomID < 1 {
		errs.Append(ErrRoomID)
	}

	if errs.Len() == 0 {
		return nil
	}

	return errs
}
