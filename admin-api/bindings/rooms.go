package bindings

// GeRoomsRequest is the request body of getting rooms
type GeRoomsRequest struct {
	RoomID *int `json:"rm"`
}

// PubGeRoomsRequest is the request body of getting the public rooms
type PubGeRoomsRequest struct {
	Lang *string `query:"lng"`
}

// EditRoomsRequest is the request body of editing rooms
type EditRoomsRequest struct {
	RoomID int `json:"rm_id"`
	Active int `json:"active"`
}

// Validate validates the rooms request
func (crr GeRoomsRequest) Validate() error {
	errs := new(RequestErrors)

	if crr.RoomID != nil && *crr.RoomID < 1 {
		errs.Append(ErrRoomID)
	}

	if errs.Len() == 0 {
		return nil
	}

	return errs
}

// Validate validates the rooms request
func (err EditRoomsRequest) Validate() error {
	errs := new(RequestErrors)

	if err.RoomID < 1 {
		errs.Append(ErrRoomID)
	}

	if err.Active != 0 && err.Active != 1 {
		errs.Append(ErrActiveID)
	}

	if errs.Len() == 0 {
		return nil
	}

	return errs
}

func (pgrr PubGeRoomsRequest) Validate() error {
	errs := new(RequestErrors)

	if pgrr.Lang != nil && *pgrr.Lang != "el" && *pgrr.Lang != "en" {
		errs.Append(ErrLang)
	}

	if errs.Len() == 0 {
		return nil
	}

	return errs
}
