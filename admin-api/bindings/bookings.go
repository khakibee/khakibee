package bindings

import "regexp"

type GetBookingsRequest struct {
	RoomID *int `query:"rm"`
}

type BookingIdRequest struct {
	RoomID int `param:"id"`
}

type BookSessionRequest struct {
	SessionID int     `json:"sess_id"`
	RoomID    int     `json:"rm_id"`
	RoomName  string  `json:"roomName"`
	PlrNum    int     `json:"plr_num"`
	Nme       string  `json:"nme"`
	Phone     string  `json:"phone"`
	Email     string  `json:"email"`
	Lang      string  `json:"lang"`
	Age       string  `json:"age"`
	StatusID  int     `json:"bkng_sts_id"`
	PlayRoom1 int     `json:"playRoom1"`
	EscRoom1  int     `json:"escRoom1"`
	Notes     *string `json:"notes"`
	Dte       string  `json:"dte"`
	Diff      string  `json:"diff"`
	PlrLvl    string  `json:"plr_lvl"`
	LearnedUs string  `json:"learned_us"`
	TeamNme   *string `json:"team_nme"`
	Code      *string `json:"code"`
	GDPR      int     `json:"gdpr"`
	Src       string  `json:"src"`
}

// Validate validates the bookings request
func (gbr GetBookingsRequest) Validate() error {
	errs := new(RequestErrors)

	if gbr.RoomID != nil && *gbr.RoomID < 1 {
		errs.Append(ErrRoomID)
	}

	if errs.Len() == 0 {
		return nil
	}

	return errs
}

// Validate validates the bookings remove request
func (bir BookingIdRequest) Validate() error {
	errs := new(RequestErrors)

	if bir.RoomID < 1 {
		errs.Append(ErrRoomID)
	}

	if errs.Len() == 0 {
		return nil
	}

	return errs
}

// Validate validatesa booking session request
func (bsr BookSessionRequest) Validate() error {
	errs := new(RequestErrors)

	if bsr.SessionID < 1 {
		errs.Append(ErrSessionID)
	}

	if bsr.RoomID < 1 {
		errs.Append(ErrRoomID)
	}

	if len(bsr.RoomName) < 1 {
		errs.Append(ErrRoomName)
	}

	if bsr.PlrNum < 1 && bsr.PlrNum > 7 {
		errs.Append(ErrPlrNum)
	}

	nameRegex := regexp.MustCompile(`^[α-ωΑ-Ω-ωίϊΐόάέύϋΰήώa-zA-Z!-+='*&^$#@0-9 ]{2,75}$`)
	if !nameRegex.MatchString(bsr.Nme) {
		errs.Append(ErrPlayerNme)
	}

	phoneRegex := regexp.MustCompile(`^[0-9]{10,20}$`)
	if !phoneRegex.MatchString(bsr.Phone) {
		errs.Append(ErrPhone)
	}

	emailRegex := regexp.MustCompile(`[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}`)
	if !emailRegex.MatchString(bsr.Email) {
		errs.Append(ErrRoomID)
	}

	notesRegex := regexp.MustCompile(`^[α-ωΑ-Ω-ωίϊΐόάέύϋΰήώa-zA-Z 0-9()!^%{}'@#&*-_+=:|?;,.?;]{0,500}$`)
	if bsr.Notes != nil && !notesRegex.MatchString(*bsr.Notes) {
		errs.Append(ErrNotes)
	}

	teamNmeRegex := regexp.MustCompile(`^[α-ωΑ-Ω-ωίϊΐόάέύϋΰήώa-zA-Z !^%,.(){}'@#&*-_+=:|?;0-9]{0,100}$`)
	if bsr.TeamNme != nil && !teamNmeRegex.MatchString(*bsr.TeamNme) {
		errs.Append(ErrTeamNme)
	}

	codeRegex := regexp.MustCompile(`^[\w-]{0,10}$`)
	if bsr.Code != nil && !codeRegex.MatchString(*bsr.Code) {
		errs.Append(ErrCode)
	}

	if bsr.Age != "m" && bsr.Age != "s" {
		errs.Append(ErrAge)
	}

	if bsr.Diff != "N" && bsr.Diff != "H" {
		errs.Append(ErrDiff)
	}

	if bsr.PlrLvl != "0-1" && bsr.PlrLvl != "2-5" && bsr.PlrLvl != "6-10" && bsr.PlrLvl != "11-20" &&
		bsr.PlrLvl != "21-35" && bsr.PlrLvl != "36-50" && bsr.PlrLvl != "51-100" && bsr.PlrLvl != "101-200" && bsr.PlrLvl != "201+" {
		errs.Append(ErrPlrLvl)
	}

	if bsr.PlayRoom1 != 0 && bsr.PlayRoom1 != 1 {
		errs.Append(ErrPlayRoom1)
	}

	if bsr.EscRoom1 != 0 && bsr.EscRoom1 != 1 {
		errs.Append(ErrEscRoom1)
	}

	if bsr.Lang != "en" && bsr.Lang != "el" {
		errs.Append(ErrLang)
	}

	if bsr.StatusID < 1 {
		errs.Append(ErrStatusID)
	}

	if bsr.GDPR != 1 {
		errs.Append(ErrGDPR)
	}

	if bsr.Src != "admin" && bsr.Src != "client" && bsr.Src != "escapeAll" {
		errs.Append(ErrBookingSource)
	}

	if errs.Len() == 0 {
		return nil
	}

	return errs
}
