package bindings

// PubGeRegionsRequest is the request body of getting the public regions
type PubGeRegionsRequest struct {
	Lang *string `query:"lng"`
}

func (pgrr PubGeRegionsRequest) Validate() error {
	errs := new(RequestErrors)

	if pgrr.Lang != nil && *pgrr.Lang != "el" && *pgrr.Lang != "en" {
		errs.Append(ErrLang)
	}

	if errs.Len() == 0 {
		return nil
	}

	return errs
}
