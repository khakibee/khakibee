package models

import (
	"context"
	"database/sql"
	"fmt"
	"time"

	"khakibee/admin-api/misc"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

// Session model
type Session struct {
	SessionID int    `json:"session_id"`
	Active    int    `json:"active"`
	RoomID    int    `json:"room_id"`
	Name      string `json:"nme"`
	WkDay     int    `json:"wk_day"`
	WkD       int
	TmStart   string `json:"tm_start"`
	Dur       string `json:"duration"`
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  ADMIN  MODELS  ~~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// SelectSessions returns selected sessions.
func SelectSessions(ctx context.Context, db *sql.DB, permitedRoom *int) (sessions []Session, err error) {
	var roomCondition string

	if permitedRoom != nil {
		roomCondition = fmt.Sprintf(" and rm_id = %d", *permitedRoom)
	}

	query := fmt.Sprintf(`
		select 
			sess_id, sess.active, rm_id, nme, wk_day, date_format(sess.tme, '%%H:%%i'), dur
		from sess
		join rm using(rm_id)
		join rm_lcl using(rm_id)
		where lcl = 'el'
		%s
		order by wk_day asc;`, roomCondition)

	ctx, cancel := context.WithTimeout(ctx, dbQueryTimeout*time.Second)
	defer cancel()

	rows, err := db.QueryContext(ctx, query)
	if err != nil {
		log.WithFields(logrus.Fields{"Query": misc.Unspace(query), "package": "models", "method.sessions": "SelectSessions"}).Error(err)
		err = errors.Wrap(err, "database query error:")
		return
	}
	defer rows.Close()

	for rows.Next() {
		var ses = Session{}

		err = rows.Scan(&ses.SessionID, &ses.Active, &ses.RoomID, &ses.Name, &ses.WkDay, &ses.TmStart, &ses.Dur)
		if err != nil {
			log.WithFields(logrus.Fields{"Query": misc.Unspace(query), "package": "models.sessions", "method": "SelectSessions"}).Error(err)
			err = errors.Wrap(err, "database scan error:")
			return
		}

		sessions = append(sessions, ses)
	}

	return
}

// EditSession update sessions activeness.
func EditSession(ctx context.Context, db *sql.DB, active int, sessionID int) error {
	tx, err := db.Begin()
	if err != nil {
		log.WithFields(logrus.Fields{"package": "models.sessions", "method": "EditSession"}).Error(err)
		return errors.Wrap(err, "database query error:")
	}

	query := fmt.Sprintf(`update sess set active = %d WHERE sess_id = %d`, active, sessionID)

	if _, err := tx.Exec(query); err != nil {
		tx.Rollback()
		qry := misc.Unspace(query)
		log.WithFields(logrus.Fields{"Query": qry, "package": "models.sessions", "method": "EditSession"}).Error(err)
		return errors.Wrap(err, "database query error:")
	}

	if err = tx.Commit(); err != nil {
		log.WithFields(logrus.Fields{"package": "models.sessions", "method": "EditSession"}).Error(err)
		return errors.Wrap(err, "database query error:")
	}

	// hub.broadcast <- []byte("DIRTY")
	go misc.Broadcast("DIRTY")

	return nil
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~  I-FRAME MODELS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// PubSelectSessions returns all sessions.
func PubSelectSessions(ctx context.Context, db *sql.DB) (sessions []Session, err error) {
	query := `
			select 
				sess_id, rm_id, nme, wk_day, date_format(sess.tme, '%H:%i'), dur
			from sess 
			join rm using(rm_id)
			join rm_lcl using(rm_id)
			where lcl = 'el'
			AND rm.active = 1 
			AND sess.active = 1
			order by wk_day asc`

	ctx, cancel := context.WithTimeout(ctx, dbQueryTimeout*time.Second)
	defer cancel()

	rows, err := db.QueryContext(ctx, query)
	if err != nil {
		log.WithFields(logrus.Fields{"Query": misc.Unspace(query), "package": "models", "method.sessions": "PubSelectSessions"}).Error(err)
		err = errors.Wrap(err, "database query error:")
		return
	}
	defer rows.Close()

	for rows.Next() {
		var ses = Session{}

		err = rows.Scan(&ses.SessionID, &ses.RoomID, &ses.Name, &ses.WkDay, &ses.TmStart, &ses.Dur)
		if err != nil {
			log.WithFields(logrus.Fields{"Query": misc.Unspace(query), "package": "models.sessions", "method": "PubSelectSessions"}).Error(err)
			err = errors.Wrap(err, "database scan error:")
			return
		}

		sessions = append(sessions, ses)
	}

	return
}
