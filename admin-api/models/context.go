package models

const (
	// DBContextKey is the key for the db in the context
	DBContextKey = "database-context"

	// Internal db query timeout
	dbQueryTimeout = 10
)
