package models

import (
	"context"
	"database/sql"
	"khakibee/admin-api/bindings"
	misc "khakibee/admin-api/misc"
	"time"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

// PubBookSession model func.
func InsertBooking(ctx context.Context, db *sql.DB, bk bindings.NewBookindRequest, src string, sessID int) (success bool, err error) {
	ctx, cancel := context.WithTimeout(ctx, dbQueryTimeout*time.Second)
	defer cancel()

	dte := bk.Date + " " + bk.Time
	undAge := "s"
	if bk.Age {
		undAge = "m"
	}

	query := `
	insert into bkng ( rm_id, dte, nme, phone, email, team_nme, notes, plr_num, age, diff, plr_lvl, lang, learned_us, code, gdpr, play_1, esc_1 , src, sess_id ) 
	values( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ) 
	ON DUPLICATE KEY UPDATE rm_id=rm_id;
	;`

	res, err := db.ExecContext(ctx, query, bk.RoomID, dte, bk.Nme, bk.Phone, bk.Email, bk.TeamNme, bk.Notes, bk.PlrNum, undAge, bk.Diff, bk.PlrLvl, bk.Lang, bk.LearnedUs, bk.Code, bk.GDPR, bk.PlayRoom1, bk.EscRoom1, src, sessID)
	if err != nil {
		qry := misc.Unspace(query)
		log.WithFields(logrus.Fields{"Query": qry, "package": "models.bookings", "method": "InsertBooking"}).Error(err)
		err = errors.Wrap(err, "database query error:")
		return
	}

	ra, err := res.RowsAffected()
	success = ra == 1

	return
}

// SelectSessionsByRoom returns selected sessions.
func SelectSessionsByRoom(ctx context.Context, db *sql.DB, roomID int) (sessions []Session, err error) {

	query := `
	select 
		sess_id, CAST(wk_day AS int), date_format(sess.tme, '%H:%i')
	from sess
	where active is true
	and rm_id = ?;`

	ctx, cancel := context.WithTimeout(ctx, dbQueryTimeout*time.Second)
	defer cancel()

	rows, err := db.QueryContext(ctx, query, roomID)
	if err != nil {
		log.WithFields(logrus.Fields{"Query": misc.Unspace(query), "package": "models", "method.sessions": "SelectSessions"}).Error(err)
		err = errors.Wrap(err, "database query error:")
		return
	}
	defer rows.Close()

	for rows.Next() {
		var ses = Session{}

		err = rows.Scan(&ses.SessionID, &ses.WkD, &ses.TmStart)
		if err != nil {
			log.WithFields(logrus.Fields{"Query": misc.Unspace(query), "package": "models.sessions", "method": "SelectSessionsByRoom"}).Error(err)
			err = errors.Wrap(err, "database scan error:")
			return
		}

		sessions = append(sessions, ses)
	}

	return
}

// SelectBookingsByRoom returns selected sessions.
func SelectBookingsByRoom(ctx context.Context, db *sql.DB, roomID int) (bookings []Booking, err error) {

	query := `
	select 
		date_format(dte, '%Y-%m-%d'), date_format(tme, '%H:%i')
	from bkng
		join sess using(sess_id)
	where sess.rm_id= ?
	and dte > now()
	and removed = '0000-00-00 00:00:00';`

	ctx, cancel := context.WithTimeout(ctx, dbQueryTimeout*time.Second)
	defer cancel()

	rows, err := db.QueryContext(ctx, query, roomID)
	if err != nil {
		log.WithFields(logrus.Fields{"Query": misc.Unspace(query), "package": "models", "method.sessions": "SelectBookingsByRoom"}).Error(err)
		err = errors.Wrap(err, "database query error:")
		return
	}
	defer rows.Close()

	for rows.Next() {
		var bk = Booking{}

		err = rows.Scan(&bk.Dte, &bk.TmStart)
		if err != nil {
			log.WithFields(logrus.Fields{"Query": misc.Unspace(query), "package": "models.sessions", "method": "SelectBookingsByRoom"}).Error(err)
			err = errors.Wrap(err, "database scan error:")
			return
		}

		bookings = append(bookings, bk)
	}

	return
}

// DeleteBooking returns selected sessions.
func DeleteBooking(ctx context.Context, db *sql.DB, bk bindings.DeleteBookingsRequest, src string) (succeed bool, err error) {
	ctx, cancel := context.WithTimeout(ctx, dbQueryTimeout*time.Second)
	defer cancel()

	dte := bk.Date + " " + bk.Time

	query := `insert into bkng_rmvd 
	select bkng_id,sess_id,bkng_sts_id,plr_id,plr_num,bkng_tme,dte,current_timestamp(), nme, phone, email, lang, age, plr_lvl, code, team_nme, learned_us, src, play_1, diff, notes, esc_1, gdpr, viewed, rm_id from bkng where rm_id=? and dte = ? and src=?;`

	res, err := db.ExecContext(ctx, query, bk.RoomID, dte, src)
	if err != nil {
		log.WithFields(logrus.Fields{"Query": misc.Unspace(query), "package": "models", "method.sessions": "DeleteBooking"}).Error(err)
		err = errors.Wrap(err, "database query error:")
		return
	}

	query = `delete from bkng where rm_id=? and dte = ? and src=?;`

	res, err = db.ExecContext(ctx, query, bk.RoomID, dte, src)
	if err != nil {
		log.WithFields(logrus.Fields{"Query": misc.Unspace(query), "package": "models", "method.sessions": "DeleteBooking"}).Error(err)
		err = errors.Wrap(err, "database query error:")
		return
	}

	ra, err := res.RowsAffected()
	succeed = ra == 1

	return
}

// SelectSessionID returns selected sessions.
func SelectSessionID(ctx context.Context, db *sql.DB, rmID int, wkDay int, tme string) (sessID *int, err error) {
	ctx, cancel := context.WithTimeout(ctx, dbQueryTimeout*time.Second)
	defer cancel()

	query := `select sess_id from sess where rm_id = ? and wk_day=? and tme = ? and active is true;`

	row := db.QueryRowContext(ctx, query, rmID, wkDay, tme)
	err = row.Scan(&sessID)
	if err != nil && err != sql.ErrNoRows {
		log.WithFields(logrus.Fields{"Query": misc.Unspace(query), "package": "models", "method.sessions": "DeleteBooking"}).Error(err)
		err = errors.Wrap(err, "database query error:")
	}
	err = nil
	return
}
