package models

import (
	"context"
	"database/sql"
	"fmt"
	misc "khakibee/admin-api/misc"
	"time"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

// Notification type
type Notification struct {
	BkngID   int     `json:"bkng_id"`
	RoomID   int     `json:"rm_id"`
	RoomName string  `json:"rm_nme"`
	Nme      string  `json:"nme"`
	Dte      string  `json:"dte"`
	BkTme    string  `json:"bk_tme"`
	PlrNum   int     `json:"plr_num"`
	Phone    string  `json:"phone"`
	Viewed   int     `json:"viewed"`
	Source   *string `json:"src"`
}

// NotificationsResp type
type NotificationsResp struct {
	Notifications []Notification `json:"notifications"`
	Count         int            `json:"count"`
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  ADMIN  MODELS  ~~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// SelectNotifications returns selected notifications.
func SelectNotifications(ctx context.Context, db *sql.DB, roomID *int, offset *int, notifLimit int) (notifResp NotificationsResp, err error) {
	notifResp.Notifications = []Notification{}

	var roomCondition string
	if roomID != nil {
		roomCondition = fmt.Sprintf(" and rm.rm_id = %d", *roomID)
	}

	offsetCondition := fmt.Sprintf("limit %d offset 0", notifLimit)
	if offset != nil {
		offsetCondition = fmt.Sprintf("limit %d offset %d", notifLimit, *offset)
	}

	query := fmt.Sprintf(`
					select 
						bkng_id, rm.rm_id, rm_lcl.nme as rm_nme, bkng.nme, DATE_FORMAT(dte, '%%Y-%%c-%%d %%H:%%i') ,
						bkng.plr_num, phone, viewed, timestampdiff(minute, bkng_tme, now()) as bk_tme, src
						from bkng
						join rm using(rm_id)
						join rm_lcl using(rm_id) 
						join sess using(sess_id)
						where (removed != '0000-00-00 00:00:00' || bkng_sts_id != 3)
						and lcl = 'el'
						%[1]s
						order by bkng_tme desc
						%[2]s`, roomCondition, offsetCondition)

	ctx, cancel := context.WithTimeout(ctx, dbQueryTimeout*time.Second)
	defer cancel()

	rows, err := db.QueryContext(ctx, query)
	if err != nil {
		log.WithFields(logrus.Fields{"Query": misc.Unspace(query), "package": "models", "method.notifications": "SelectNotifications"}).Error(err)
		err = errors.Wrap(err, "database query error:")
		return
	}
	defer rows.Close()

	for rows.Next() {
		var notif = Notification{}

		err = rows.Scan(&notif.BkngID, &notif.RoomID, &notif.RoomName, &notif.Nme, &notif.Dte, &notif.PlrNum, &notif.Phone, &notif.Viewed, &notif.BkTme, &notif.Source)
		if err != nil {
			log.WithFields(logrus.Fields{"Query": misc.Unspace(query), "package": "models.notifications", "method": "SelectNotifications"}).Error(err)
			err = errors.Wrap(err, "database scan error:")
			return
		}

		notifResp.Notifications = append(notifResp.Notifications, notif)
	}

	if len(notifResp.Notifications) == 0 {
		return
	}

	queryCount := fmt.Sprintf(`select COUNT(bkng_id) as count
								from bkng
								join rm using(rm_id)
								join rm_lcl using(rm_id)
								join sess using(sess_id)
								where (removed != '0000-00-00 00:00:00' || bkng_sts_id != 3)
								and lcl = 'el'
								%[1]s
								order by bkng_tme desc`, roomCondition)

	row := db.QueryRow(queryCount)
	err = row.Scan(&notifResp.Count)
	if err != nil {
		log.WithFields(logrus.Fields{"Query": misc.Unspace(query), "package": "models.notifications", "method": "SelectNotifications"}).Error(err)
		err = errors.Wrap(err, "database query error:")
	}

	return
}

// UpdateNotifications update selected notifications.
func UpdateNotifications(ctx context.Context, db *sql.DB, bkngID int, viewed int) error {
	tx, err := db.Begin()
	if err != nil {
		log.WithFields(logrus.Fields{"package": "models.notifications", "method": "UpdateNotifications"}).Error(err)
		return errors.Wrap(err, "database query error:")
	}

	query := fmt.Sprintf(`update bkng set viewed = %[1]d where bkng_id = %[2]d`, viewed, bkngID)

	if _, err := tx.Exec(query); err != nil {
		tx.Rollback()
		qry := misc.Unspace(query)
		log.WithFields(logrus.Fields{"Query": qry, "package": "models.notifications", "method": "UpdateNotifications"}).Error(err)
		return errors.Wrap(err, "database query error:")
	}

	if err = tx.Commit(); err != nil {
		log.WithFields(logrus.Fields{"package": "models.notifications", "method": "UpdateNotifications"}).Error(err)
		return errors.Wrap(err, "database query error:")
	}

	return nil
}

// func arrayToString(ar []int, delim string) string {
// 	return strings.Trim(strings.Replace(fmt.Sprint(ar), " ", delim, -1), "[]")
// }
