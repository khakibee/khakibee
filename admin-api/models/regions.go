package models

import (
	"context"
	"database/sql"
	"fmt"
	misc "khakibee/admin-api/misc"
	"time"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

// PubRegion model
type PubRegion struct {
	RegionID int    `json:"rgn_id"`
	Name     string `json:"nme"`
	Img      string `json:"img"`
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~  I-FRAME MODELS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// PubSelectRegions returns the active regions.
func PubSelectRegions(ctx context.Context, db *sql.DB, lng *string) (regions []PubRegion, err error) {
	lang := "el"
	if lng != nil {
		lang = *lng
	}

	query := fmt.Sprintf(`
						select 
							rgn_id, nme, img
						from rgn
						join rgn_lcl using(rgn_id)
						where active
						and rgn_lcl.lcl = '%[1]s'`, lang)

	ctx, cancel := context.WithTimeout(ctx, dbQueryTimeout*time.Second)
	defer cancel()

	rows, err := db.QueryContext(ctx, query)
	if err != nil {
		log.WithFields(logrus.Fields{"Query": misc.Unspace(query), "package": "models", "method.rooms": "PubSelectRegions"}).Error(err)
		err = errors.Wrap(err, "database query error:")
		return
	}
	defer rows.Close()

	for rows.Next() {
		var rgn = PubRegion{}

		err = rows.Scan(&rgn.RegionID, &rgn.Name, &rgn.Img)
		if err != nil {
			log.WithFields(logrus.Fields{"Query": misc.Unspace(query), "package": "models.rooms", "method": "PubSelectRegions"}).Error(err)
			err = errors.Wrap(err, "database scan error:")
			return
		}

		regions = append(regions, rgn)
	}

	return
}
