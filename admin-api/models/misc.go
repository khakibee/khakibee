package models

import (
	"context"
	"crypto/sha256"
	"database/sql"
	"fmt"
	"time"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"

	"khakibee/admin-api/misc"
	"khakibee/admin-api/misc/logger"
)

var log = logger.Logger

// CheckUser return true if the user account exists with the right credentials, otherwise it returns true.
func CheckUser(db *sql.DB, username string, password string) (exists bool, userID string, roleID string, roomID *string, err error) {

	// Create sha256 hash
	h := sha256.New()
	_, err = h.Write([]byte(password))
	if err != nil {
		return
	}
	passwordHash := fmt.Sprintf("%x", h.Sum(nil))

	query := fmt.Sprintf(`select 
								usr_id, role_id, rm_id
						  from usr
						  where usrnm = '%[1]s'
						  and passwd = '%[2]s';`, username, passwordHash)

	row := db.QueryRow(query)

	err = row.Scan(&userID, &roleID, &roomID)
	if err != nil && err != sql.ErrNoRows {
		log.WithFields(logrus.Fields{"Query": misc.Unspace(query), "package": "models.user", "method": "CheckUser"}).Error(err)
		err = errors.Wrap(err, "database query error:")
		return
	} else if err == sql.ErrNoRows {
		err = nil
	} else {
		exists = true
	}

	return
}

// AuthToken return true if the user account exists with the right credentials, otherwise it returns true.
func AuthToken(ctx context.Context, db *sql.DB, token string) (apiuser *string, err error) {

	// Create sha256 hash
	// h := sha256.New()
	// _, err = h.Write([]byte(token))
	// if err != nil {
	// 	return
	// }
	// passwordHash := fmt.Sprintf("%x", h.Sum(nil))

	query := `select usrnm
	from usr
	where role_id = 11
		and passwd = ?
	;`

	ctx, cancel := context.WithTimeout(ctx, dbQueryTimeout*time.Second)
	defer cancel()

	row := db.QueryRow(query, token)

	err = row.Scan(&apiuser)
	if err != nil && err != sql.ErrNoRows {
		log.WithFields(logrus.Fields{"Query": misc.Unspace(query), "package": "models.user", "method": "AuthToken"}).Error(err)
		err = errors.Wrap(err, "database query error:")
		return
	} else if err == sql.ErrNoRows {
		err = nil
	}

	return
}
