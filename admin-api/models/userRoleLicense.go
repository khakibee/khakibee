package models

import (
	"context"
	"database/sql"
	"fmt"
	misc "khakibee/admin-api/misc"
	"time"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

// PubSelectRegions returns the active regions.
func UserRoleLicense(ctx context.Context, db *sql.DB, userID string, roleID int) (l *int, err error) {
	query := fmt.Sprintf(`
					select 
						rm_id
					from usr
					where usr.role_id = %[1]d
					and usr.usr_id = %[2]s;`, roleID, userID)

	ctx, cancel := context.WithTimeout(ctx, dbQueryTimeout*time.Second)
	defer cancel()

	row := db.QueryRowContext(ctx, query)

	err = row.Scan(&l)

	if err != nil && err != sql.ErrNoRows {
		log.WithFields(logrus.Fields{"Query": misc.Unspace(query), "package": "models", "method": "UserRoleLicense"}).Error(err)
		err = errors.Wrap(err, "database query error:")
	}

	if err == sql.ErrNoRows {
		return nil, nil
	}

	return
}
