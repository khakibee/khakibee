package models

import (
	"context"
	"database/sql"
	"fmt"
	"time"

	"khakibee/admin-api/bindings"
	misc "khakibee/admin-api/misc"
	emails "khakibee/admin-api/misc/emails"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

// Booking model
type Booking struct {
	BkngID    int     `json:"bkng_id"`
	PlrNum    int     `json:"plr_num"`
	BkngTme   string  `json:"bkng_tme"`
	Dte       string  `json:"dte"`
	Nme       string  `json:"nme"`
	Phone     string  `json:"phone"`
	Email     string  `json:"email"`
	Lang      string  `json:"lang"`
	Age       string  `json:"age"`
	PlrLvl    string  `json:"plr_lvl"`
	Code      *string `json:"code"`
	TeamNme   *string `json:"team_nme"`
	LearnedUs string  `json:"learned_us"`
	Source    *string `json:"src"`
	Viewed    bool    `json:"viewed"`
	Play1     *bool   `json:"play_1"`
	Esc1      *bool   `json:"esc_1"`
	Diff      string  `json:"diff"`
	Notes     *string `json:"notes"`
	Session
}

// PubBooking model
type PubBooking struct {
	SessionID int    `json:"session_id"`
	Dte       string `json:"dte"`
}

const (
	layoutISO = "02/01/2006 15:04"
)

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  ADMIN  MODELS  ~~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// GetBookings per room.
func GetBookings(ctx context.Context, db *sql.DB, permitedRoom *int) (bookings []Booking, err error) {
	var roomCondition string

	if permitedRoom != nil {
		roomCondition = fmt.Sprintf(" and sess.rm_id = %d", *permitedRoom)
	}

	query := fmt.Sprintf(`
		select 
			sess_id, sess.rm_id, bkng_id, plr_num, bkng_tme, dte, nme, phone, email, lang, age, plr_lvl, code,
			team_nme, learned_us, viewed, src, play_1, esc_1, diff, notes, date_format(tme, '%%H:%%i'), wk_day
		from bkng
		join sess using(sess_id)
		where sess.sess_id = bkng.sess_id
		%s
		and removed = '0000-00-00 00:00:00'`, roomCondition)

	ctx, cancel := context.WithTimeout(ctx, dbQueryTimeout*time.Second)
	defer cancel()

	rows, err := db.QueryContext(ctx, query)
	if err != nil {
		log.WithFields(logrus.Fields{"Query": misc.Unspace(query), "package": "models", "method.bookings": "GetBookings"}).Error(err)
		err = errors.Wrap(err, "database query error:")
		return
	}
	defer rows.Close()

	for rows.Next() {
		var bkng = Booking{}

		err = rows.Scan(&bkng.SessionID, &bkng.RoomID, &bkng.BkngID, &bkng.PlrNum, &bkng.BkngTme, &bkng.Dte, &bkng.Nme, &bkng.Phone,
			&bkng.Email, &bkng.Lang, &bkng.Age, &bkng.PlrLvl, &bkng.Code, &bkng.TeamNme, &bkng.LearnedUs, &bkng.Viewed, &bkng.Source,
			&bkng.Play1, &bkng.Esc1, &bkng.Diff, &bkng.Notes, &bkng.TmStart, &bkng.WkDay)
		if err != nil {
			log.WithFields(logrus.Fields{"Query": misc.Unspace(query), "package": "models.bookings", "method": "GetBookings"}).Error(err)
			err = errors.Wrap(err, "database scan error:")
			return
		}

		bookings = append(bookings, bkng)
	}

	return
}

// BookSession model func.
func BookSession(db *sql.DB, bkngInfo bindings.BookSessionRequest, adminUsername string) (success bool, err error) {
	query := fmt.Sprintf(`
		insert into bkng (sess_id, nme, phone, email, plr_num, lang, plr_lvl, age, diff, code, learned_us,
						  team_nme, play_1, esc_1, notes, dte, bkng_sts_id, gdpr, src, rm_id)
		values(%d, '%s', '%s', '%s', %d, '%s', '%s', '%s', '%s', %s, '%s', %s, %v, %v, %s, '%s', %d, %v, '%s', %d)
		ON DUPLICATE KEY UPDATE rm_id=rm_id;`,
		bkngInfo.SessionID, bkngInfo.Nme, bkngInfo.Phone, bkngInfo.Email, bkngInfo.PlrNum, bkngInfo.Lang, bkngInfo.PlrLvl, bkngInfo.Age, bkngInfo.Diff,
		misc.NullString{bkngInfo.Code}, bkngInfo.LearnedUs, misc.NullString{bkngInfo.TeamNme}, bkngInfo.PlayRoom1, bkngInfo.EscRoom1,
		misc.NullString{bkngInfo.Notes}, bkngInfo.Dte, bkngInfo.StatusID, bkngInfo.GDPR, "admin", bkngInfo.RoomID)

	res, err := db.Exec(query)
	if err != nil {
		qry := misc.Unspace(query)
		log.WithFields(logrus.Fields{"Query": qry, "package": "models.bookings", "method": "BookSession"}).Error(err)
		err = errors.Wrap(err, "database query error:")
		return
	}

	ra, err := res.RowsAffected()
	success = ra == 1
	if err != nil || !success {
		return
	}

	// hub.broadcast <- []byte("DIRTY")
	go misc.Broadcast("DIRTY")

	// Fill struct with needed email details
	emailDetails := emails.EmailBookingDetails{}

	dateParse, _ := time.Parse("2006-01-02 15:04:05", bkngInfo.Dte)
	dateFormat := dateParse.Format(layoutISO)

	emailDetails.Date = dateFormat
	emailDetails.Name = bkngInfo.Nme
	emailDetails.Email = bkngInfo.Email
	emailDetails.Phone = bkngInfo.Phone
	emailDetails.Players = bkngInfo.PlrNum
	emailDetails.RoomName = bkngInfo.RoomName
	emailDetails.Level = bkngInfo.PlrLvl
	emailDetails.Notes = *bkngInfo.Notes
	emailDetails.TeamName = *bkngInfo.TeamNme
	emailDetails.RoomID = bkngInfo.RoomID
	emailDetails.Lang = bkngInfo.Lang

	emailDetails.Helps = "NORMAL"
	if bkngInfo.Diff == "H" {
		emailDetails.Helps = "HARD"
	}

	emailDetails.PlayRoom1 = "ΟΧΙ"
	if bkngInfo.PlayRoom1 == 1 {
		emailDetails.PlayRoom1 = "ΝΑΙ"
	}

	emailDetails.EscRoom1 = "ΟΧΙ"
	if bkngInfo.EscRoom1 == 1 {
		emailDetails.EscRoom1 = "ΝΑΙ"
	}

	emailDetails.Age = "ΝΑΙ"
	if bkngInfo.Age != "s" {
		emailDetails.Age = "ΟΧΙ"
	}

	switch bkngInfo.LearnedUs {
	case "go":
		emailDetails.LearnUs = "Google"
	case "fb":
		emailDetails.LearnUs = "Facebook"
	case "fr":
		emailDetails.LearnUs = "Friends"
	case "si":
		emailDetails.LearnUs = "Escape sites"
	case "ta":
		emailDetails.LearnUs = "Tripadvisor"
	case "mn":
		emailDetails.LearnUs = "The Mansion"
	case "bo":
		emailDetails.LearnUs = "The BookStore"
	case "co":
		emailDetails.LearnUs = "Conservator"
	case "lf":
		emailDetails.LearnUs = "Leaflet/ Card"
	default:
		bkngInfo.LearnedUs = "fr"
		emailDetails.LearnUs = "Facebook"
	}

	emailTemplate := "bkng_"
	emailTemplateAdmin, emailTemplateClient := emailTemplate, emailTemplate
	if bkngInfo.StatusID == 1 {
		emailTemplateAdmin = fmt.Sprintf(`%s%s`, emailTemplateAdmin, "admin")
		emailTemplateClient = fmt.Sprintf(`%s%s%d`, emailTemplateClient, "rm", bkngInfo.RoomID)
	} else {
		emailDetails.Lang = "en"
		emailTemplateAdmin = "edit_admin"
		emailTemplateClient = "edit"
	}

	emails.SendEmail(db, emailDetails, emailTemplateClient, bkngInfo.Email, bkngInfo.Lang)
	emails.SendEmail(db, emailDetails, emailTemplateAdmin, adminUsername, "en")

	return
}

// EditBookSession model func.
func EditBookSession(db *sql.DB, bkngInfo bindings.BookSessionRequest, adminUsername string) error {
	tx, err := db.Begin()
	if err != nil {
		log.WithFields(logrus.Fields{"package": "models.bookings", "method": "BookSession"}).Error(err)
		return errors.Wrap(err, "database query error:")
	}

	query := fmt.Sprintf(`
		insert into bkng (sess_id, nme, phone, email, plr_num, lang, plr_lvl, age, diff, code, learned_us,
						  team_nme, play_1, esc_1, notes, dte, bkng_sts_id, gdpr, src, rm_id)
		values(%d, '%s', '%s', '%s', %d, '%s', '%s', '%s', '%s', %s, '%s', %s, %v, %v, %s, '%s', %d, %v, '%s', %d)
		on duplicate key update nme = '%[2]s', phone = '%[3]s' , email = '%[4]s', plr_num = %[5]d, lang = '%[6]s', plr_lvl = '%[7]s',
		age = '%[8]s', diff = '%[9]s', code = %[10]s, learned_us = '%[11]s', team_nme = %[12]s, play_1 = %[13]v, esc_1 = %[14]v, notes = %[15]s, bkng_sts_id = %[17]d, src = '%[19]s', rm_id=%d;`,
		bkngInfo.SessionID, bkngInfo.Nme, bkngInfo.Phone, bkngInfo.Email, bkngInfo.PlrNum, bkngInfo.Lang, bkngInfo.PlrLvl, bkngInfo.Age, bkngInfo.Diff,
		misc.NullString{bkngInfo.Code}, bkngInfo.LearnedUs, misc.NullString{bkngInfo.TeamNme}, bkngInfo.PlayRoom1, bkngInfo.EscRoom1,
		misc.NullString{bkngInfo.Notes}, bkngInfo.Dte, bkngInfo.StatusID, bkngInfo.GDPR, "admin", bkngInfo.RoomID)

	if _, err := tx.Exec(query); err != nil {
		tx.Rollback()
		qry := misc.Unspace(query)
		log.WithFields(logrus.Fields{"Query": qry, "package": "models.bookings", "method": "BookSession"}).Error(err)
		return errors.Wrap(err, "database query error:")
	}

	if err = tx.Commit(); err != nil {
		log.WithFields(logrus.Fields{"package": "models.bookings", "method": "BookSession"}).Error(err)
		return errors.Wrap(err, "database query error:")
	}

	// hub.broadcast <- []byte("DIRTY")
	go misc.Broadcast("DIRTY")

	// Fill struct with needed email details
	emailDetails := emails.EmailBookingDetails{}

	dateParse, _ := time.Parse("2006-01-02 15:04:05", bkngInfo.Dte)
	dateFormat := dateParse.Format(layoutISO)

	emailDetails.Date = dateFormat
	emailDetails.Name = bkngInfo.Nme
	emailDetails.Email = bkngInfo.Email
	emailDetails.Phone = bkngInfo.Phone
	emailDetails.Players = bkngInfo.PlrNum
	emailDetails.RoomName = bkngInfo.RoomName
	emailDetails.Level = bkngInfo.PlrLvl
	emailDetails.Notes = *bkngInfo.Notes
	emailDetails.TeamName = *bkngInfo.TeamNme
	emailDetails.RoomID = bkngInfo.RoomID
	emailDetails.Lang = bkngInfo.Lang

	emailDetails.Helps = "NORMAL"
	if bkngInfo.Diff == "H" {
		emailDetails.Helps = "HARD"
	}

	emailDetails.PlayRoom1 = "ΟΧΙ"
	if bkngInfo.PlayRoom1 == 1 {
		emailDetails.PlayRoom1 = "ΝΑΙ"
	}

	emailDetails.EscRoom1 = "ΟΧΙ"
	if bkngInfo.EscRoom1 == 1 {
		emailDetails.EscRoom1 = "ΝΑΙ"
	}

	emailDetails.Age = "ΝΑΙ"
	if bkngInfo.Age != "s" {
		emailDetails.Age = "ΟΧΙ"
	}

	switch bkngInfo.LearnedUs {
	case "go":
		emailDetails.LearnUs = "Google"
	case "fb":
		emailDetails.LearnUs = "Facebook"
	case "fr":
		emailDetails.LearnUs = "Friends"
	case "si":
		emailDetails.LearnUs = "Escape sites"
	case "ta":
		emailDetails.LearnUs = "Tripadvisor"
	case "mn":
		emailDetails.LearnUs = "The Mansion"
	case "bo":
		emailDetails.LearnUs = "The BookStore"
	case "co":
		emailDetails.LearnUs = "Conservator"
	case "lf":
		emailDetails.LearnUs = "Leaflet/ Card"
	default:
		bkngInfo.LearnedUs = "fr"
		emailDetails.LearnUs = "Facebook"
	}

	emailTemplate := "bkng_"
	emailTemplateAdmin, emailTemplateClient := emailTemplate, emailTemplate
	if bkngInfo.StatusID == 1 {
		emailTemplateAdmin = fmt.Sprintf(`%s%s`, emailTemplateAdmin, "admin")
		emailTemplateClient = fmt.Sprintf(`%s%s%d`, emailTemplateClient, "rm", bkngInfo.RoomID)
	} else {
		emailDetails.Lang = "en"
		emailTemplateAdmin = "edit_admin"
		emailTemplateClient = "edit"
	}

	emails.SendEmail(db, emailDetails, emailTemplateClient, bkngInfo.Email, bkngInfo.Lang)
	emails.SendEmail(db, emailDetails, emailTemplateAdmin, adminUsername, "en")

	return nil
}

// RemoveBooking model func.
func RemoveBooking(ctx context.Context, db *sql.DB, adminUsername string, bkngID int) (updated bool, err error) {
	// Booking model for cancellation email.
	type rmBkngDetails struct {
		Nme      string
		Email    string
		Dte      string
		RoomName string
	}

	query := fmt.Sprintf(`
		select 
			bkng.nme, email, dte, rl.nme as room_name
		from bkng
		join sess using(sess_id)
		join rm on rm.rm_id = sess.rm_id
		join rm_lcl rl on rm.rm_id = rl.rm_id
		where bkng_id = %[1]d
		and bkng.bkng_sts_id <> 3
		and lcl = (select lang from bkng where bkng_id =  %[1]d)
		and removed = '0000-00-00 00:00:00'`, bkngID)

	ctx, cancel := context.WithTimeout(ctx, dbQueryTimeout*time.Second)
	defer cancel()

	var rmBkngEmail = rmBkngDetails{}
	row := db.QueryRowContext(ctx, query)

	err = row.Scan(&rmBkngEmail.Nme, &rmBkngEmail.Email, &rmBkngEmail.Dte, &rmBkngEmail.RoomName)
	if err != nil {
		log.WithFields(logrus.Fields{"Query": misc.Unspace(query), "package": "models.bookings", "method": "RemoveBooking"}).Error(err)
		err = errors.Wrap(err, "database query error:")
	}

	tx, err := db.Begin()
	if err != nil {
		log.WithFields(logrus.Fields{"package": "models.account", "method": "UpdatePersonalDetails"}).Error(err)
		return false, err
	}

	query = `insert into bkng_rmvd 
	select bkng_id,sess_id,bkng_sts_id,plr_id,plr_num,bkng_tme,dte,current_timestamp(), nme, phone, email, lang, age, plr_lvl, code, team_nme, learned_us, src, play_1, diff, notes, esc_1, gdpr, viewed, rm_id from bkng where bkng_id=?;`

	res, err := tx.ExecContext(ctx, query, bkngID)
	if err != nil {
		tx.Rollback()
		log.WithFields(logrus.Fields{"Query": misc.Unspace(query), "package": "models.bookings", "method": "RemoveBooking"}).Error(err)
		err = errors.Wrap(err, "database query error:")
		return
	}

	query = `delete from bkng where bkng_id=?;`

	res, err = tx.ExecContext(ctx, query, bkngID)
	if err != nil {
		tx.Rollback()
		log.WithFields(logrus.Fields{"Query": misc.Unspace(query), "package": "models.bookings", "method": "RemoveBooking"}).Error(err)
		err = errors.Wrap(err, "database query error:")
		return
	}

	ra, err := res.RowsAffected()
	updated = ra == 1

	if err := tx.Commit(); err != nil {
		log.WithFields(logrus.Fields{"package": "models.bookings", "method": "RemoveBooking"}).Error(err)
		return false, err
	}

	//hub.broadcast <- []byte("DIRTY")
	go misc.Broadcast("DIRTY")

	// Fill struct with needed email details
	emailDetails := emails.EmailBookingDetails{}

	dateParse, _ := time.Parse("2006-01-02 15:04:05", rmBkngEmail.Dte)
	dateFormat := dateParse.Format(layoutISO)

	emailDetails.Name = rmBkngEmail.Nme
	emailDetails.Email = rmBkngEmail.Email
	emailDetails.Date = dateFormat
	emailDetails.RoomName = rmBkngEmail.RoomName

	emails.SendEmail(db, emailDetails, "cncl_rm", fmt.Sprintf("%v", rmBkngEmail.Email), "en")
	emails.SendEmail(db, emailDetails, "cncl_rm_admin", adminUsername, "en")

	return true, nil
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~  I-FRAME MODELS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// PubGetBookings per room.
func PubGetBookings(ctx context.Context, db *sql.DB, roomID int) (bookings []PubBooking, err error) {

	query := fmt.Sprintf(`
					select
						sess_id, dte
					from bkng 
					join sess using(sess_id)
					where sess.sess_id = bkng.sess_id 
					and sess.rm_id = %d
					and removed = '0000-00-00 00:00:00' 
					and dte >= now()`, roomID)

	ctx, cancel := context.WithTimeout(ctx, dbQueryTimeout*time.Second)
	defer cancel()

	rows, err := db.QueryContext(ctx, query)
	if err != nil {
		log.WithFields(logrus.Fields{"Query": misc.Unspace(query), "package": "models", "method.bookings": "PubGetBookings"}).Error(err)
		err = errors.Wrap(err, "database query error:")
		return
	}
	defer rows.Close()

	for rows.Next() {
		var bkng = PubBooking{}

		err = rows.Scan(&bkng.SessionID, &bkng.Dte)
		if err != nil {
			log.WithFields(logrus.Fields{"Query": misc.Unspace(query), "package": "models.bookings", "method": "PubGetBookings"}).Error(err)
			err = errors.Wrap(err, "database scan error:")
			return
		}

		bookings = append(bookings, bkng)
	}

	return
}

// PubBookSession model func.
func PubBookSession(ctx context.Context, db *sql.DB, bkngInfo bindings.BookSessionRequest, adminUsername string) (success bool, err error) {
	query := fmt.Sprintf(`
		insert into bkng (sess_id, nme, phone, email, plr_num, lang, plr_lvl, age, diff, code, learned_us,
						  team_nme, play_1, esc_1, notes, dte, bkng_sts_id, gdpr, src, rm_id)
		values(%d, '%s', '%s', '%s', %d, '%s', '%s', '%s', '%s', %s, '%s', %s, %v, %v, %s, '%s', %d, %v, '%s', %d)
		ON DUPLICATE KEY UPDATE rm_id=rm_id;`,
		bkngInfo.SessionID, bkngInfo.Nme, bkngInfo.Phone, bkngInfo.Email, bkngInfo.PlrNum, bkngInfo.Lang, bkngInfo.PlrLvl, bkngInfo.Age, bkngInfo.Diff,
		misc.NullString{bkngInfo.Code}, bkngInfo.LearnedUs, misc.NullString{bkngInfo.TeamNme}, bkngInfo.PlayRoom1, bkngInfo.EscRoom1, misc.NullString{bkngInfo.Notes}, bkngInfo.Dte, bkngInfo.StatusID, bkngInfo.GDPR, bkngInfo.Src, bkngInfo.RoomID)

	res, err := db.Exec(query)
	if err != nil {
		qry := misc.Unspace(query)
		log.WithFields(logrus.Fields{"Query": qry, "package": "models.bookings", "method": "PubBookSession"}).Error(err)
		err = errors.Wrap(err, "database query error:")
		return
	}

	ra, err := res.RowsAffected()
	success = ra == 1
	if err != nil || !success {
		return
	}

	//hub.broadcast <- []byte("DIRTY")
	go misc.Broadcast("DIRTY")

	// Fill struct with needed email details
	emailDetails := emails.EmailBookingDetails{}

	dateParse, _ := time.Parse("2006-01-02 15:04:05", bkngInfo.Dte)
	dateFormat := dateParse.Format(layoutISO)

	emailDetails.Date = dateFormat
	emailDetails.Name = bkngInfo.Nme
	emailDetails.Email = bkngInfo.Email
	emailDetails.Phone = bkngInfo.Phone
	emailDetails.Players = bkngInfo.PlrNum
	emailDetails.RoomName = bkngInfo.RoomName
	emailDetails.Level = bkngInfo.PlrLvl
	emailDetails.Notes = *bkngInfo.Notes
	emailDetails.TeamName = *bkngInfo.TeamNme
	emailDetails.RoomID = bkngInfo.RoomID
	emailDetails.Lang = bkngInfo.Lang

	emailDetails.Helps = "NORMAL"
	if bkngInfo.Diff == "H" {
		emailDetails.Helps = "HARD"
	}

	emailDetails.PlayRoom1 = "ΟΧΙ"
	if bkngInfo.PlayRoom1 == 1 {
		emailDetails.PlayRoom1 = "ΝΑΙ"
	}

	emailDetails.EscRoom1 = "ΟΧΙ"
	if bkngInfo.EscRoom1 == 1 {
		emailDetails.EscRoom1 = "ΝΑΙ"
	}

	emailDetails.Age = "ΝΑΙ"
	if bkngInfo.Age != "s" {
		emailDetails.Age = "ΟΧΙ"
	}

	switch bkngInfo.LearnedUs {
	case "go":
		emailDetails.LearnUs = "Google"
	case "fb":
		emailDetails.LearnUs = "Facebook"
	case "fr":
		emailDetails.LearnUs = "Friends"
	case "si":
		emailDetails.LearnUs = "Escape sites"
	case "ta":
		emailDetails.LearnUs = "Tripadvisor"
	case "mn":
		emailDetails.LearnUs = "The Mansion"
	case "bo":
		emailDetails.LearnUs = "The BookStore"
	case "co":
		emailDetails.LearnUs = "Conservator"
	case "lf":
		emailDetails.LearnUs = "Leaflet/ Card"
	default:
		bkngInfo.LearnedUs = "fr"
		emailDetails.LearnUs = "Facebook"
	}

	emailTemplate := "bkng_"
	emailTemplateAdmin, emailTemplateClient := emailTemplate, emailTemplate
	if bkngInfo.StatusID == 1 {
		emailTemplateAdmin = fmt.Sprintf(`%s%s`, emailTemplateAdmin, "admin")
		emailTemplateClient = fmt.Sprintf(`%s%s%d`, emailTemplateClient, "rm", bkngInfo.RoomID)
	} else {
		emailDetails.Lang = "en"
		emailTemplateAdmin = "edit_admin"
		emailTemplateClient = "edit"
	}

	emails.SendEmail(db, emailDetails, emailTemplateClient, bkngInfo.Email, bkngInfo.Lang)
	emails.SendEmail(db, emailDetails, emailTemplateAdmin, adminUsername, "en")

	return
}
