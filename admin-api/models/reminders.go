package models

import (
	"context"
	"database/sql"
	"fmt"
	"time"

	misc "khakibee/admin-api/misc"
	emails "khakibee/admin-api/misc/emails"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

// GetReminderBookingDetails.
func GetReminderBookingDetails(db *sql.DB, timeCondition string) (ebds []emails.EmailBookingDetails, err error) {
	var timeQuery string

	if timeCondition == "3HB" {
		timeQuery = "AND DATE_ADD(now(), INTERVAL 180 MINUTE) BETWEEN DATE_SUB(dte, INTERVAL 15 MINUTE) AND dte"
	} else if timeCondition == "24HB" {
		timeQuery = "AND DATE_ADD(now(), INTERVAL 60*24 MINUTE) BETWEEN DATE_SUB(dte, INTERVAL 60 MINUTE) AND dte"
	} else if timeCondition == "24HA" {
		timeQuery = "AND DATE_SUB(now(), INTERVAL 60*24 MINUTE) BETWEEN dte AND DATE_ADD(dte, INTERVAL 60 MINUTE)"
	} else {
		err = errors.New("Empty time condition variable!!")
		return
	}

	query := fmt.Sprintf(`
		select 
			rm_lcl.nme, email, bkng.plr_num, lang, age, plr_lvl, notes, dte, bkng.nme, diff, phone, team_nme, learned_us, src
		from bkng
		join sess on sess.sess_id = bkng.sess_id
		join rm on rm.rm_id = sess.rm_id
		join rm_lcl on rm_lcl.rm_id = rm.rm_id
		where bkng.bkng_sts_id <> 3
		and rm_lcl.lcl = 'el'
		and removed = '0000-00-00 00:00:00'
		%[1]s`, timeQuery)

	ctx, cancel := context.WithTimeout(context.Background(), dbQueryTimeout*time.Second)
	defer cancel()

	rows, err := db.QueryContext(ctx, query)
	if err != nil {
		log.WithFields(logrus.Fields{"Query": misc.Unspace(query), "package": "models", "method.reminders": "GetReminderBookingDetails"}).Error(err)
		err = errors.Wrap(err, "database query error:")
		return
	}
	defer rows.Close()

	for rows.Next() {
		var ebd = emails.EmailBookingDetails{}

		err = rows.Scan(&ebd.RoomName, &ebd.Email, &ebd.Players, &ebd.Lang, &ebd.Age, &ebd.Level, &ebd.Notes, &ebd.Date, &ebd.Name, &ebd.Helps, &ebd.Phone, &ebd.TeamName, &ebd.LearnUs, &ebd.Src)
		if err != nil {
			log.WithFields(logrus.Fields{"Query": misc.Unspace(query), "package": "models.reminders", "method": "GetReminderBookingDetails"}).Error(err)
			err = errors.Wrap(err, "database scan error:")
			return
		}

		ebds = append(ebds, ebd)
	}

	return
}
