package models

import (
	"context"
	"database/sql"
	"fmt"
	misc "khakibee/admin-api/misc"
	"time"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

// Room model
type Room struct {
	RoomID int    `json:"room_id"`
	Name   string `json:"nme"`
	Img    string `json:"img"`
	Active int    `json:"active"`
}

// PubRoom model
type PubRoom struct {
	RoomID   int    `json:"room_id"`
	Name     string `json:"nme"`
	Img      string `json:"img"`
	Addr     string `json:"addr"`
	Map      string `json:"map"`
	PlrNum   string `json:"plr_num"`
	Dur      string `json:"duration"`
	RegionID int    `json:"rgn_id"`
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  ADMIN  MODELS  ~~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// SelectRooms returns selected rooms.
func SelectRooms(ctx context.Context, db *sql.DB, permitedRoom *int) (rooms []Room, err error) {
	var roomCondition string

	if permitedRoom != nil {
		roomCondition = fmt.Sprintf(" and rm_id = %d", *permitedRoom)
	}

	query := fmt.Sprintf(`
		select rm_id, nme, img, active
 		from rm
 		join rm_lcl using(rm_id)
 		where lcl = 'el'
		%s`, roomCondition)

	ctx, cancel := context.WithTimeout(ctx, dbQueryTimeout*time.Second)
	defer cancel()

	rows, err := db.QueryContext(ctx, query)
	if err != nil {
		log.WithFields(logrus.Fields{"Query": misc.Unspace(query), "package": "models", "method.rooms": "SelectRooms"}).Error(err)
		err = errors.Wrap(err, "database query error:")
		return
	}
	defer rows.Close()

	for rows.Next() {
		var rm = Room{}

		err = rows.Scan(&rm.RoomID, &rm.Name, &rm.Img, &rm.Active)
		if err != nil {
			log.WithFields(logrus.Fields{"Query": misc.Unspace(query), "package": "models.rooms", "method": "SelectRooms"}).Error(err)
			err = errors.Wrap(err, "database scan error:")
			return
		}

		rooms = append(rooms, rm)
	}

	return
}

// EditRoom update rooms activeness.
func EditRoom(ctx context.Context, db *sql.DB, active int, roomID int) error {
	tx, err := db.Begin()
	if err != nil {
		log.WithFields(logrus.Fields{"package": "models.rooms", "method": "EditRoom"}).Error(err)
		return errors.Wrap(err, "database query error:")
	}

	query := fmt.Sprintf(`update rm set active = %d WHERE rm_id = %d`, active, roomID)

	if _, err := tx.Exec(query); err != nil {
		tx.Rollback()
		qry := misc.Unspace(query)
		log.WithFields(logrus.Fields{"Query": qry, "package": "models.rooms", "method": "EditRoom"}).Error(err)
		return errors.Wrap(err, "database query error:")
	}

	if err = tx.Commit(); err != nil {
		log.WithFields(logrus.Fields{"package": "models.rooms", "method": "EditRoom"}).Error(err)
		return errors.Wrap(err, "database query error:")
	}

	return nil
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~  I-FRAME MODELS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// PubSelectRooms returns the active rooms.
func PubSelectRooms(ctx context.Context, db *sql.DB, lng *string) (rooms []PubRoom, err error) {
	lang := "el"
	if lng != nil {
		lang = *lng
	}

	query := fmt.Sprintf(`
						select 
							rm.rm_id, rl.nme, rm.img, addr, map, plr_num, dur, rgn.rgn_id
						from rm
						join rm_lcl rl using(rm_id)
						join rgn on rgn.rgn_id = rm.rgn_id
						where rm.active
						and rgn.active
						and rl.lcl = '%[1]s'`, lang)

	ctx, cancel := context.WithTimeout(ctx, dbQueryTimeout*time.Second)
	defer cancel()

	rows, err := db.QueryContext(ctx, query)
	if err != nil {
		log.WithFields(logrus.Fields{"Query": misc.Unspace(query), "package": "models", "method.rooms": "PubSelectRooms"}).Error(err)
		err = errors.Wrap(err, "database query error:")
		return
	}
	defer rows.Close()

	for rows.Next() {
		var rm = PubRoom{}

		err = rows.Scan(&rm.RoomID, &rm.Name, &rm.Img, &rm.Addr, &rm.Map, &rm.PlrNum, &rm.Dur, &rm.RegionID)
		if err != nil {
			log.WithFields(logrus.Fields{"Query": misc.Unspace(query), "package": "models.rooms", "method": "PubSelectRooms"}).Error(err)
			err = errors.Wrap(err, "database scan error:")
			return
		}

		rooms = append(rooms, rm)
	}

	return
}
