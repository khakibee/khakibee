import React from 'react';


import en_tr from "./locales/en.json";
import el_tr from "./locales/el.json";

const translations = {
  en: en_tr,
  el: el_tr
}

const fallbackLng = 'el'

// This function takes a component...
function withTranslation(WrappedComponent) {
  // ...and returns another component...
  return class extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        lng: fallbackLng,
        translation: translations[fallbackLng]
      };

      this.setLang = this.setLang.bind(this);
    }

    setLang(lng) {
      if (!translations.hasOwnProperty(lng)) {
        lng = fallbackLng
      }
      this.setState({
        lng: lng,
        translation: translations[lng]
      })
    }

    render() {
      return <WrappedComponent setLang={this.setLang} lng={this.state.lng} tr={this.state.translation} {...this.props} />;
    }
  };
}

export default withTranslation;
