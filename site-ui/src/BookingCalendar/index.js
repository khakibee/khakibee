import React, { Component } from 'react';
import Websocket from 'react-websocket';
import withTranslation from './withTranslation';
import BkngDate from './BkngDate';
import BkngForm from './BkngForm';
import {
  Button, Row, Col, Container, CardImg,
  Card, CardGroup, CardHeader, CardBody, CardFooter,
  Nav, NavItem, NavLink, TabContent, TabPane, Alert
} from 'reactstrap';

const API = 'http://localhost:3001/api';

class Calendar extends Component {
  constructor(props, context) {
    super(props, context);

    let currentDate = new Date();
    currentDate.setHours(0, 0, 0, 0);

    var initSteps = {
      0: { descr: "room", title: {}, address: {}, map: "" },
      1: { descr: "date", date: currentDate },
      2: { descr: "form", bkng: {} },
      3: { descr: "booking", last: true }
    }

    this.props.setLang('el')

    this.state = {
      steps: { ...initSteps },
      rooms: [],
      step: 0,
      currentDate: currentDate,
      selectedMonth: { showingDate: "", days: {} },
      monthOffset: 0,
      sessions: [],
      info: '',
      dirty: false,
      selectedRoom: {}
    };
  }


  handleWS = (data) => {
    this.setState({ dirty: data === "DIRTY" });

    if (data === "DIRTY") {
      var room_id = this.state.steps[0].room_id
      fetch(API + "/bookings/" + room_id, {
        method: 'GET',
      })
        .then(response => {
          if (response.status === 200)
            return response.json();
          else
            console.log("Error:", response.status)
        })
        .then(data => {
          let bookings = data
          var monthOffset = this.state.monthOffset;
          var date = new Date(this.state.currentDate.getFullYear(), this.state.currentDate.getMonth() + monthOffset)
          var sessions = this.state.allSessions

          let chosenDate = this.state.steps[1].date

          let selectedMonth = getMonth(date, room_id, sessions, bookings, this.props.lng)
          let selectedDay = Object.values(selectedMonth.days).find((day) => day.date.getTime() === chosenDate.getTime())
          this.setState({
            sessions: selectedDay ? selectedDay.sess : [],
            allBookings: bookings,
            selectedMonth: selectedMonth,
            dirty: false
          })
        })
    }
  }

  render() {
    const websocket = <Websocket url='ws://localhost:8080' onMessage={this.handleWS} />
    const tr = this.props.tr
    const lng = this.props.lng
    const step = this.state.step
    const steps = { ...this.state.steps }
    var sessions = []
    var rooms = []
    var monthDays = []


    const selectedMonth = this.state.selectedMonth;
    const showingDate = selectedMonth.showingDate;

    this.state.directStep === 1 && this.selecteRoom(this.props.room)

    switch (step) {
      case 0:
        rooms = this.state.rooms.map((room) =>
          <Col sm="4" key={room.room_id} className="mt-3">
            <Card onClick={() => this.selecteRoom(room.room_id)}>
              {room.room_id === 3 && <div className="ribbon-online-cat"><span>{tr.ribbon}</span></div>}
              <CardImg top className="photos clickable" width="100%" src={room.img_url} />
              <CardBody className="text-center">
                <Col lg={12} className="text-center space-bottom">
                  <Button className={"btn btn-brand btn-client btn-block"}>
                    <span>{lng === "en" ? room.title2 : room.title}</span>
                  </Button>
                </Col>
                <div className="d-flex justify-content-between">
                  <small><i className="icon fa fa-hourglass-half" /> <b>{room.min_duration}</b></small>
                  <small><i className="icon fa fa-users" /> <b>{room.plr_num}</b></small>
                </div>
              </CardBody>
            </Card>
          </Col>)
        break;
      case 1:
        sessions = this.state.sessions.map((sess, idx) => <Button key={idx} className="btn btn-vimeo" size="lg" block onClick={() => this.openBooking(sess)}>{sess.tmStarting.substring(0, sess.tmStarting.length - 3)}</Button>)
        monthDays = Object.values(selectedMonth.days).map((day, idx) =>
          <BkngDate key={idx} date={day.date} sessions={day.sess} currentDate={this.state.currentDate} click={this.showSessions} selectedDay={steps[1].date} />)

        break;
    }

    const calendarCard = <Card style={{ "borderRightStyle": "none" }}>
      <CardHeader className="bg-paradox">
        <Row className="d-flex justify-content-between">
          <Col className="col-2 pl-2"><a disabled={this.state.monthOffset === 0} onClick={() => this.handleMonthOffset("left")}><i className="fa fa-arrow-left clickable" /> </a></Col>
          <Col className="text-center col-8"><strong>{showingDate}</strong></Col>
          <Col className="col-2"><a disabled={this.state.monthOffset === 1} onClick={() => this.handleMonthOffset("right")}> <i className="fa fa-arrow-right clickable" />  </a></Col>
        </Row>
        <hr />
        <Row className="d-flex">
          <div className="weekday-col">{tr.week.mo}</div>
          <div className="weekday-col">{tr.week.tu}</div>
          <div className="weekday-col">{tr.week.we}</div>
          <div className="weekday-col">{tr.week.th}</div>
          <div className="weekday-col">{tr.week.fr}</div>
          <div className="weekday-col">{tr.week.sa}</div>
          <div className="weekday-col">{tr.week.su}</div>
        </Row>
      </CardHeader>
      <CardBody>
        <Row>
          {monthDays}
        </Row>
      </CardBody>
    </Card>

    var sessionsCard = <Card style={{ "borderRightStyle": "none" }}>
      <CardHeader className="text-center bg-paradox">
        <strong>{steps[1].date && new Date(steps[1].date).toLocaleDateString(this.state.lang, { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' })}</strong>
      </CardHeader>
      <CardBody>
        {!sessions.length && <Alert color="danger">{tr.noRoomsAvail}</Alert>}
        {sessions}
      </CardBody>
    </Card>

    var formCard = <BkngForm
      selectedDay={steps[1].date}
      event={steps[1].session}
      room={this.state.selectedRoom.room_id}
      onComplete={this.bookingComplete}
      handleBack={this.handleBack}
      tr={tr}
      lang={lng}
      title={steps[0].title} />

    var finishCard = <>
      <Card>
        <CardHeader className="text-center rounded-0">
          <h4>{tr.bookingInfo}</h4>
        </CardHeader>
        <Alert color="success">
          <p className="alert-heading"> <i className="fa fa-check-circle icon"></i> {tr.checkEmail}</p>
          <p> <i className="fa fa-exclamation-circle icon"></i> <strong>{tr.important}</strong>
            {tr.infoAboutMail}
          </p>
        </Alert>
        <CardBody>
          <div>
            <h5>{steps[0].title[lng]}, {tr.form.players} {steps[2].bkng.plr_num} {"  -  "}{steps[1].date.toLocaleString(lng, { weekday: 'long', day: "numeric", month: 'long', year: 'numeric', hour: 'numeric', minute: 'numeric', hour12: false }
            )}</h5>
            <i size="lg" className="fa fa-map-pin icon cursor-pointer" onClick={(e) => window.open(steps[0].map, "_blank")}> {tr.weAreHere}</i>
            <hr />
            <div>

              <Row>
                <Col md="8">
                  <Row>
                    <Col>{tr.address} </Col>
                    <Col className="h5"><b>{steps[0].address[lng]}</b></Col>
                  </Row>
                  <Row>
                    <Col>{tr.form.name} </Col>
                    <Col><b>{steps[2].bkng.name}</b></Col>
                  </Row>
                  <Row>
                    <Col>{tr.form.email} </Col>
                    <Col><b>{steps[2].bkng.email_addr}</b></Col>
                  </Row>
                  <Row>
                    <Col>{tr.form.mobile} </Col>
                    <Col><b>{steps[2].bkng.mob_num}</b></Col>
                  </Row>
                </Col>

                <Col md="4">
                  <img className="t-shirt" src="https://res.cloudinary.com/hwrkhvisl/image/upload/v1589025884/Paradox%20Project/cpuuzawnt72bshegn11h.jpg"></img>
                </Col>

              </Row>
            </div>
          </div>
        </CardBody>
        <CardFooter className="text-center"> <Button className="btn-paradox" onClick={() => this.setState({ step: 0 })}>{tr.newBooking}</Button></CardFooter>
      </Card>
    </>

    var stepTabs = Object.keys(steps).map((stp, idx) => {
      var className = ""
      if (step === idx) {
        className = "active"
      } else if (steps[step].last || (idx > step && !steps[idx - 1].choice)) {
        className = "disabled"
      }

      return <NavItem key={idx}>
        <NavLink
          className={className}
          onClick={() => this.setState({ step: idx })}
        >
          <div className="tab-step">
            <div className="fa-stack fa-1x">
              <strong className="fa-stack-1x">{idx + 1}.</strong>
            </div>
            {tr.tabs[steps[stp].descr]}</div>
          {steps[stp].choice ? <div className="text-muted">{steps[stp].choice}</div> : <br />}
        </NavLink>
      </NavItem>
    })




    return (
      <Container>
        <Nav tabs justified>
          {stepTabs}
        </Nav>

        <TabContent activeTab={step} className="d-flex justify-content">
          <TabPane tabId={0}>
            <Row>
              {rooms}
            </Row>
          </TabPane>
          <TabPane tabId={1}>
            <Row>
              <br />
              <CardGroup>
                {calendarCard}
                {sessionsCard}
              </CardGroup>
            </Row>
          </TabPane>
          <TabPane tabId={2}>
            <br />
            {formCard}
          </TabPane>
          <TabPane tabId={3}>
            {finishCard}
          </TabPane>
        </TabContent>
        { websocket}
      </Container >
    );
  }

  componentDidMount() {
    fetch(API + "/rooms", {
      method: 'GET',
    })
      .then(response => {
        if (response.status === 200)
          return response.json();
        else
          console.log("Error:", response.status)
      })
      .then(data => {
        if (data) {
          this.setState({
            rooms: data,
            directStep: this.props.step
          })
        }
      })
  }

  selecteRoom = (room_id) => {
    this.setState({ directStep: -1 })

    var fetchSessions = fetch(API + "/gamesess", {
      method: 'GET',
    })
      .then(response => {
        if (response.status === 200)
          return response.json();
        else
          console.log("Error:", response.status)
      })

    var fetchBookings = fetch(API + "/bookings/" + room_id, {
      method: 'GET',
    })
      .then(response => {
        if (response.status === 200)
          return response.json();
        else
          console.log("Error:", response.status)
      })

    Promise.all([fetchSessions, fetchBookings]).then(([sessions, bookings]) => {
      let steps = this.state.steps
      let room = this.state.rooms.find(rm => rm.room_id === room_id)

      steps[0].title = {
        el: room.title,
        en: room.title2,
      }
      steps[0].address = {
        el: room.addr,
        en: room.addr2,
      }
      steps[0].map = room.map
      steps[0].choice = steps[0].title[this.props.lng]
      steps[0].room_id = room.room_id
      steps[1].choice = null

      let date = new Date(this.state.currentDate.getFullYear(), this.state.currentDate.getMonth())
      let selectedMonth = getMonth(date, room_id, sessions, bookings, this.props.lng)

      this.setState({
        allSessions: sessions,
        allBookings: bookings,
        selectedMonth: selectedMonth,
        step: this.state.step + 1,
        steps: steps,
        selectedRoom: room,
        monthOffset: 0,
      })
    })
  }

  handleBack = () => {
    this.setState({
      step: this.state.step > 0 ? this.state.step - 1 : 0
    })
  }

  bookingComplete = (bkng) => {
    let steps = this.state.steps
    steps[2] = { ...steps[2], bkng: bkng }
    this.setState({
      steps: steps,
      step: this.state.step + 1

    })
  }

  openBooking = (session) => {
    let step = this.state.step
    let steps = this.state.steps

    let tmStarting = session.tmStarting.split(":")
    steps[step].date.setHours(tmStarting[0], tmStarting[1])

    let options = { day: 'numeric', month: 'numeric', year: 'numeric', hour: 'numeric', minute: 'numeric', hour12: false };
    steps[step].choice = steps[step].date.toLocaleString('el', options)

    steps[step].session = session

    this.setState({
      step: this.state.step + 1,
      steps: steps
    })
  }

  showSessions = (sessions, date) => {
    let step = this.state.step
    let steps = this.state.steps

    steps[step].choice = null
    steps[step].date = date
    steps[step].session = null

    this.setState({
      sessions: sessions,
      steps: steps
    })
  }

  handleMonthOffset = (direction) => {
    var monthOffset = direction === "right" ? this.state.monthOffset + 1 : this.state.monthOffset - 1;
    var date = new Date(this.state.currentDate.getFullYear(), this.state.currentDate.getMonth() + monthOffset)
    var room_id = this.state.steps[0].room_id
    var sessions = this.state.allSessions
    var bookings = this.state.allBookings

    let selectedMonth = getMonth(date, room_id, sessions, bookings, this.props.lng)
    this.setState({
      monthOffset: monthOffset,
      selectedMonth: selectedMonth
    });
  }
}

function getMonth(date, room, sessions, bookings, lang) {
  const monthDatesObj = {};
  const showingDate = new Intl.DateTimeFormat(lang, { month: 'long', year: 'numeric' }).format(date);
  const daysInMonth = new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate();
  const daysInPrevMonth = new Date(date.getFullYear(), date.getMonth(), 0).getDate();
  var firstDayInMonth = new Date(date.getFullYear(), date.getMonth(), 1).getDay();
  firstDayInMonth = firstDayInMonth === 0 ? 7 : firstDayInMonth //making Sunday the last day of the week
  const curMonth = date.getMonth();
  const curYear = date.getFullYear();
  var day = 0;
  var dayIndx = 0;
  var dayObj = {};

  let threeHoursFromNow = new Date(Date.now() + 3 * 60 * 60 * 1000)
  let twoHoursFromNow = new Date(Date.now() + 2 * 60 * 60 * 1000)
  let threeWeeksTime = new Date(Date.now() + 21 * 24 * 60 * 60 * 1000)

  for (let i = 0; i < 6; i++) { //weeks
    if (dayIndx > daysInMonth) break;
    for (let j = 1; j <= 7; j++) { //days
      dayIndx++;
      let month = curMonth;
      let year = curYear;

      day = j + (i * 7) - firstDayInMonth + 1;
      if (i === 0 && j < firstDayInMonth) {
        day = daysInPrevMonth + day;
        if (curMonth === 0) year = curYear - 1;
        month = curMonth === 0 ? 11 : curMonth - 1;
      } else if (day > daysInMonth) {
        day = day - daysInMonth
        if (curMonth === 12) year = curYear + 1;
        month = curMonth === 12 ? 1 : curMonth + 1;
      }
      let wkday = j === 7 ? 0 : j
      let room_sessions = sessions[wkday].filter((session) => {
        if (session.room_id !== room)
          return false
        let date = `${year}-${month + 1 > 9 ? month + 1 : "0" + (month + 1)}-${day > 9 ? day : "0" + day} ${session.tmStarting}`
        let is_open = bookings.findIndex(booking => booking.game_sess_id === session.game_sess_id && booking.tmStarting === session.tmStarting && booking.date === date) < 0

        //formating date for date constructor
        date = new Date(date.replace(' ', 'T') + '+03:00')

        return is_open && date >= (room !== 3 ? threeHoursFromNow : twoHoursFromNow) && date <= threeWeeksTime
      })

      dayObj[`${day}/${month}/${year}`] = { date: new Date(year, month, day), sess: room_sessions }
    }
  }
  monthDatesObj.showingDate = showingDate;
  monthDatesObj.days = dayObj;
  return monthDatesObj;
}


export default withTranslation(Calendar)