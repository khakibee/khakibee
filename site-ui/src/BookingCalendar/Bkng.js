import React, { Component } from 'react';
import { Card, Button, Form } from 'react-bootstrap';



export default class Bkng extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {};
  }

  render() {

    return (
      <Card>
        <Card.Body>
          <Card.Header className="text-center">
            {this.props.selectedDay}
          </Card.Header>
          <Form>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Email address</Form.Label>
              <Form.Control type="email" placeholder="Enter email" />
              <Form.Text className="text-muted">
                We'll never share your email with anyone else.
              </Form.Text>
            </Form.Group>

            <Form.Group controlId="formBasicPassword">
              <Form.Label>Password</Form.Label>
              <Form.Control type="password" placeholder="Password" />
            </Form.Group>
            <Form.Group controlId="formBasicChecbox">
              <Form.Check type="checkbox" label="Check me out" />
            </Form.Group>
            <Button variant="primary" type="submit">
              Submit
            </Button>
          </Form>
        </Card.Body>
      </Card>
    )
  }

}
