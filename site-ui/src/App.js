import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { Container } from 'react-bootstrap';
import Companies from './components/Companies';
import Offers from './components/Offers';
import Dashboard from './components/Dashboard';
import Navigation from './components/Navigation';
import Register from './components/Register';
import Login from './components/Login';
import CompanyRooms from './components/CompanyRooms';
import Room from './components/Room';
import Footer from './components/Footer';

// Styles
import './custom.scss';

class App extends Component {

  render() {
    return (
      <>
        <Router>
          <div>
            <Navigation />
            <hr />
            <Container>
              <Switch>
                <Route exact path='/' component={Dashboard} />
                <Route path='/companies/:title' component={CompanyRooms} />
                <Route path='/companies' component={Companies} />
                <Route path='/offers' component={Offers} />
                <Route path='/register' component={Register} />
                <Route path='/login' component={Login} />
                <Route path='/room/:title' component={Room} />
              </Switch>
            </Container>
          </div>
        </Router>
        <Footer />
      </>
    );
  }
}

export default App;
