import React, { Component } from 'react';
import { Button, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default class SpecificRooms extends Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
        };
    }

    render() {

        let room = this.props.room;

        return (

            <Card border="warning" style={{ width: '14.5rem' }}>
                <Link to={"/room/" + room.perm}>
                    <Card.Img variant="top" src={room.img_url} />
                </Link>
                <Card.Body>
                    <Card.Title> {room.title} </Card.Title> <hr />
                    <Card.Text> <br />
                        <i className="fas fa-map-marker-alt"> {room.addr}</i> <br />
                        <i className="fas fa-hourglass-start" paddingright='5px'> {room.dur} 'Ωρες</i> <br />
                        <i className="fas fa-users"> {room.plr_num} Άτομα</i>
                    </Card.Text>
                    <Link to={"/room/" + room.perm} >
                        <Button className="text-center" variant="danger" >
                            Δες ακόμη ...
                    </Button>
                    </Link>
                </Card.Body>
            </Card>
        )
    }
}