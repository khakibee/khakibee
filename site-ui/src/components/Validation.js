function ValidationInput(value, rule, type) {
    this.value = value;
    this.validate = () => rule(this.value);
    this.type = type ? type : "string";
}

export default ValidationInput;