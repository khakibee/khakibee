import React, { Component } from 'react';
import { InputGroup, FormControl, Col, Card, Button } from 'react-bootstrap';
import DatePicker from 'react-date-picker';


export default class Research extends Component {
    state = {
        date: new Date(),
    }

    render() {
        return (
            <Col>
                <Card bg="dark" text="light">
                    <Card.Header>
                        <h4 className="fas fa-search"> Αναζήτηση</h4>
                    </Card.Header>
                    <Card.Body>

                        <h6>Περιοχή</h6>
                        <InputGroup className="mb-4">
                            <InputGroup.Prepend>
                                <InputGroup.Text id="basic-addon1">
                                    <i className="fas fa-map-marker-alt"></i>
                                </InputGroup.Text>
                            </InputGroup.Prepend>
                            <FormControl
                                as="select"
                                aria-describedby="basic-addon1"
                            >
                                <option>Αθήνα</option>
                                <option>Θεσσαλονίκη</option>
                                <option>Πάτρα</option>
                            </FormControl>
                        </InputGroup>

                        <h6>Ημερομηνία</h6>
                        <InputGroup className="mb-3">
                            <InputGroup.Prepend>
                                <InputGroup.Text id="basic-addon1">
                                    <i className="far fa-calendar"></i>
                                </InputGroup.Text>
                            </InputGroup.Prepend>
                            <DatePicker
                                className="form-control"
                                selected={this.state.date}
                                onChange={date => this.setState({ date })}
                            />
                        </InputGroup>

                        <h6>Κατηγορία</h6>
                        <InputGroup className="mb-4">
                            <InputGroup.Prepend>
                                <InputGroup.Text id="basic-addon1">
                                    <i className="fab fa-kickstarter-k"></i>
                                </InputGroup.Text>
                            </InputGroup.Prepend>
                            <FormControl
                                as="select"
                                aria-describedby="basic-addon1"
                            >
                                <option>Όλα</option>
                                <option>Με Ηθοποιό</option>
                                <option>Δράσης</option>
                                <option>Τρομακτικά</option>
                                <option>Θέμα Ταινιών</option>
                            </FormControl>
                        </InputGroup>

                        <h6>Κυνηγοί Μυστηρίου</h6>
                        <InputGroup className="mb-4" >
                            <InputGroup.Prepend >
                                <InputGroup.Text id="basic-addon1" >
                                    <i className="fas fa-user-secret"></i>
                                </InputGroup.Text>
                            </InputGroup.Prepend>
                            <FormControl
                                as="select"
                                aria-describedby="basic-addon1"
                            >
                                <option>1 Παίκτης</option>
                                <option>2 Παίκτες</option>
                                <option>3 Παίκτες</option>
                                <option>4 Παίκτες</option>
                                <option>5 Παίκτες</option>
                                <option>6 Παίκτες</option>
                                <option>7 Παίκτες</option>
                                <option>8 Παίκτες</option>
                                <option>9 Παίκτες</option>
                                <option>10 Παίκτες</option>
                                <option>10+ Παίκτες</option>
                            </FormControl>
                        </InputGroup>

                        <Button variant="warning" block>
                            Ανανέωσε
                    </Button>

                    </Card.Body>
                </Card>

            </Col>
        )
    }
}