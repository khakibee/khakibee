import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Navbar, Nav, Form, Button } from 'react-bootstrap';


export default class Navigation extends Component {

    render() {
        return (
            <Navbar bg="dark" variant="dark" style={{ height: 80 }}>
                <Navbar.Brand href="/" style={{ fontSize: 25 }}><b>EscapeLand</b></Navbar.Brand>
                <Nav className="mr-auto">
                    <Link to={'/companies'} className="nav-link" style={{ fontSize: 20 }}>Εταιρίες</Link>
                    <Link to={'/offers'} className="nav-link" style={{ fontSize: 20 }}>Προσφορές</Link>
                </Nav>

                <Form>
                    <Nav>
                        <Link to="/register">
                            <Button variant="info" className="mr-sm-2">
                                <span>Εγγραφή</span>
                            </Button>
                        </Link>
                        <Link to="/login">
                            <Button variant="warning">
                                <span>Είσοδος</span>
                            </Button>
                        </Link>
                    </Nav>
                </Form>
            </Navbar>
        )
    }
}