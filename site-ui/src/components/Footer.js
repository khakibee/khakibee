import React, { Component } from 'react';


export default class Footer extends Component {

    render() {
        return (
            <footer className="footer my-3 sticky-footer">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12 d-flex justify-content-between">
                            <div><small className="text-muted">EscapeLand Thesis 2020 - v1.0.5</small></div>
                            <div>
                                <button className="btn btn-link small text-muted ml-2">Privacy + Terms</button>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        )
    }
}