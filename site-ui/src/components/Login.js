import React, { Component } from 'react';
import { Button, Container, Form, Row, Col } from 'react-bootstrap';


export default class Login extends Component {

    constructor(props) {
        super(props);

        this.state = {
            username: "",
            password: ""
        }
        this.handleChange = this.handleChange.bind(this);
        this.validationForm = this.validationForm.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    validationForm() {
        return this.state.username.length > 0 &&
            this.state.password.length > 0;
    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    handleSubmit = (e) => {
        e.preventDefault();

        console.log("Yes");
    }

    render() {
        return (

            <Container className="Login" style={{ marginTop: 80 }} >
                <Row>
                    <Col />
                    <Col xs={8}>
                        <h2>Sign In</h2>
                        <Form
                            className="loginForm"

                            style={{ marginTop: 25 }} >

                            <Form.Group>
                                <Form.Label>Όνομα χρήστη</Form.Label>
                                <Form.Control
                                    type="username"
                                    name="username"
                                    placeholder="Enter username"
                                    value={this.state.username}
                                    onChange={e => this.handleChange(e)}
                                />
                            </Form.Group>

                            <Form.Group>
                                <Form.Label>Κωδικός</Form.Label>
                                <Form.Control
                                    type="password"
                                    name="password"
                                    placeholder="*********"
                                    value={this.state.password}
                                    onChange={e => this.handleChange(e)}
                                />
                            </Form.Group>

                            <Button variant="warning" type="submit" disabled={!this.validationForm()} onClick={e => this.handleChange(e)}>
                                Είσοδος
                        </Button>

                        </Form>
                    </Col>
                    <Col />
                </Row>
            </Container>
        )
    }
}