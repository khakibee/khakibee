import React, { Component } from 'react';
import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default class CompanyRooms extends Component {

  render() {
    return (
      <Card bg="light" text="dark" size="sm">
        <Link to={"/room/" + this.props.room.permalink}>
          <Card.Img variant="top" src={this.props.room.img_url} />
        </Link>
        <Card.Body>
          <Card.Title className="text-center"><u>{this.props.room.title}</u></Card.Title>
          <hr />
          <Card.Text>
            <Link to={"/room/" + this.props.room.permalink}>
              {this.props.room.descr}
            </Link>
          </Card.Text>
          <Card.Text>
            <i className="fas fa-map-marker-alt"> {this.props.room.addr}</i> <br />
            <i className="fas fa-hourglass-start" paddingright='5px'> {this.props.room.min_duration}</i> <br />
            <i className="fas fa-users"> {this.props.room.plr_num} Άτομα</i>
          </Card.Text>
          <div className="text-right">
            <Link to={"/room/" + this.props.room.permalink} className="text-center">
              <Button variant="warning" size="sm">
                Δες ακόμη ...
                        </Button>
            </Link>
          </div>
        </Card.Body>
      </Card>
    );
  }
}
