import React, { Component } from 'react';
import { FormControl, InputGroup, Row, Container, Col, CardColumns } from 'react-bootstrap';
import DatePicker from 'react-date-picker';
import RoomListItem from './RoomListItem';
import ControlledCarousel from './ControlledCarousel';

const API = 'http://localhost:3001/api';


export default class Dashboard extends Component {
  state = {
    date: new Date(),
    filter: "",
    rooms: []
  }

  render() {
    let roomsCol = this.state.rooms.map(room => {
      if (this.state.filter.length === 0)
        return <RoomListItem key={room.id} room={room} />
      else if (room.title.toLowerCase().includes(this.state.filter.toLowerCase()))
        return <RoomListItem key={room.id} room={room} />

      return null
    })
    return (
      <Container>
        <ControlledCarousel />
        <br />
        <Row className="pr-15">
          <Col md={12}>
            <InputGroup className="mb-3">
              <InputGroup.Prepend>
                <InputGroup.Text id="basic-addon1">
                  <i className="fas fa-search"></i>
                </InputGroup.Text>
              </InputGroup.Prepend>
              <FormControl id="search" placeholder="Search..." onChange={e => { this.setState({ filter: e.target.value }) }} />
            </InputGroup>
          </Col>

          <Col md={4}>
            <InputGroup className="mb-3">
              <InputGroup.Prepend>
                <InputGroup.Text id="basic-addon1"><i className="fas fa-map-marker"></i></InputGroup.Text>
              </InputGroup.Prepend>
              <FormControl
                as="select"
                aria-describedby="basic-addon1"
              >
                <option>Location</option>
                <option>...</option>
              </FormControl>
            </InputGroup>
          </Col>
          <Col md={4}>
            <InputGroup className="mb-3">
              <InputGroup.Prepend>
                <InputGroup.Text id="basic-addon1">@</InputGroup.Text>
              </InputGroup.Prepend>
              <DatePicker
                className="form-control"
                selected={this.state.date}
                onSelect={date => this.setState({ date })}
              />
            </InputGroup>
          </Col>

          <Col md={4}>
            <InputGroup className="mb-3">
              <InputGroup.Prepend>
                <InputGroup.Text id="basic-addon1"><i className="fab fa-kickstarter-k"></i></InputGroup.Text>
              </InputGroup.Prepend>
              <FormControl
                as="select"
                aria-describedby="basic-addon1"
              >
                <option>Category</option>
                <option>...</option>
              </FormControl>
            </InputGroup>
          </Col>

          {roomsCol &&
            <Col md={12}>
              <CardColumns>
                {roomsCol}
              </CardColumns>
            </Col>}
        </Row>
      </Container>
    );
  }

  componentDidMount() {
    fetch(API + "/rooms", {
      method: 'GET',
    })
      .then(response => {
        if (response.status === 200)
          return response.json();
        else
          console.log("Error:", response.status)
      })
      .then(data => {
        if (data) {
          this.setState({
            rooms: data
          })
        }
      })
  }

}
