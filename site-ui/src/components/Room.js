import React, { Component } from 'react';
import { Container, Row, Tabs, Tab, Breadcrumb } from 'react-bootstrap';
import Research from './Research';
import axios from 'axios';
import Calendar from '../BookingCalendar';
import Loader from 'react-loader-spinner';

class Room extends Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
      room: this.props.room ? this.props.room : [],
      loading: true
    };
  }

  componentDidMount() {
    const title = this.props.match.params.title
    axios.get(`http://localhost:3001/api/cmpnrooms/${title}`)
      .then(room => {
        this.setState({
          room: room.data[0],
          loading: false
        })

      })
      .catch(err => console.log(err))
  }

  render() {
    const permalink = this.props.match.params.title

    return (
      <div style={{ marginTop: 50 }} >
        <Container>
          <Row>
            {/* <div className="col-sm-4">
              <Research />
            </div> */}
            <div className="col-sm-12">
              <div className="containerRoom" aligin="right">
                <Breadcrumb>
                  <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
                  <Breadcrumb.Item href="/companies">Εταιρίες</Breadcrumb.Item>
                  {/* <Breadcrumb.Item href={"/companies/" + this.state.room.perm}>{this.state.room.nme}</Breadcrumb.Item> */}
                  <Breadcrumb.Item active>{permalink}</Breadcrumb.Item>
                </Breadcrumb>

                <h1><u>{this.state.room.title}</u></h1>
                <br /> <br />
                <Tabs
                  id="controlled-tab"
                  activeKey={this.state.key}
                  onSelect={key => this.setState({ key })}
                >
                  <Tab eventKey="home" title="Γενικά">
                    <br />
                    <ul>
                      <li>{this.state.room.descr}</li>
                    </ul>
                  </Tab>
                  <Tab eventKey="feautures" title="Χαρακτηριστικά">
                    <br />
                    <ul>
                      <li>Η διάρκεια του παιχνιδιού είναι: {this.state.room.dur} ώρα.</li>
                    </ul>
                  </Tab>
                </Tabs>

                {this.state.loading
                  ?
                  <div className="text-center">
                    <Loader type="Oval" color="#d4c90b" height={250} width={250} />
                  </div>
                  : <div> <br /> <Calendar step={1} room={this.state.room.room_id} /> <br /> </div>}
              </div>
            </div>
          </Row>
        </Container>
      </div>
    )
  }
}

export default Room;
