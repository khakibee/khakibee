import React, { Component } from 'react';
import { Button, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default class SpecificCompanies extends Component {

    render() {

        const company = this.props.company;
        return (
            <Card border="dark" >
                <Link to={"/companies/" + company.permalink} >
                    <Card.Img variant="top" src={company.imgUrl} />
                </Link>
                <Card.Body>
                    <Card.Title>{company.nme}</Card.Title>
                    <hr />
                    <Card.Text>
                        <i className="fas fa-map-marker-alt"> {company.addr}</i><br />
                    </Card.Text>
                    <Link to={"/companies/" + company.permalink} >
                        <Button className="text-center" variant="warning">
                            Δες ακόμη ...
                                </Button>
                    </Link>
                </Card.Body>
            </Card>
        )
    }
}
