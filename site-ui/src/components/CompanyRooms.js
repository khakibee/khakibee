import React, { Component } from 'react';
import { Row, Breadcrumb } from 'react-bootstrap';
import Research from './Research';
import axios from 'axios';
import Calendar from '../BookingCalendar';
import Loader from 'react-loader-spinner';
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";


export default class CompanyRooms extends Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            companyRooms: [],
            companyName: '',
            loading: true
        };
    }

    componentDidMount() {
        const title = this.props.match.params.title
        axios.get(`http://localhost:3001/api/companies/${title}`)
            .then(companyRooms => {
                this.setState({
                    companyRooms: companyRooms.data,
                    companyName: companyRooms.data[0].nme,
                    loading: false
                })
            })
            .catch(err => console.log(err))
    }

    render() {
        const { companyName, loading } = this.state;

        return (
            <div className="d-flex justify-content-between flex-fill mt-5" >
                <br />
                <Row>
                    <div className="col-sm-4">
                        <Research />
                    </div>

                    <div className="col-sm-8">

                        <Breadcrumb>
                            <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
                            <Breadcrumb.Item href="/companies">Εταιρίες</Breadcrumb.Item>
                            <Breadcrumb.Item active>{companyName}</Breadcrumb.Item>
                        </Breadcrumb>

                        <p className="h2 ml-2"><b>{companyName}</b></p>

                        {loading
                            ?
                            <div className="text-center">
                                <Loader type="Oval" color="#d4c90b" height={250} width={250} />
                            </div>
                            : <div> <br /> <Calendar /> <br /> </div>}
                        {/* <CardColumns> {companyWithRooms.rooms.map(room => { */}
                        {/* return <SpecificRooms key={room.room_id} room={room} /> */}
                        {/*  })} </CardColumns>} */}

                    </div>
                </Row>
            </div >
        )
    }
}