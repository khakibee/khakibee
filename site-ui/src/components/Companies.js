import React, { Component } from 'react';
import SpecificCompanies from './SpecificCompanies';
import Research from './Research';
import axios from 'axios';
import { ButtonToolbar, Button, Container, Row, CardColumns } from 'react-bootstrap';
import Loader from 'react-loader-spinner';
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";


const tags = {
    1: ['action', 'actor', 'scary', 'all'],
    2: ['movies', 'others', 'action', 'all'],
    3: ['scary', 'movies', 'others', 'all']
}

export default class Companies extends Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            companies: [],
            tagedCompanies: [],
            loading: true
        };
    }

    componentDidMount() {
        axios.get(`http://localhost:3001/api/companies`)
            .then(res => {
                this.setState({
                    companies: res.data,
                    tagedCompanies: res.data,
                    loading: false
                })
            })
            .catch(err => console.log(err))
    }

    render() {

        var companyCol = this.state.tagedCompanies.map(company => {
            return <SpecificCompanies key={company.cmpn_id} company={company} />
        })

        return (
            <div style={{ marginTop: 20 }} >
                <br />
                <Container>
                    <Row>
                        {/* <div className="col-sm-4">
                            <Research />
                        </div> */}

                        <div className="col-sm-12">
                            <div className="container-tags">
                                {!this.state.loading && <ButtonToolbar className="ml-2">
                                    <Button variant="secondary mr-2 mt-2" onClick={() => this.handleTags("all")}>Όλα</Button>
                                    <Button variant="success mr-2 mt-2" onClick={() => this.handleTags("action")}>Δράσης</Button>
                                    <Button variant="warning mr-2 mt-2" onClick={() => this.handleTags("actor")}>Με Ηθοποιό</Button>
                                    <Button variant="danger mr-2 mt-2" onClick={() => this.handleTags("scary")}>Τρομακτικά</Button>
                                    <Button variant="info mr-2 mt-2" onClick={() => this.handleTags("movies")}>θέμα Ταινιών</Button>
                                    <Button variant="dark mr-2 mt-2" onClick={() => this.handleTags("others")}>Άλλα</Button>
                                </ButtonToolbar>}
                            </div>

                            <br />  <br />

                            {this.state.loading ? <>
                                <div className="text-center">
                                    <Loader type="Oval" color="#d4c90b" height={200} width={200} />
                                </div>
                            </>
                                : <CardColumns> {companyCol} </CardColumns>}

                        </div>
                    </Row>
                </Container>

            </div >
        )
    }

    handleTags = (tag) => {
        var tagedCmpns = this.state.companies.filter(company => tags[company.cmpn_id].includes(tag))
        this.setState({ tagedCompanies: tagedCmpns })
    }
}