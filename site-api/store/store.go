package store

import (
	"database/sql"
)

// Store :
type Store struct {
	db *sql.DB
	DB *sql.DB
}

// NewStore : creates new handler
func NewStore(db *sql.DB) *Store {
	return &Store{db: db, DB: db}
}
