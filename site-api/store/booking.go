package store

import (
	"errors"
	"fmt"

	"khakibee/site-api/util"
)

//NewBooking :Get Bookings Per Room.
func (s *Store) NewBooking(b Booking) error {
	query := `
	INSERT INTO bkng (game_sess_id, name, mob_num, email_addr, plr_num, lang, plr_lvl, 
						  age, diff, code, learned_us, notes, date, bkng_status_id) 
	VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);
	`

	if res, err := s.db.Exec(query, b.GmSessID, b.Name, b.Mob, b.Email, b.PlrNum, b.Lang, b.PlrLvl, b.Age, b.Dfcl, b.Code, b.LrndUs, b.Notes, b.Date, b.BkngStsID); err != nil {
		return util.SQLError(err, query, b.GmSessID, b.Name, b.Mob, b.Email, b.PlrNum, b.Lang, b.PlrLvl, b.Age, b.Dfcl, b.Code, b.LrndUs, b.Notes, b.Date, b.BkngStsID)
	} else if ra, err := res.RowsAffected(); err != nil {
		return util.SQLError(err, query, b.GmSessID, b.Name, b.Mob, b.Email, b.PlrNum, b.Lang, b.PlrLvl, b.Age, b.Dfcl, b.Code, b.LrndUs, b.Notes, b.Date, b.BkngStsID)
	} else if ra == 0 {
		err := errors.New("Zero rows affected")
		return util.SQLError(err, query, b.GmSessID, b.Name, b.Mob, b.Email, b.PlrNum, b.Lang, b.PlrLvl, b.Age, b.Dfcl, b.Code, b.LrndUs, b.Notes, b.Date, b.BkngStsID)
	}

	return nil
}

//GetGameSessions :Get Bookings Per Room.
func (s *Store) GetGameSessions() ([]Booking, error) {
	bkS := []Booking{}

	query := `
	SELECT game_sess_id, room_id, title, wk_day, tmStarting, duration
	FROM game_sess 
		INNER JOIN room USING(room_id)
	WHERE room.enable = 1 
		AND game_sess.enable = 1
	ORDER BY wk_day ASC`

	rows, err := s.db.Query(query)
	defer rows.Close()
	if err != nil {
		return nil, util.SQLError(err, query)
	}

	for rows.Next() {
		bk := Booking{}
		if err = rows.Scan(&bk.GmSessID, &bk.RoomID, &bk.Title, &bk.WkDay, &bk.Time, &bk.Duration); err != nil {
			return nil, util.SQLError(err, query)
		}
		bkS = append(bkS, bk)
	}

	return bkS, nil
}

//GetBookingsByRoom :Get Bookings Per Room.
func (s *Store) GetBookingsByRoom(roomID string) ([]Booking, error) {
	bkS := []Booking{}

	var roomIDCond string
	if roomID != "0" {
		roomIDCond = fmt.Sprintf("AND game_sess.room_id = %s;", roomID)
	} else {
		roomIDCond = fmt.Sprintf("AND game_sess.room_id != %s;", roomID)
	}

	query := `
	SELECT 
		game_sess_id, bkng_id, room_id, bkng_status_id, 
		date, wk_day, tmStarting, diff, plr_lvl, plr_num,
		name, mob_num, email_addr, notes, lang, age, code, learned_us
	FROM bkng
		INNER JOIN game_sess USING(game_sess_id)
	WHERE removed = '0000-00-00 00:00:00'
	` + roomIDCond

	rows, err := s.db.Query(query)
	defer rows.Close()
	if err != nil {
		return nil, util.SQLError(err, query, roomID)
	}

	for rows.Next() {
		bk := Booking{}
		if err = rows.Scan(&bk.GmSessID, &bk.BkngID, &bk.RoomID, &bk.BkngStsID,
			&bk.Date, &bk.WkDay, &bk.Time, &bk.Dfcl, &bk.PlrLvl, &bk.PlrNum,
			&bk.Name, &bk.Mob, &bk.Email, &bk.Notes, &bk.Lang, &bk.Age, &bk.Code, &bk.LrndUs); err != nil {
			return nil, util.SQLError(err, query, roomID)
		}
		bkS = append(bkS, bk)
	}

	return bkS, nil
}
