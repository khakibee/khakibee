package store

//Booking :
type Booking struct {
	GmSessID  int `json:"game_sess_id"`
	BkngID    int
	BkngStsID int
	RoomID    int `json:"room_id"`

	PlrNum   string `json:"plr_num"`
	Title    string `json:"title"`
	Notes    *string
	Date     string `json:"date"`
	Time     string `json:"tmStarting"`
	Duration string `json:"duration"`
	Name     string
	Mob      string
	Email    string
	Lang     *string
	Age      string
	Code     *string
	Dfcl     string
	PlrLvl   string
	LrndUs   string
	TeamName string
	WkDay    int `json:"wk_day"`
}

//Room :
type Room struct {
	RoomID    int    `json:"room_id"`
	Title     string `json:"title"`
	Title2    string `json:"title2"`
	Duration  string `json:"dur"`
	MinDur    string `json:"min_duration"`
	Img       string `json:"img_url"`
	PlrNum    string `json:"plr_num"`
	Descr     string `json:"descr"`
	Addr      string `json:"addr"`
	Addr2     string `json:"addr2"`
	Map       string `json:"map"`
	Enabled   bool   `json:"enabled"`
	Permalink string `json:"permalink"`
}

//Company :
type Company struct {
	Nme  string `json:"nme"`
	Addr string `json:"addr"`
	City string `json:"city"`
	Perm string `json:"perm"`
	Img  string `json:"img_url"`
	CmpnRm
}

//CmpnRm :
type CmpnRm struct {
	CompanyID int    `json:"cmpn_id"`
	RoomID    int    `json:"room_id"`
	Permalink string `json:"permalink"`
	ImgURL    string `json:"imgUrl"`
	Plr       string `json:"plr_num"`
	Actor     bool   `json:"actor"`
	Title     string `json:"title"`
	Descr     string `json:"descr"`
	Duration  string `json:"dur"`
}
