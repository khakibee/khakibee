package store

import (
	"fmt"
	"khakibee/site-api/util"
)

// GetCompanies :
func (s *Store) GetCompanies() ([]Company, error) {
	cS := []Company{}

	query := `
		SELECT cmpn_id, nme, permalink, addr, city, img_url
		FROM cmpn 
		ORDER BY rand();`

	rows, err := s.db.Query(query)
	defer rows.Close()
	if err != nil {
		return nil, util.SQLError(err, query)
	}

	for rows.Next() {
		c := Company{}
		if err = rows.Scan(&c.CompanyID, &c.Nme, &c.Permalink, &c.Addr, &c.City, &c.ImgURL); err != nil {
			return nil, util.SQLError(err, query)
		}
		cS = append(cS, c)
	}

	return cS, nil
}

// GetCompany :
func (s *Store) GetCompany(permalink string) ([]Company, error) {
	cS := []Company{}

	query := fmt.Sprintf(`
	SELECT cmpn_id, nme, c.permalink, addr, city, c.img_url, room_id, title,
		rm.permalink, descr, dur, actor, rm.img_url, plr_num
	FROM cmpn c
	LEFT JOIN rm USING(cmpn_id)
	WHERE c.permalink = '%s'
	ORDER BY room_id`, permalink)

	rows, err := s.db.Query(query)
	defer rows.Close()
	if err != nil {
		return nil, util.SQLError(err, query)
	}

	for rows.Next() {
		c := Company{}
		if err = rows.Scan(&c.CompanyID, &c.Nme, &c.Permalink, &c.Addr, &c.City, &c.ImgURL, &c.RoomID, &c.Title, &c.Perm, &c.Descr, &c.Duration, &c.Actor, &c.Img, &c.Plr); err != nil {
			return nil, util.SQLError(err, query)
		}
		cS = append(cS, c)
	}

	return cS, nil
}

// GetCmpnRooms :
func (s *Store) GetCmpnRooms() ([]CmpnRm, error) {
	rS := []CmpnRm{}

	query := `
		SELECT *
		FROM rm 
		ORDER BY rand()`

	rows, err := s.db.Query(query)
	defer rows.Close()
	if err != nil {
		return nil, util.SQLError(err, query)
	}

	for rows.Next() {
		rm := CmpnRm{}
		if err = rows.Scan(&rm.RoomID, &rm.CompanyID, &rm.Title, &rm.Descr, &rm.Permalink, &rm.Duration, &rm.ImgURL, &rm.Actor, &rm.Plr); err != nil {
			return nil, util.SQLError(err, query)
		}
		rS = append(rS, rm)
	}

	return rS, nil
}

// GetCmpnRoom :
func (s *Store) GetCmpnRoom(permalink string) ([]CmpnRm, error) {
	rm := []CmpnRm{}

	query := fmt.Sprintf(`
		SELECT rm.cmpn_id, rm.room_id, rm.title, rm.descr, rm.permalink, rm.dur, rm.img_url, rm.actor, rm.plr_num
		FROM rm 
		LEFT JOIN cmpn USING(cmpn_id)
		WHERE rm.permalink = '%s'
		ORDER BY room_id`, permalink)

	rows, err := s.db.Query(query)
	defer rows.Close()
	if err != nil {
		return nil, util.SQLError(err, query)
	}

	for rows.Next() {
		r := CmpnRm{}
		if err = rows.Scan(&r.CompanyID, &r.RoomID, &r.Title, &r.Descr, &r.Permalink, &r.Duration, &r.ImgURL, &r.Actor, &r.Plr); err != nil {
			return nil, util.SQLError(err, query)
		}
		rm = append(rm, r)
	}

	return rm, nil
}
