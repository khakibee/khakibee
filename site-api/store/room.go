package store

import (
	"khakibee/site-api/util"
)

// GetRooms :
func (s *Store) GetRooms() ([]Room, error) {
	rS := []Room{}

	query := `
	SELECT room_id, title, title2, duration, min_duration, img_url, plr_num, description, addr, addr2, map, permalink
	FROM room 
	WHERE enable = 1;`

	rows, err := s.db.Query(query)
	defer rows.Close()
	if err != nil {
		return nil, util.SQLError(err, query)
	}

	for rows.Next() {
		r := Room{}
		if err = rows.Scan(&r.RoomID, &r.Title, &r.Title2, &r.Duration, &r.MinDur, &r.Img, &r.PlrNum, &r.Descr, &r.Addr, &r.Addr2, &r.Map, &r.Permalink); err != nil {
			return nil, util.SQLError(err, query)
		}
		rS = append(rS, r)
	}

	return rS, nil
}
