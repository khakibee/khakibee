package handler

import (
	"encoding/json"
	"khakibee/site-api/config"
	"khakibee/site-api/store"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"

	"khakibee/site-api/util"
)

// BookSession :Create Game Sessions for a Rooms.
func (h *Handler) BookSession(w http.ResponseWriter, r *http.Request) {
	var b booking
	_ = json.NewDecoder(r.Body).Decode(&b)

	var emailLang, difficulty, learnUs, date, teamName string

	GmSessID, err := strconv.Atoi(b.GameSessID)
	if err != nil {
		util.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	sb := store.Booking{
		GmSessID:  GmSessID,
		Name:      b.Name,
		Mob:       b.MobNum,
		Email:     b.EmailAddr,
		PlrNum:    b.PlrNum,
		PlrLvl:    b.PlrLvl,
		Lang:      b.Lang,
		Age:       b.Age,
		Dfcl:      b.Diff,
		Code:      b.Code,
		LrndUs:    b.LrnedUs,
		Notes:     b.Notes,
		Date:      b.Date,
		BkngStsID: b.Status,
	}

	err = h.s.NewBooking(sb)
	if err != nil {
		util.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	go util.Broadcast("DIRTY")

	if *b.Lang == "en" {
		emailLang = "ΑΓΓΛΙΚΑ"
	} else {
		emailLang = "ΕΛΛΗΝΙΚΑ"
	}

	if b.Diff == "H" {
		difficulty = "HARD"
	} else {
		difficulty = "NORMAL"
	}

	switch b.LrnedUs {
	case "go":
		learnUs = "Google"
	case "fb":
		learnUs = "Facebook"
	case "fr":
		learnUs = "Friends"
	case "si":
		learnUs = "Escape sites"
	case "ta":
		learnUs = "Tripadvisor"
	case "mn":
		learnUs = "The Mansion"
	case "bo":
		learnUs = "The BookStore"
	case "co":
		learnUs = "Conservator"
	case "lf":
		learnUs = "Leaflet/ Card"
	default:
		b.LrnedUs = "fr"
		learnUs = "Facebook"
	}

	if sb.Date != "" {
		date = sb.Date
		sb.Date = "'" + sb.Date + "'"
	} else {
		sb.Date = "null"
	}

	const (
		dateISO = "02/01/2006"
		timeISO = "15:04"
	)
	dateParse, _ := time.Parse("2006-01-02T15:04:05.000Z", date)
	dateFormat := dateParse.Format(dateISO)
	timeFormat := dateParse.Format(timeISO)

	playRoom1 := "ΟΧΙ"
	if b.PlayRoom1 {
		playRoom1 = "ΝΑΙ"
	}

	escRoom1 := "ΟΧΙ"
	if b.EscRoom1 {
		escRoom1 = "ΝΑΙ"
	}

	age := "ΝΑΙ"
	if b.Age != "s" {
		age = "ΟΧΙ"
	}

	emailData := map[string]interface{}{
		"Name":      b.Name,
		"Email":     b.EmailAddr,
		"Phone":     b.MobNum,
		"Date":      dateFormat,
		"Players":   b.PlrNum,
		"Hour":      timeFormat,
		"Helps":     difficulty,
		"LearnUs":   learnUs,
		"TeamName":  teamName,
		"RoomName":  b.RoomName,
		"RoomName2": b.RoomName2,
		"Level":     b.PlrLvl,
		"Notes":     b.Notes,
		"PlayRoom1": playRoom1,
		"EscRoom1":  escRoom1,
		"Lang":      emailLang,
		"RoomID":    b.RoomID,
		"Age":       age,
	}

	emailTemplateAdmin := "bkng_admin"

	util.SendEmail(h.s.DB, emailData, templateMap[b.Status][*b.Lang][b.RoomID], b.EmailAddr, *b.Lang)

	util.SendEmail(h.s.DB, emailData, emailTemplateAdmin, config.Conf.Email.Username, "en")

}

//GetGameSessions :Get All Game Sessions.
func (h *Handler) GetGameSessions(w http.ResponseWriter, r *http.Request) {
	gsS, err := h.s.GetGameSessions()
	if err != nil {
		util.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	var wkDt [7][]store.Booking
	for _, gs := range gsS {
		wkDt[gs.WkDay] = append(wkDt[gs.WkDay], gs)
	}

	err = json.NewEncoder(w).Encode(wkDt)
	if err != nil {
		util.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

//GetBookings :Get Bookings Per Room.
func (h *Handler) GetBookings(w http.ResponseWriter, r *http.Request) {
	roomID := mux.Vars(r)["id"]

	bkS, err := h.s.GetBookingsByRoom(roomID)
	if err != nil {
		util.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = json.NewEncoder(w).Encode(bkS)
	if err != nil {
		util.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

//GetRooms :Get All Enable Rooms.
func (h *Handler) GetRooms(w http.ResponseWriter, r *http.Request) {
	rS, err := h.s.GetRooms()
	if err != nil {
		util.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = json.NewEncoder(w).Encode(rS)
	if err != nil {
		util.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

//GetCompanies :Get All Companies.
func (h *Handler) GetCompanies(w http.ResponseWriter, r *http.Request) {
	cS, err := h.s.GetCompanies()
	if err != nil {
		util.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = json.NewEncoder(w).Encode(cS)
	if err != nil {
		util.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

//GetCompany :Get specific company.
func (h *Handler) GetCompany(w http.ResponseWriter, r *http.Request) {
	permalink := mux.Vars(r)["permalink"]

	c, err := h.s.GetCompany(permalink)
	if err != nil {
		util.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = json.NewEncoder(w).Encode(c)
	if err != nil {
		util.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

//GetCmpnRooms :Get All Companies.
func (h *Handler) GetCmpnRooms(w http.ResponseWriter, r *http.Request) {
	crS, err := h.s.GetCmpnRooms()
	if err != nil {
		util.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = json.NewEncoder(w).Encode(crS)
	if err != nil {
		util.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

//GetCmpnRoom :Get room of specific company.
func (h *Handler) GetCmpnRoom(w http.ResponseWriter, r *http.Request) {
	permalink := mux.Vars(r)["permalink"]

	cr, err := h.s.GetCmpnRoom(permalink)
	if err != nil {
		util.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = json.NewEncoder(w).Encode(cr)
	if err != nil {
		util.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
