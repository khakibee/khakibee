package router

import (

	"github.com/gorilla/mux"
	"khakibee/site-api/router/middleware"
)

// New initialises the router
func New() *mux.Router{
	
	r := mux.NewRouter()
	r.Use(middleware.CommonMiddleware)
	r.Use(middleware.HandleOptionsMiddleware)

	return r
}