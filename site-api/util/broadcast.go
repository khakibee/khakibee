package util

import (
	"fmt"
	"net/http"
	"strings"

	"khakibee/site-api/config"
)

// Broadcast :
func Broadcast(msg string) {
	url := config.Conf.Websocket + "/broadcast"
	method := "POST"

	payload := strings.NewReader(fmt.Sprintf("{\"msg\":\"%s\"}", msg))

	client := &http.Client{}
	req, err := http.NewRequest(method, url, payload)
	if err != nil {
		fmt.Println(err)
	}

	req.Header.Add("Content-Type", "application/json")

	res, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
	}
	defer res.Body.Close()

}
