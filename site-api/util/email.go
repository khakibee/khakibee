package util

import (
	"bytes"
	"database/sql"
	"fmt"
	"html/template"
	"khakibee/site-api/config"
	"net/smtp"
	"strings"
)

// SendEmail :
func SendEmail(db *sql.DB, emailData map[string]interface{}, emailType string, receiver string, lang string) error {
	var receivers = []string{receiver}
	sub, body, err := getEmail(db, emailType, emailData, lang)
	if err != nil {
		fmt.Println("GetEmail error")
		return err
	}

	ok, err := send(receivers, sub, body)
	if err != nil {
		fmt.Println("Send Email error", err)
	}
	fmt.Println("Email STATUS: [", ok, "], TYPE ==> ", emailType, ", RECEIVER ==> ", receiver)

	return nil
}

func send(receivers []string, subject string, body string) (bool, error) {
	var from = config.Conf.Email.Username
	var pass = config.Conf.Email.Password
	var host = config.Conf.Email.Host

	smtpAuth := smtp.PlainAuth("", from, pass, host)

	receiversStr := strings.Join(receivers, " ")
	mime := "MIME-version: 1.0;\nContent-Type: text/html; charset=\"UTF-8\";\n\n"
	header := "From: Paradox Project \r\n" +
		"To: " + receiversStr + "\r\n" +
		"Subject: " + subject + "\n"
	msg := []byte(header + mime + "\n" + body)
	addr := "smtp.gmail.com:587"

	if err := smtp.SendMail(addr, smtpAuth, from, receivers, msg); err != nil {
		return false, err
	}
	return true, nil
}

func getEmail(db *sql.DB, emailID string, bkngData interface{}, Lang string) (subject string, body string, err error) {
	query := `
	SELECT subject, body FROM email 
		WHERE code = ? AND lang = ?
	`

	row := db.QueryRow(query, emailID, Lang)
	row.Scan(&subject, &body)
	if err != nil {
		err = SQLError(err, query, emailID, Lang)
	}

	body, err = parseTemplate(body, bkngData)
	if err != nil {
		err = SQLError(err, query, emailID, Lang)
	}

	return
}

func parseTemplate(templateFileName string, bkngData interface{}) (html string, err error) {

	t := template.New("html")
	t, err = t.Parse(templateFileName)
	if err != nil {
		return
	}
	buf := new(bytes.Buffer)
	err = t.Execute(buf, bkngData)
	html = buf.String()
	return
}
