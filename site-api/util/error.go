package util

import (
	"net/http"
	"log"
	"fmt"
)

// Error :like http.Error() but with JSON format instead of plain text
func Error(w http.ResponseWriter, err string, code int) {
	w.WriteHeader(code)
	fmt.Fprintf(w, `{"error":%q}`, err)
	log.Println(err)
}

// SQLError :like http.Error() but with JSON format instead of plain text
func SQLError(err error, query string, prms ...interface{}) error {
	return fmt.Errorf("db query error { query: %s, params: %v, error: %q}", query, prms, err)
}