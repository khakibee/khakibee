package main

import (
	"fmt"
	"log"
	"net/http"

	"khakibee/site-api/config"
	"khakibee/site-api/db"
	"khakibee/site-api/handler"
	"khakibee/site-api/router"
	"khakibee/site-api/store"
)

type contextKey string

func main() {

	d := db.New()
	defer d.Close()

	r := router.New()
	api := r.PathPrefix("/" + config.Conf.Subpath).Subrouter()

	s := store.NewStore(d)
	h := handler.New(s)

	api.HandleFunc("/booking", h.BookSession).Methods(http.MethodPost, http.MethodOptions)
	api.HandleFunc("/gamesess", h.GetGameSessions).Methods(http.MethodGet, http.MethodOptions)
	api.HandleFunc("/bookings/{id}", h.GetBookings).Methods(http.MethodGet, http.MethodOptions)
	api.HandleFunc("/rooms", h.GetRooms).Methods(http.MethodGet, http.MethodOptions)
	api.HandleFunc("/companies", h.GetCompanies).Methods(http.MethodGet, http.MethodOptions)
	api.HandleFunc("/companies/{permalink}", h.GetCompany).Methods(http.MethodGet, http.MethodOptions)
	api.HandleFunc("/cmpnrooms", h.GetCmpnRooms).Methods(http.MethodGet, http.MethodOptions)
	api.HandleFunc("/cmpnrooms/{permalink}", h.GetCmpnRoom).Methods(http.MethodGet, http.MethodOptions)

	// Server port
	p := config.Conf.Port
	log.Println("Server started at", p)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", p), r))

}
