var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan'); 
require('dotenv').config();
var db = require('./db');
var routes = require('./routes');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use("",routes);
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});


// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};


  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

db.pool.connect(function (err, client, done) {
  if (err) {
    console.log('not able to connect ' + err)
    return
  }
    client.query('SELECT $1::varchar AS my_first_query',['DB escapeLand connected...'], function (err, result) {
    done()
    if (err) {
      console.log(err)
    }
    console.log(result.rows[0].my_first_query)
  })
})

module.exports = app;
