module.exports = {
    PG_CONFIG:
    {
      user: process.env.PGUSER,
      database: process.env.PGDATABASE,
      password: process.env.PGPASSWORD,
      host: process.env.PGHOST,
      port: process.env.PGPORT,
      ssl: true,
      max: 10,
      idleTimeoutMillis: 30000
    },
  }