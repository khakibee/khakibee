var express = require('express');
var router = express.Router();
const queries = require('../queries');


router.all('/*', (req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*" ); 
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
           GET home page. 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
router.get('/', (req, res, next) => {
  res.render('index', { title: 'Express' });
});


/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
         GET All Rooms. 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
router.get('/room', (req, res, next) => {
    queries.getAllRooms(req, res, (err, result) => {
      if (err) {
        done (err)
      }
      res.json(result);
    })
});


/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
       GET Specific Room. 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
router.get('/room/:title', (req, res, next) => {
  var permalink = req.params.title;
  queries.getRoom(req, res, permalink, (err, result) => {
    if (err) {
      done (err)
    }
    res.json(result);
  })
});


/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      GET All Companies. 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
router.get('/companies', (req, res, next) => {
  queries.getAllCompanies(req, res, (err, result) => {
    if (err) {
      done (err)
    }
    res.json(result);
  })
});


/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      GET Specific Company. 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
router.get('/companies/:title', (req, res, next) => {
  var permalink = req.params.title;
  queries.getCompanyRooms(req, res, permalink, (err, result) => {
    if (err) {
      done (err)
    }
    res.json(result);
  })
});


/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  GET reservation hours for a specific room. 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
router.get('/roomBkg/:id', (req, res, next) => {
  var roomId = req.params.id;
  queries.getBkgTimesRoom(req, res, roomId, (err, result) => {
    if (err) {
      done (err)
    }
    res.json(result);
  })
});


/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
   POST Booking for a specific room. 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
router.post('/bookRoom', (req, res, next) => {
  queries.setReserv(req, res, (err, result) => {
    if (err) {
      done (err)
    }
    res.json(result);
  })
});


module.exports = router;
