const pg = require('pg')
const connections = require('../config/connections.js')

module.exports = {
  pool: new pg.Pool(connections.PG_CONFIG),
}