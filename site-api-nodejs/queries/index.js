var db = require('../db')
//const sha256 = require('sha256)

module.exports = {

    /*~~~~~~~~~~~~~~~getAllRooms~~~~~~~~~~~~~~~~
                    Select all Rooms. 
    ~~~~~~~~~~~~~~~~~getAllRooms~~~~~~~~~~~~~~~~*/
    getAllRooms: (req, res, callback) => {
        db.pool.connect( (err, client, done) => {
          if (err) {
            console.log('Cannot able to get connection!' + err)
            res.status(400).send(err)
          }
          var qString = `SELECT * FROM main.room ORDER BY random();`;
          client.query(qString, (err, result) => {
            done()
            if (err) {
              console.log('Error executing query getAllRooms: ', qString, "\n", err)
              res.status(400).send(err)
            }
            return callback(null, result.rows)
          })
        })
    },


    /*~~~~~~~~~~~~~~~getRoom~~~~~~~~~~~~~~~~
              Select a specific room. 
    ~~~~~~~~~~~~~~~~~getRoom~~~~~~~~~~~~~~~~*/
    getRoom: (req, res, permalink, callback) => {
        db.pool.connect((err, client, done) => {
        if (err) {
          console.log('Cannot able to get connection!' + err)
          res.status(400).send(err)
        }
        var qString = `SELECT * FROM main.room LEFT JOIN main.cmpn USING(cmpn_id) where main.room.cmpn_id = main.cmpn.cmpn_id  AND room.permalink = $1;`;
        client.query(qString,[permalink], (err, result) => {
          var room = {
            "room_id" : result.rows[0].room_id,
            "title" : result.rows[0].title,
            "permalink" : result.rows[0].permalink,
            "description" : result.rows[0].description,
            "tag_id" : result.rows[0].tag_id,
            "duration" : result.rows[0].duration,
            "actor" : result.rows[0].actor,
            "img_url" : result.rows[0].img_url,
            "plr_num" : result.rows[0].plr_num,
            "cmpn_id" : result.rows[0].cmpn_id,
            "name" : result.rows[0].name,
            "email_addr" : result.rows[0].email_addr,
            "mob_num" : result.rows[0].mob_num,
            "fix_num" : result.rows[0].fix_num,
            "address" : result.rows[0].address,
            "city" : result.rows[0].city
          }
          done()
          if (err) {
            console.log('Error executing query Room: ', qString, "\n", err)
            res.status(400).send(err)
          }
          return callback(null, result.rows)
        })
      })
    },


    /*~~~~~~~~~~~~~~~getAllCompanies~~~~~~~~~~~~~~~~
          Select All Companies by random order. 
    ~~~~~~~~~~~~~~~~~getAllCompanies~~~~~~~~~~~~~~~~*/
    getAllCompanies: (req, res, callback) => {
      db.pool.connect( (err, client, done) => {
        if (err) {
          console.log('Cannot able to get connection!' + err)
          res.status(400).send(err)
        }
        var qString = `SELECT * FROM main.cmpn ORDER BY random();`;
        client.query(qString, (err, result) => {
          done()
          if (err) {
            console.log('Error executing query getAllCompanies: ', qString, "\n", err)
            res.status(400).send(err)
          }
          return callback(null, result.rows)
        })
      })
    },

    /*~~~~~~~~~~~~~~~getCompanyRooms~~~~~~~~~~~~~~~~
                    Select Specific Room. 
    ~~~~~~~~~~~~~~~~~getCompanyRooms~~~~~~~~~~~~~~~~*/
    getCompanyRooms: (req, res, roomId, callback) => {
      db.pool.connect( (err, client, done) => {
        if (err) {
          console.log('Cannot able to get connection!' + err)
          res.status(400).send(err)
        }

        var qString = `SELECT * FROM main.cmpn LEFT JOIN main.room USING(cmpn_id) where cmpn_id = main.cmpn.cmpn_id  AND cmpn.permalink = $1 ORDER BY room_id;`;
        client.query(qString,[roomId], (err, result) => {
          var cmpn = {
            "cmpn_id" : result.rows[0].cmpn_id,
            "name" : result.rows[0].name,
            "permalink" : result.rows[0].permalink,
            "email_addr" : result.rows[0].email_addr,
            "mob_num" : result.rows[0].mob_num,
            "fix_num" : result.rows[0].fix_num,
            "address" : result.rows[0].address,
            "city" : result.rows[0].city,
            "vat" : result.rows[0].vat,
            "owner_nm" : result.rows[0].owner_nm
          }
          var room = []
          result.rows.forEach(rooms => {
            room.push(rooms)
          });
          var returnConf = {
            "company" : cmpn,
            "rooms" : room
          }
          done()
          if (err) {
            console.log('Error executing query getCompanyRooms: ', qString, "\n", err)
            res.status(400).send(err)
          }
          return callback(null, returnConf)
        })
      })
    },


    /*~~~~~~~~~~~~~~~getBkgTimesRoom~~~~~~~~~~~~~~~~
        Select booking Hours for a specific room. 
    ~~~~~~~~~~~~~~~~~getBkgTimesRoom~~~~~~~~~~~~~~~~*/
    getBkgTimesRoom: (req, res, roomId, callback) =>  {
      db.pool.connect( (err, client, done) => {
        if (err) {
          console.log('Cannot able to get connection!' + err)
          res.status(400).send(err)
        }
        console.log("test",typeof roomId, roomId)
        var qString = `SELECT * FROM main.game_sess LEFT JOIN main.bkng ON main.game_sess.game_sess_id = main.bkng.game_sess_id AND main.game_sess.room_id = $1 ;`;
        client.query(qString,[roomId],(err, result) => {
          done()
          if (err) {
            console.log('Error executing query getBkgTimesRoom: ', qString, "\n", err)
            res.status(400).send(err)
          }
          console.log("test", result.rows[0])
          return callback(null, result.rows)
        })
      })
    },
    

    /*~~~~~~~~~~~~~~~getBkgTimesRoom~~~~~~~~~~~~~~~~
         Insert New Booking for a specific room. 
    ~~~~~~~~~~~~~~~~~getBkgTimesRoom~~~~~~~~~~~~~~~~*/
    setReserv: (req, res, callback) => {
        console.log("insert",req.body)
        db.pool.connect( (err, client, done) => {
        if (err) {
          console.log('Cannot able to get connection!' + err)
          res.status(400).send(err)
        }
        var currentDate = new Date();
        var game_sess_id = req.body.game_sess_id;

        var qString = `INSERT INTO main.bkng (game_sess_id,plr_id,plr_num,bkng_status_id,date) VALUES ($1,$2,$3,$4,$5);`;
        client.query(qString,[game_sess_id, req.body.plr_id, req.body.plr_num, req.body.bkng_status_id, currentDate], (err, result) => {
          done()
          if (err) {
            console.log('Error executing query Room: ', qString, "\n", err)
            res.status(400).send(err)
          }
          return callback(null, result.rows)
        })
      })
    }
  
}