export default {
  items: [
    {
      name: 'Ημερολόγιο',
      url: '/calendar',
      icon: 'icon-calendar',
    },
    {
      name: 'Δωμάτια',
      url: '/rooms',
      icon: 'icon-puzzle',
    }
  ],
};
