import React from 'react';

const Calendar = React.lazy(() => import('./components/Scheduler'));
const Rooms = React.lazy(() => import('./components/Rooms'));

// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  { path: '/calendar', exact: true, name: 'Ημερολόγιο', component: Calendar },
  { path: '/rooms', exact: true, name: 'Δωμάτια', component: Rooms }
];

export default routes;
