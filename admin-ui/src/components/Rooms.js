import React, { Component } from 'react';
import { connect } from 'react-redux';
import { css } from "@emotion/core";
import { HashLoader } from "react-spinners";
import Switch from "react-switch";
import { Row, Col, Card, CardHeader, CardBody, CardImg } from 'reactstrap';


const override = css`display: block; margin: 0 auto; border-color: red;`;

const API = process.env.REACT_APP_API;
const ROOMS = API + '/api/rooms';

class Rooms extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            rooms: []
        };

    }

    render() {
        const { rooms, loading } = this.state

        const myRooms = rooms.map((rm) =>
            <Col key={rm.room_id} className="">
                <Card id={rm.room_id}>
                    <CardHeader as="h2">
                        <div className="d-flex justify-content-between">
                            <b className="col-form-label font-weight-semibold">{rm.nme}</b>
                            <Switch
                                id={rm.room_id.toString()}
                                checked={!!rm.active}
                                onColor="#35c725"
                                offColor="#e10707"
                                onChange={() => this.handleRoomsStatus(rm.room_id, rm.active)}
                                boxShadow="0px 1px 5px rgba(0, 0, 0, 0.6)"
                                activeBoxShadow="0px 0px 1px 10px rgba(0, 0, 0, 0.2)"
                            />
                        </div>
                    </CardHeader>
                    <CardBody>
                        <CardImg className="session" src={rm.img} alt="Card image cap" />
                    </CardBody>
                </Card>
            </Col >
        )


        return (
            <div>
                {/* While Loading */}
                <div style={{ position: 'absolute', left: '55%', top: '50%', transform: 'translate(-50%, -50%)' }}>
                    <div className="sweet-loading">
                        <HashLoader
                            css={override}
                            color={"#060707"}
                            size={300}
                            loading={loading}
                        />
                    </div>
                </div>

                {!loading &&
                    <Row>
                        {myRooms}
                    </Row>
                }
            </div>
        );
    }

    componentDidMount() {
        this.setState({ loading: true })

        fetch(ROOMS, {
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + localStorage.token,
            }
        })
            .then(response => {
                if (response.status === 200)
                    return response.json();
                else
                    console.log("GAME_ROOMS Error:", response.status)
            })
            .then(data => {
                if (data)
                    this.setState({
                        rooms: data,
                        loading: false
                    })
            })
    }

    handleRoomsStatus(id, active) {
        let activeStatus = active ? 0 : 1

        fetch(ROOMS, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.token,
            },
            body: JSON.stringify({
                rm_id: id,
                active: activeStatus
            })
        })
            .then(response => {
                if (response.status === 200) {
                    var rooms = this.state.rooms.map(rm => {
                        if (rm.room_id === id)
                            rm.active = activeStatus

                        return rm
                    })
                    this.setState({ rooms: rooms })
                } else
                    console.log("Error:", response.status)
            })
    }
}

function mapStateToProps(state) {
    return {
        api: state.apiReducer,
    }
}

export default connect(mapStateToProps)(Rooms);