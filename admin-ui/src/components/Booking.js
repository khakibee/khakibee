import React, { Component } from 'react';
import {
  Button, Row, Col, TabPane, Nav, NavItem, NavLink, // Common Components
  Modal, ModalHeader, ModalBody, ModalFooter, // Modal Components
  FormGroup, Input, Label, TabContent, Alert // Form Components
} from 'reactstrap';
import CountUp from 'react-countup';
import { SyncLoader } from 'react-spinners';
import Switch from "react-switch";
import classnames from 'classnames';
import ValidationInput from './Validation';
import { capitalize } from '../util/misc';

const API = process.env.REACT_APP_API;
const HANDLE_BOOKING = API + '/api/booking';
const SESSIONS = API + '/api/sessions';

class Booking extends Component {
  constructor(props) {
    super(props);

    const booked = this.props.event.booked;
    const bkngData = this.props.event.bkngData;

    this.state = {
      active: this.props.event.active,
      players: new ValidationInput(booked ? bkngData.plr_num : "", (value) => value !== ""),
      name: new ValidationInput(booked ? bkngData.nme : "", (value) => /^[α-ωΑ-Ω-ωίϊΐόάέύϋΰήώa-zA-Z!-+='*&^$#@0-9 ]{2,100}$/.test(value)),
      email: new ValidationInput(booked ? bkngData.email : "", (value) => /\b[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}\b/.test(value)),
      mobile: new ValidationInput(booked ? bkngData.phone : "", (value) => /^[0-9]{10,20}$/.test(value)),
      diff: new ValidationInput(booked ? bkngData.diff : "", (value) => value !== ""),
      plrLvl: new ValidationInput(booked ? bkngData.plr_lvl : "", (value) => value !== ""),
      learnedUs: new ValidationInput(booked ? bkngData.learned_us : "", (value) => value !== ""),
      teamName: new ValidationInput(booked && bkngData.team_nme !== null ? bkngData.team_nme : "", (value) => /^[α-ωΑ-Ω-ωίϊΐόάέύϋΰήώa-zA-Z !^%,.(){}'@#&*-_+=:|?;0-9]{0,100}$/.test(value)),
      playRoom1: new ValidationInput(booked && bkngData.play_1 !== null ? bkngData.play_1 : "", (value) => value !== ""),
      escRoom1: new ValidationInput(booked && bkngData.esc_1 !== null ? bkngData.esc_1 : "", (value) => value !== ""),
      lang: new ValidationInput(booked ? bkngData.lang : "el", (value) => value !== "0"),
      code: new ValidationInput(booked && bkngData.code !== null && this.props.event.room !== 4 ? bkngData.code : "", (value) => /^[A-Z 0-9]{0,5}$/.test(value)),
      manCode: new ValidationInput(booked && bkngData.code !== null && this.props.event.room === 4 ? bkngData.code : "", (value) => /^[0-9]{9}$/.test(value), "number"),
      age: new ValidationInput(booked ? bkngData.age : "s", (value) => value !== "0"),
      notes: new ValidationInput(booked && bkngData.notes !== null ? bkngData.notes : "", (value) => /^[α-ωΑ-Ω-ωίϊΐόάέύϋΰήώa-zA-Z 0-9()!^%{}'@#&*-_+=:|?;,.?;]{0,500}$/.test(value)),
      invalidInputs: [], showRemoveBkng: false, saveFlag: false,
      processLoading: false,
      activeTab: '1',
      enableSession: 1,
      manCodeCashIn: this.props.event.room === 4 && booked && bkngData.code !== null && bkngData.code !== "" ? true : false
    };

  }

  render() {
    const event = this.props.event;
    var { saveFlag, processLoading } = this.state

    const manSave = this.state.manCodeCashIn && <CountUp
      start={0}
      end={5}
      decimals={2}
      delay={.5}
      duration={5}
    />;

    if (!event)
      return <></>

    return (
      <div>
        <Modal isOpen={this.props.show} size="lg">
          <ModalHeader className="bg-primary" toggle={this.props.hide} >
            {!event.booked && <> <span className="fa fa-unlock" /> Νέα κράτηση</>}
            {event.booked && <> <span className="fa fa-lock" /> Επεξεργασία κράτησης</>}
          </ModalHeader>
          <ModalBody>

            <Row>
              <Col lg="12">
                <div className="mr-2 float-right">
                  <Switch
                    id={event.id.toString()}
                    checked={!!this.state.active}
                    onColor="#35c725"
                    offColor="#e10707"
                    onChange={() => this.handleSessionsActivity(event.id, this.state.active)}
                    boxShadow="0px 1px 5px rgba(0, 0, 0, 0.6)"
                    activeBoxShadow="0px 0px 1px 10px rgba(0, 0, 0, 0.2)"
                  />
                </div>
              </Col>
            </Row>

            <Row>
              <Col lg="12">

                <Nav tabs>
                  {/* ----------------- 1st Tab ------------------ */}
                  <NavItem>
                    <NavLink className={classnames({ active: this.state.activeTab === '1' })} onClick={() => this.toggleTabs('1')}>
                      Κράτηση
                    </NavLink>
                  </NavItem>
                  {/* ----------------- 2st Tab ------------------ */}
                  {/* <NavItem>
                    <NavLink className={classnames({ active: this.state.activeTab === '2' })} onClick={() => this.toggleTabs('2')}>
                      Περισσότερα
                    </NavLink>
                  </NavItem> */}
                </Nav>

                {/* ----------------- 1st Tab Content ------------------ */}

                <TabContent activeTab={this.state.activeTab}>
                  <TabPane tabId="1">

                    <ul className="list-group room-title list-group-flush px-4 py-2 bg-gray-300 rounded">
                      <div className="h5 mr-3 mb-1">
                        {event.roomTitle} {",  "}
                        <span className="icon-calendar"> {event.start.toDateString()} </span>
                        <span className="icon-clock"> {event.start.toTimeString().replace(/:00\s.*/, "")} </span>

                        {/* {(event.booked && saveFlag && (new Date() - event.start < 0)) && <div className="row">
                          <EditBooking
                            dte={event.start}
                            rm_id={event.room}
                            editted={this.state.edittedSession}
                            getDateTime={this.getEdittedDateTime}
                            setEditFlag={() => this.setState({ edittedSession: true })} />
                        </div>} */}

                      </div>
                    </ul>
                    {!!event.src && <em>Booked by {capitalize(event.src)}</em>}

                    <Row form className="mt-3">
                      <Col md={12}>
                        <FormGroup>
                          <Label for="players">Παίκτες:<b>*</b></Label>
                          <Input type="select" name="player-select" id="players" disabled={!event.booked ? false : !saveFlag} className="custom-select" value={this.state.players.value} onChange={(e) => this.handleInputsChange(e.target.value, e.target.id)} invalid={this.state.invalidInputs.includes("players")}>
                            <option value="" >Παρακαλώ επιλέξτε αριθμό παικτών...</option>
                            {event.room === 1 && <>
                              <option value="3">3 x 26€</option>
                              <option value="4">4 x 24€</option>
                              <option value="5">5 x 22€</option>
                              <option value="6">6 x 21€</option>
                              <option value="7">7 x 20€</option></>}
                            {event.room === 2 && <>
                              <option value="4">4 x 30€</option>
                              <option value="5">5 x 30€</option>
                              <option value="6">6 x 27€</option>
                              <option value="7">7 x 27€</option> </>}
                            {event.room === 3 && <>
                              <option value="4">4 x 29€</option>
                              <option value="5">5 x 27€</option>
                              <option value="6">6 x 26€</option>
                              <option value="7">7 x 25€</option> </>}
                            {event.room === 4 && !this.state.manCodeCashIn && <>
                              <option value="4">4 x 28€</option>
                              <option value="5">5 x 28€</option>
                              <option value="6">6 x 25€</option>
                              <option value="7">7 x 25€</option> </>}
                            {event.room === 4 && this.state.manCodeCashIn && <>
                              <option value="4">4 x 23€</option>
                              <option value="5">5 x 23€</option>
                              <option value="6">6 x 20€</option>
                              <option value="7">7 x 20€</option> </>}
                          </Input>
                          {this.state.invalidInputs.includes("players") && <div className="invalid-feedback">Μη αποδεκτός χαρακτήρας...</div>}
                        </FormGroup>
                      </Col>

                      <hr />
                      <Col md={6}>

                        <FormGroup>
                          <Label for="name">Ονοματεπώνυμο:<b>*</b></Label>
                          <Input type="text" name="fullName" id="name" disabled={!event.booked ? false : !saveFlag} value={this.state.name.value} onChange={(e) => this.handleInputsChange(e.target.value, e.target.id)} invalid={this.state.invalidInputs.includes("name")} />
                          {this.state.invalidInputs.includes("name") && <div className="invalid-feedback">Μη αποδεκτός χαρακτήρας...</div>}
                        </FormGroup>

                        <FormGroup>
                          <Label for="email">Email:<b>*</b></Label>
                          <Input type="email" name="email" id="email" disabled={!event.booked ? false : !saveFlag} value={this.state.email.value} onChange={(e) => this.handleInputsChange(e.target.value, e.target.id)} invalid={this.state.invalidInputs.includes("email")} />
                          {this.state.invalidInputs.includes("email") && <div className="invalid-feedback">Μη αποδεκτός email...</div>}
                        </FormGroup>

                        <FormGroup>
                          <Label for="mobile">Κινητό:<b>*</b></Label>
                          <Input type="tel" name="mobile" id="mobile" disabled={!event.booked ? false : !saveFlag} value={this.state.mobile.value} onChange={(e) => this.handleInputsChange(e.target.value, e.target.id)} invalid={this.state.invalidInputs.includes("mobile")} />
                          {this.state.invalidInputs.includes("mobile") && <div className="invalid-feedback">Μη αποδεκτός αριθμός...</div>}
                        </FormGroup>

                        <FormGroup>
                          <Label for="lang">Γλώσσα:<b>*</b></Label>
                          <Input type="select" name="lang-select" id="lang" disabled={!event.booked ? false : !saveFlag} value={this.state.lang.value} className="custom-select" onChange={(e) => this.handleInputsChange(e.target.value, e.target.id)} invalid={this.state.invalidInputs.includes("lang")}>
                            <option value="0">Παρακαλώ επιλέξτε γλώσσα...</option>
                            <option value="el">Ελληνικά</option>
                            <option value="en">Αγγλικά</option>
                          </Input>
                          {this.state.invalidInputs.includes("lang") && <div className="invalid-feedback">Παρακαλώ επιλέξτε ένα από τα παρακάτω.</div>}
                        </FormGroup>

                        {event.room === 2 && <FormGroup>
                          <Label for="playRoom1">Έχουν παίξει όλοι οι παίκτες την "Έπαυλη":<b>*</b></Label>
                          <Input type="select" name="playRoom1" id="playRoom1" disabled={!event.booked ? false : !saveFlag} value={this.state.playRoom1.value} className="custom-select" onChange={(e) => this.handleInputsChange(e.target.value, e.target.id)} invalid={this.state.invalidInputs.includes("playRoom1")}>
                            <option value="">Παρακαλώ επιλέξτε...</option>
                            <option value={true}>Ναί</option>
                            <option value={false}>Όχι</option>
                          </Input>
                          {this.state.invalidInputs.includes("playRoom1") && <div className="invalid-feedback">Παρακαλώ επιλέξτε ένα από τα παρακάτω.</div>}
                        </FormGroup>}

                        <FormGroup>
                          <Label for="team_name">Όνομα ομάδας:</Label>
                          <Input type="text" name="team-name" id="teamName" disabled={!event.booked ? false : !saveFlag} value={this.state.teamName.value} onChange={(e) => this.handleInputsChange(e.target.value, e.target.id)} invalid={this.state.invalidInputs.includes("teamName")} />
                          {this.state.invalidInputs.includes("teamName") && <div className="invalid-feedback">Μη αποδεκτός χαρακτήρας...</div>}
                        </FormGroup>

                      </Col>
                      <Col md={6}>
                        <FormGroup>
                          <Label for="diff">Δυσκολία:<b>*</b></Label>
                          <Input type="select" name="diff-select" id="diff" disabled={!event.booked ? false : !saveFlag} value={this.state.diff.value} className="custom-select" onChange={(e) => this.handleInputsChange(e.target.value, e.target.id)} invalid={this.state.invalidInputs.includes("diff")}>
                            <option value="">Παρακαλώ επιλέξτε δυσκολία...</option>
                            <option value="N">Κανονικό</option>
                            <option value="H">Δύσκολο</option>
                          </Input>
                          {this.state.invalidInputs.includes("diff") && <div className="invalid-feedback">Παρακαλώ επιλέξτε ένα από τα παρακάτω.</div>}
                        </FormGroup>
                        <FormGroup>
                          <Label for="plrLvl">Επίπεδο παικτών:<b>*</b></Label>
                          <Input type="select" name="plrLvl-select" id="plrLvl" disabled={!event.booked ? false : !saveFlag} value={this.state.plrLvl.value} className="custom-select" onChange={(e) => this.handleInputsChange(e.target.value, e.target.id)} invalid={this.state.invalidInputs.includes("plrLvl")}>
                            <option value="">Παρακαλώ επιλέξτε επίπεδο δυσκολίας...</option>
                            <option value="0-1">0-1</option>
                            <option value="2-5">2-5</option>
                            <option value="6-10">6-10</option>
                            <option value="11-20">11-20</option>
                            <option value="21-35">21-35</option>
                            <option value="36-50">36-50</option>
                            <option value="51-100">51-100</option>
                            <option value="101-200">101-200</option>
                            <option value="201+">201+</option>
                          </Input>
                          {this.state.invalidInputs.includes("plrLvl") && <div className="invalid-feedback">Παρακαλώ επιλέξτε ένα από τα παρακάτω.</div>}
                        </FormGroup>

                        <FormGroup>
                          <Label for="us">Πώς μάθατε για εμάς:<b>*</b></Label>
                          <Input type="select" name="aboutUs-select" id="learnedUs" disabled={!event.booked ? false : !saveFlag} value={this.state.learnedUs.value} className="custom-select" onChange={(e) => this.handleInputsChange(e.target.value, e.target.id)} invalid={this.state.invalidInputs.includes("learnedUs")}>
                            <option value="">Παρακαλώ επιλέξτε ένα από τα παρακάτω...</option>
                            <option value="fb">Facebook</option>
                            <option value="go">Google</option>
                            <option value="fr">Φίλοι</option>
                            <option value="ta">TripAdvisor</option>
                            {event.room !== 1 && <option value="mn">Την Έπαυλη</option>}
                            {event.room !== 2 && <option value="bo">Το Βιβλειοπωλείον</option>}
                            {event.room !== 3 && <option value="co">Το Ωδείον</option>}
                            <option value="si">Escape room sites</option>
                            <option value="lf">Φυλλάδια / Κάρτες</option>
                          </Input>
                          {this.state.invalidInputs.includes("learnedUs") && <div className="invalid-feedback">Παρακαλώ επιλέξτε ένα από τα παρακάτω.</div>}
                        </FormGroup>

                        <FormGroup>
                          <Label for="age">Ηλικία:<b>*</b></Label>
                          <Input type="select" name="age-select" id="age" className="custom-select" disabled={!event.booked ? false : !saveFlag} value={this.state.age.value} onChange={(e) => this.handleInputsChange(e.target.value, e.target.id)} invalid={this.state.invalidInputs.includes("age")}>
                            <option value="">Παρακαλώ επιλέξτε ένα από τα παρακάτω...</option>
                            <option value="m">Κάτω των 18</option>
                            <option value="s">Άνω των 18</option>
                          </Input>
                          {this.state.invalidInputs.includes("age") && <div className="invalid-feedback">Παρακαλώ επιλέξτε ένα από τα παρακάτω.</div>}
                        </FormGroup>

                        {event.room === 2 && <FormGroup>
                          <Label for="escRoom1">Είχατε ανακαλύψει την Πικρή Αλήθεια στο τέλος της Έπαυλης:<b>*</b></Label>
                          <Input type="select" name="escRoom1" id="escRoom1" disabled={!event.booked ? false : !saveFlag} value={this.state.escRoom1.value} className="custom-select" onChange={(e) => this.handleInputsChange(e.target.value, e.target.id)} invalid={this.state.invalidInputs.includes("escRoom1")}>
                            <option value="">Παρακαλώ επιλέξτε ένα από τα παρακάτω...</option>
                            <option value={true}>Ναί</option>
                            <option value={false}>Όχι</option>
                          </Input>
                          {this.state.invalidInputs.includes("escRoom1") && <div className="invalid-feedback">Παρακαλώ επιλέξτε ένα από τα παρακάτω.</div>}
                        </FormGroup>}

                        {event.room !== 4 && <FormGroup>
                          <Label for="code">Κωδικός προσφοράς:</Label>
                          <Input type="text" name="code-select" id="code" disabled={!event.booked ? false : !saveFlag} value={this.state.code.value} onChange={(e) => this.handleInputsChange(e.target.value, e.target.id)} invalid={this.state.invalidInputs.includes("code")}></Input>
                          {this.state.invalidInputs.includes("code") && <div className="invalid-feedback">Μη αποδεκτός κωδικός.</div>}
                        </FormGroup>}

                        {event.room === 4 && <FormGroup>
                          <Label for="manCode">Μοναδικός κωδικός νησιώτη(ΜΑΝ)</Label>
                          <div className="input-group">
                            <Input type="text" name="code-select" id="manCode" pattern="[0-9]+" disabled={!event.booked ? false : !saveFlag} value={this.state.manCode.value} onChange={(e) => this.handleManCode(e)} invalid={this.state.invalidInputs.includes("manCode")}></Input>
                            <div className="input-group-append">
                              {!this.props.event.booked && <Button className="btn btn-xing" type="button" onClick={() => this.handleInputsChange(this.state.manCode.value, "manCode")}>Eξαργύρωση</Button>}
                            </div>
                            {this.state.invalidInputs.includes("manCode") && <div className="invalid-feedback">Μη αποδεκτός κωδικός.</div>}
                          </div>
                        </FormGroup>}

                        {event.room === 4 && !this.props.event.booked && this.state.manCodeCashIn && <Alert className="alert alert-success" isOpen={this.state.manCodeCashIn}>
                          <>Κερδίσατε έκπτωση <strong className="text-black">{manSave}€</strong> κατ' άτομο, Θα χρειαστεί η επίδειξη του ΜΑΝ αριθμού σας.</>
                        </Alert>}

                      </Col>
                      <Col md={12}>

                        <FormGroup>
                          <Label for="notes">Σημειώσεις:</Label>
                          <Input type="textarea" name="notes" id="notes" disabled={!event.booked ? false : !saveFlag} value={this.state.notes.value === null ? '' : this.state.notes.value} onChange={(e) => this.handleInputsChange(e.target.value, e.target.id)} invalid={this.state.invalidInputs.includes("notes")} />
                          {this.state.invalidInputs.includes("notes") && <div className="invalid-feedback">Μη αποδεκτός χαρακτήρας...</div>}
                        </FormGroup>
                      </Col>

                    </Row>
                    <div className="text-right">
                      <small>Τα πεδία με (*) είναι υποχρεωτικά.</small>
                    </div>

                  </TabPane>

                  <TabPane tabId="2">
                  </TabPane>

                </TabContent>
              </Col>
            </Row>

          </ModalBody>

          <ModalFooter>
            {!processLoading && <Button className="btn btn-brand mr-1" color="secondary" onClick={this.props.hide}>Ακύρωση</Button>}
            {!event.booked && !processLoading && <Button name="new" className="btn btn-brand mr-1" color="primary" onClick={this.handleCompleteBooking}>Ολοκλήρωση</Button>}
            {event.booked && !processLoading && <Button className="btn btn-brand mr-1" color="danger" onClick={this.handleRemoveBkngEvent}>Διαγραφή</Button>}
            {event.booked && !processLoading && !saveFlag && <Button className="btn btn-brand mr-1" color="primary" onClick={() => this.setState({ saveFlag: true })}>Επεξεργασία</Button>}
            {event.booked && !processLoading && saveFlag && <Button name="edit" className="btn btn-brand mr-1" color="primary" onClick={this.handleCompleteBooking}>Αποθήκευση</Button>}
            {processLoading && <Button color="success" disabled>
              <SyncLoader sizeUnit={"px"} size={6} color={'#fff'} /></Button>}
          </ModalFooter>
        </Modal>

        {this.state.showRemoveBkng && <Modal isOpen={this.state.showRemoveBkng} className="modal-dialog-centered" toggle={this.handleCloseRemoveBkngModal} size="md">
          <ModalHeader className="bg-dark" toggle={this.handleCloseRemoveBkngModal}>
            Διαγραφή κράτησης</ModalHeader>
          <ModalBody>
            <p className="h6">Είστε σίγουρος ότι θέλετε να διαγράψετε το ραντεβού του <b>{event.bkngData.nme}</b>, <u>{event.bkngData.plr_num} παίχτες</u>,
              στις <mark>{event.bkngData.dte}</mark>?
            </p>
          </ModalBody>
          <ModalFooter>
            {!processLoading && <Button color="secondary" onClick={this.handleCloseRemoveBkngModal}>Ακύρωση</Button>}
            {!processLoading && <Button color="danger" onClick={this.handleDeleteBooking}>Διαγραφή</Button>}
            {processLoading && <Button color="success" disabled>
              <SyncLoader sizeUnit={"px"} size={6} color={'#fff'} /></Button>}
          </ModalFooter>
        </Modal>}

        {this.state.showBkngNotAvailable && <Modal isOpen={this.state.showBkngNotAvailable} className="modal-dialog-centered" size="md">
          <ModalHeader className="bg-dark" toggle={()=>this.setState({showBkngNotAvailable:false})}>
            Μη διαθέσιμη κράτηση
          </ModalHeader>
          <ModalBody>
            <p className="h6">Η κράτηση δεν είναι πλέον διαθέσιμη. </p>
          </ModalBody>
          <ModalFooter>
            <Button color="success" onClick={()=>this.setState({showBkngNotAvailable:false})}> ΟΚ </Button>
          </ModalFooter>
        </Modal>}

      </div>
    )
  }

  toggleTabs = (tab) => {
    if (this.state.activeTab !== tab)
      this.setState({ activeTab: tab });
  }

  handleManCode = (e) => {
    let inputID = e.target.id, value = e.target.value, state = this.state

    state[inputID].value = value
    this.setState({ manCode: state[inputID] })
  }

  handlechChangeSessionStatus = (e) => {
    this.setState({
      enableSession: !e.target.checked === true ? "0" : "1"
    })
  }

  handleRemoveBkngEvent = () => {
    this.setState({ showRemoveBkng: true })
  }

  handleCloseRemoveBkngModal = () => {
    this.setState({ showRemoveBkng: false })
  }

  handleInputsChange = (inputValue, inputID) => {
    let state = { invalidInputs: this.state.invalidInputs }, manCodeCashIn = this.state.manCodeCashIn
    state[inputID] = this.state[inputID]

    if (state[inputID].type === "string") {
      state[inputID].value = inputValue;
    } else if (state[inputID].type === "number")
      state[inputID].value = inputValue
    else {
      inputValue = inputValue === 'true'
      state[inputID].value = !inputValue;
    }

    if (!state[inputID].validate()) {
      if (!state.invalidInputs.includes(inputID))
        state.invalidInputs.push(inputID)
    } else {
      state.invalidInputs = state.invalidInputs.filter(input => input !== inputID);

      if (inputID === "manCode")
        manCodeCashIn = true
    }

    this.setState({
      invalidInputs: state.invalidInputs,
      manCodeCashIn: manCodeCashIn
    });
  }

  handleDeleteBooking = () => {
    this.setState({ processLoading: true })

    fetch(HANDLE_BOOKING + '/' + this.props.event.bkngData.bkng_id, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.token,
      }
    })
      .then(response => {
        if (response.status === 200) {
          this.setState({ processLoading: false })
          this.props.hide();
        } else
          console.log("Error:", response.status)
      })
  }

  getEdittedDateTime = (date, time) => {
    console.log("getEdittedDateTime: ", date, time)
    if (!date || !time)
      return

    this.setState({
      edittedDate: date,
      edittedTime: time,
    })
  }

  handleCompleteBooking = (e) => {
    var invalidInputs = []

    for (var key in this.state) //Validate all inputs before fetching
      if (this.state[key] && typeof this.state[key].validate === "function" && !this.state[key].validate()) { //checking if type is ValidationInput(by lookiing for validate function) and if value is valid
        if (!(key === "manCode" && this.state[key].value === ""))
          invalidInputs.push(key);
      }

    if (this.props.event.room !== 2) {
      invalidInputs = invalidInputs.filter((value) => { return (value !== "playRoom1" && value !== "escRoom1") });
    }

    if (invalidInputs.length) {
      this.setState({ invalidInputs: invalidInputs });
      return;
    }
    this.setState({ processLoading: true })

    var pad = function (num) { return ('00' + num).slice(-2) };
    let date = this.props.event.start.getUTCFullYear() + '-' +
      pad(this.props.event.start.getUTCMonth() + 1) + '-' +
      pad(this.props.event.start.getUTCDate()) + ' ' +
      pad(this.props.event.start.getHours()) + ':' +
      pad(this.props.event.start.getMinutes()) + ':' +
      pad(this.props.event.start.getSeconds())

    let body = {
      "sess_id": this.props.event.id,
      "rm_id": this.props.event.room,
      "roomName": this.props.event.roomTitle,
      "nme": this.state.name.value,
      "phone": this.state.mobile.value,
      "email": this.state.email.value,
      "plr_num": parseInt(this.state.players.value),
      "diff": this.state.diff.value,
      "plr_lvl": this.state.plrLvl.value,
      "learned_us": this.state.learnedUs.value,
      "team_nme": this.state.teamName.value,
      "code": this.props.event.room !== 4 ? this.state.code.value : this.state.manCode.value,
      "age": this.state.age.value,
      "playRoom1": this.props.event.room === 2 ? this.state.playRoom1.value === "true" ? 1 : 0 : 0,
      "escRoom1": this.props.event.room === 2 ? this.state.escRoom1.value === "true" ? 1 : 0 : 0,
      "lang": this.state.lang.value,
      "notes": this.state.notes.value,
      "dte": date,
      "bkng_sts_id": this.props.event.booked ? 2 : 1,
      "gdpr": 1,
      "src": "admin"
    }

    let method = 'POST'
    if (e.target.name === "edit")
      method = 'PUT'

    fetch(HANDLE_BOOKING, {
      method: method,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.token,
      },
      body: JSON.stringify(body)
    })
      .then(response => {
        if (response.status === 200) {
          this.setState({ processLoading: false })
          this.props.hide()
        } else if (response.status === 404)
          this.setState({showBkngNotAvailable:true})
        else
          console.log("Error:", response.status)
      })
  }

  handleSessionsActivity(id, active) {
    let activeStatus = active ? 0 : 1

    fetch(SESSIONS, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.token,
      },
      body: JSON.stringify({
        session_id: id,
        active: activeStatus
      })
    })
      .then(response => {
        if (response.status === 200)
          this.setState({ active: activeStatus })
        else
          console.log("Error:", response.status)

      })
  }

}



export default Booking;