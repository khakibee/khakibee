import React, { Component } from 'react';
import { connect } from 'react-redux';
import Websocket from 'react-websocket';
import Booking from './Booking'
import { Row, Card, CardBody } from 'reactstrap';
import { css } from "@emotion/core";
import classnames from 'classnames';
import { HashLoader } from "react-spinners";
import { Calendar, momentLocalizer } from 'react-big-calendar';
import "react-big-calendar/lib/css/react-big-calendar.css";
import moment from 'moment';
import { sessIDs, buttonIcons } from '../util/consts';

// Start week from Monday
moment.locale('en-GB', { week: { dow: 1, doy: 1 } });
const override = css`display: block; margin: 0 auto; border-color: red;`;

const API = process.env.REACT_APP_API;
const WS = process.env.REACT_APP_WS;
const ROOMS = API + '/api/rooms';
const SESSIONS = API + '/api/sessions';
const BOOKINGS = API + '/api/bookings';
const allRmsID = 0

class Scheduler extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      showAddBkng: false,
      showEditBkng: false,
      currView: "month",
      allGameSess: [],
      events: [],
      eventsRangeIDs: [1, 105],
      bkngs: [],
      rooms: [],
      nextBackMonth: null,
      currentRoom: allRmsID,
      dirty: false,
      showingEvents: "all",
      openRoomDropdown: false,
      openFeatDropdown: false
    };

  }


  handleWS = (data) => {
    const { currentRoom } = this.state
    this.setState({ dirty: data === "DIRTY" });
    let roomParam = ''

    if (!!currentRoom)
      roomParam = '?rm=' + currentRoom

    if (data === "DIRTY")
      fetch(BOOKINGS + roomParam, {
        method: 'GET',
        headers: { 'Authorization': 'Bearer ' + localStorage.token }
      })
        .then(response => {
          if (response.status === 200)
            return response.json();
          else
            console.log("Error:", response.status)
        })
        .then(data => {
          if (data)
            this.setState({
              bkngs: data,
              dirty: false
            }, this.fetchSessions())

        })
  }

  render() {
    var { loading, modalEvent, openFeatDropdown, openRoomDropdown, showAddBkng, showEditBkng, showingEvents } = this.state
    var websocket = <div> <Websocket url={`${WS}/ws`} onMessage={this.handleWS} /> </div>

    const roomBtnGroup = classnames('btn-group', { 'show': openRoomDropdown })
    const roomDropdownMenu = classnames('dropdown-menu', { 'show': openRoomDropdown })
    const featBtnGroup = classnames('btn-group', { 'show': openFeatDropdown })
    const featDropdownMenu = classnames('dropdown-menu-feat', { 'show': openFeatDropdown })

    // Room Buttons
    var allRoomBtn =
      <a className={"dropdown-item clickable text-light btn-admin " + buttonIcons[0].button} key={allRmsID} onClick={() => this.handleChangeRoom(allRmsID, sessIDs)}>
        <i className={buttonIcons[0].icon} /><span>Όλα</span>
      </a>
    var roomBtns = this.state.rooms.map((rm) =>
      <a className={"dropdown-item clickable text-light btn-admin " + buttonIcons[rm.room_id].button} key={rm.room_id} onClick={() => this.handleChangeRoom(rm.room_id, sessIDs)}>
        <i className={buttonIcons[rm.room_id].icon} /><span>{rm.nme}</span>
      </a>)
    roomBtns.unshift(allRoomBtn)

    return (
      <>

        <div className="sweet-loading" style={{ position: 'absolute', left: '55%', top: '50%', transform: 'translate(-50%, -50%)' }}>
          <HashLoader
            css={override}
            color={"#060707"}
            size={300}
            loading={loading}
          />
        </div>

        {websocket}

        {!loading && <>
          <div className="d-flex justify-content-between">

            <div className={roomBtnGroup}>
              <button type="button" className="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded={openRoomDropdown} onClick={() => this.setState({ openRoomDropdown: !this.state.openRoomDropdown })}>Δωμάτια</button>
              <div className={roomDropdownMenu}>
                {roomBtns}
              </div>
            </div>

            <div className={featBtnGroup}>
              <button type="button" className="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded={openFeatDropdown} onClick={() => this.setState({ openFeatDropdown: !this.state.openFeatDropdown })}>Χαρακτηριστικά</button>
              <div className={featDropdownMenu}>
                <a className={"dropdown-item clickable" + (showingEvents === "all" ? " active" : "")} onClick={() => this.showEvents("all")}><i className="fa fa-th-large" />Όλα</a>
                <a className={"dropdown-item clickable" + (showingEvents === "open" ? " active" : "")} onClick={() => this.showEvents("open")}><i className="fa fa-unlock" style={{ color: "green" }} />Ελεύθερα</a>
                <a className={"dropdown-item clickable" + (showingEvents === "bked" ? " active" : "")} onClick={() => this.showEvents("bked")}><i className="fa fa-lock" style={{ color: "red" }} />Κλεισμένα</a>
                <a className={"dropdown-item clickable" + (showingEvents === "escapeAll" ? " active" : "")} onClick={() => this.showEvents("escapeAll")}><i className="fa fa-trophy" style={{ color: "orange" }} />EscapeAll</a>
                <a className={"dropdown-item clickable" + (showingEvents === "tzampatzides" ? " active" : "")} onClick={() => this.showEvents("tzampatzides")}><i className="fa fa-puzzle-piece" style={{ color: "pink" }} />Tzampatzides</a>
              </div>
            </div>

          </div>

          <hr />

          <Row>
            <div className="col">
              <Card>
                <CardBody>
                  <Calendar
                    localizer={momentLocalizer(moment)}
                    defaultDate={new Date()}
                    defaultView="month"
                    events={this.state.events}
                    onSelectEvent={event => this.openBkngModal(event)}
                    onNavigate={(date, view) => this.handleNavigation(date, view)}
                    eventPropGetter={this.eventStyleGetter}
                    popup={true}
                    popupOffset={{ x: 40, y: 30 }}
                    step={60}
                    timeslots={2}
                    style={{ height: "110vh", widht: "140vh" }}
                  />
                </CardBody>
              </Card>
            </div>

            {showAddBkng && <Booking
              show={showAddBkng}
              hide={this.closeAddBkng}
              event={modalEvent}
              history={this.props.history}
            />}

            {showEditBkng && <Booking
              show={showEditBkng}
              hide={this.closeEditBkng}
              event={modalEvent}
              history={this.props.history}
            />}
          </Row>
        </>}
      </>
    );
  }

  componentDidMount() {
    this.fetchSessions();
  }

  fetchSessions = () => {
    const { currentRoom } = this.state

    const ROOMS_RESULT = fetch(ROOMS, {
      method: 'GET',
      headers: {
        'Authorization': 'Bearer ' + localStorage.token,
      }
    })
      .then(response => {
        if (response.status === 200)
          return response.json();
        else
          console.log("ROOMS Error:", response.status)
      })
      .then(data => {
        if (data) return data;
      })

    const BOOKINGS_RESULT = fetch(BOOKINGS, {
      method: 'GET',
      headers: {
        'Authorization': 'Bearer ' + localStorage.token,
      }
    })
      .then(response => {
        if (response.status === 200)
          return response.json();
        else
          console.log("BOOKINGS Error:", response.status)
      })

    const SESSIONS_RESULT = fetch(SESSIONS, {
      method: 'GET',
      headers: {
        'Authorization': 'Bearer ' + localStorage.token,
      }
    })
      .then(response => {
        if (response.status === 200)
          return response.json();
        else
          console.log("SESSIONS Error:", response.status)
      })

    Promise.all([ROOMS_RESULT, BOOKINGS_RESULT, SESSIONS_RESULT]).then((data) => {
      this.setState({
        loading: false,
        rooms: data[0],
        bkngs: data[1],
        allGameSess: data[2],
      }, this.handleScheduler(data[2], currentRoom, data[1]))
    })

  }

  handleChangeRoom = (rmID, sessIDs) => {
    this.handleScheduler(this.state.allGameSess, rmID, this.state.bkngs, this.state.nextBackMonth)

    this.setState({
      currentRoom: rmID,
      eventsRangeIDs: sessIDs[rmID],
      openRoomDropdown: false
    })
  }

  handleScheduler = (sessions, rmID, bkngs, onNavigate) => {
    var index = 1, events = [];
    var date = (onNavigate === null || onNavigate === undefined) ? new Date() : onNavigate._d, y = date.getFullYear(), m = date.getMonth()
    var firstDay = new Date(y, m, index), lastDay = new Date(y, m + 1, 0)
    const yearIndex = date.getFullYear(), monthIndex = date.getMonth()


    while (firstDay <= lastDay) {
      var session = sessions[firstDay.getDay()]

      session.forEach(ses => {
        let timeS = ses.tm_start.split(":")

        let tmeDur = parseInt(ses.duration.split("'")[0])
        let timeE= [(tmeDur/60).toString(), (tmeDur%60).toString()]

        let  timeEndHH = parseInt(timeS[0]) + parseInt(timeE[0])
        let timeEndMM = parseInt(timeS[1]) + parseInt(timeE[1])


        // Check if minutes events overflow 60'.
        if (timeEndMM >= 60) {
          timeEndMM -= 60
          timeEndMM = parseInt(timeEndMM < 10 ? timeEndMM : "0" + timeEndMM)
          timeEndHH++
        }

        // Check if event duration overflow current day and then set dateEnd = 23:59:59.
        if (timeEndHH >= 24) {
          timeEndHH = 23; timeEndMM = 59;
        }

        // Solve the problem 14:0
        if (timeEndMM < 10)
          timeEndMM = "0" + timeEndMM


        if (rmID === allRmsID)
          events.push({
            active: ses.active,
            id: ses.session_id,
            room: ses.room_id,
            roomTitle: ses.nme,
            start: new Date(yearIndex, monthIndex, firstDay.getDate(), timeS[0], timeS[1]),
            end: new Date(yearIndex, monthIndex, firstDay.getDate(), timeEndHH, timeEndMM),
            title: <><i className="fa fa-unlock" /> <span>{timeS[0] + ":" + timeS[1]}</span></>,
            booked: false
          })
        else
          if (ses.room_id === rmID) {
            events.push({
              active: ses.active,
              id: ses.session_id,
              room: ses.room_id,
              roomTitle: ses.nme,
              start: new Date(yearIndex, monthIndex, firstDay.getDate(), timeS[0], timeS[1]),
              end: new Date(yearIndex, monthIndex, firstDay.getDate(), timeEndHH, timeEndMM),
              title: <><i className="fa fa-unlock" /> <span>{timeS[0] + ":" + timeS[1]}</span></>,
              booked: false
            })
          }

      })

      firstDay = new Date(y, m, ++index);
    }

    this.handleBookings(bkngs, events, rmID)
  }

  handleBookings = (bkngs, events, rmID) => {
    events.forEach((event) => {
      var eventDate = moment(event.start).format('YYYY-MM-DD')
      var bkngStartmm = ("0" + event.start.getMinutes()).slice(-2);
      // var bkngEndmm = ("0" + event.end.getMinutes()).slice(-2);

      bkngs && bkngs.forEach((bkng) => {
        var dateWithOutTime = bkng.dte.split(" ")
        if (bkng.session_id === event.id && dateWithOutTime[0] === eventDate) {
          event.title = <>
            <i className="fa fa-lock" />
            {" " + event.start.getHours() + ":" + bkngStartmm}
            <b>{" " + bkng.nme}</b> {bkng.plr_num + " Παίχτες"}
          </>
          event.booked = true
          event.bkngData = bkng
          event.src = bkng.src
          this.eventStyleGetter(event)
        }
      })
    })

    var allEvents = events

    switch (this.state.showingEvents) {
      case "bked":
        events = events.filter(event => (event.booked && event.id >= this.state.eventsRangeIDs[0] && event.id <= this.state.eventsRangeIDs[1]));
        break;
      case "open":
        events = events.filter(event => (!event.booked && event.id >= this.state.eventsRangeIDs[0] && event.id <= this.state.eventsRangeIDs[1]));
        break;
    }

    this.setState({
      events: events,
      allEvents: allEvents
    });
  }

  eventStyleGetter = (event) => {
    var backgroundColor, opacity = event.booked ? 1.0 : 0.6
    switch (event.room) {
      case 1:
        backgroundColor = '#628f09'
        break;
      case 2:
        backgroundColor = '#9c0e0e'
        break;
      case 3:
        backgroundColor = '#476f8f'
        break;
      default:
        backgroundColor = '#ff0084'
        break;
    }

    var style = {
      backgroundColor: backgroundColor,
      borderRadius: '0px',
      opacity: opacity,
      borderColor: 'black',
      borderStyle: event.booked ? 'dashed' : 'dotted',
      color: event.booked ? 'black' : 'white',
      border: '1,8px',
    };
    return { style: style };
  }

  handleNavigation = (date, view) => {
    var { allGameSess, bkngs, currentRoom } = this.state
    let startOfMonth = moment(date).startOf('month')

    if (view !== 'month')
      startOfMonth = moment(date).startOf('week')

    this.setState({
      nextBackMonth: startOfMonth,
      currView: view
    }, () => this.handleScheduler(allGameSess, currentRoom, bkngs, startOfMonth))
  }

  openBkngModal = (e) => {
    this.setState({
      showAddBkng: !e.booked,
      showEditBkng: e.booked,
      modalEvent: e
    })
  }

  closeAddBkng = () => {
    this.setState({
      showAddBkng: false,
      modalEvent: null
    })
  }

  closeEditBkng = () => {
    this.setState({
      showEditBkng: false,
      modalEvent: null
    })
  }

  showEvents = (show) => {
    var events = this.state.allEvents

    switch (show) {
      case "bked":
        events = events.filter(event => (event.booked && event.id >= this.state.eventsRangeIDs[0] && event.id <= this.state.eventsRangeIDs[1]));
        break;
      case "open":
        events = events.filter(event => (!event.booked && event.id >= this.state.eventsRangeIDs[0] && event.id <= this.state.eventsRangeIDs[1]));
        break;
      case "escapeAll":
        events = events.filter(event => (event.src === "escapeAll"));
        break;
      case "tzampatzides":
        events = events.filter(event => (event.src === "tzampatzides"));
        break;
    }

    this.setState({
      events: events,
      showingEvents: show,
      openFeatDropdown: false
    });
  }

  handleBookedEvents = () => {
    var events = this.state.allEvents
    var result = events.filter(event => (event.booked && event.id >= this.state.eventsRangeIDs[0] && event.id <= this.state.eventsRangeIDs[1]));
    this.setState({ events: result });
  }

  handleAvailableEvents = () => {
    var events = this.state.allEvents
    var result = events.filter(event => (!event.booked && event.id >= this.state.eventsRangeIDs[0] && event.id <= this.state.eventsRangeIDs[1]));
    this.setState({ events: result });
  }
}

function mapStateToProps(state) {
  return {
    api: state.apiReducer,
  }
}


export default connect(mapStateToProps)(Scheduler);
