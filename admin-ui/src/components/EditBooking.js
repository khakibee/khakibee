import React, { Component, forwardRef } from 'react';
import DatePicker, { registerLocale } from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";
import { Input } from 'reactstrap';
import el from 'date-fns/locale/el';
import { addDays } from 'date-fns';
registerLocale('el', el)

const API = process.env.REACT_APP_API;
const SESSIONS = API + '/api/gamesess';
const BOOKINGS = API + '/api/bookings';

const dte = new Date()
class EditBooking extends Component {

    constructor(props) {
        super(props);

        this.state = {
            bookings: [],
            datepicker: true,
            sessions: [],
            selectedTime: "",
            selectedTimes: [],
            startDate: this.props.dte
        };

    }

    render() {
        const { datepicker, selectedTimes, selectedTime, startDate } = this.state
        const ExampleCustomInput = forwardRef(
            ({ value, onClick }, ref) => (
                <button className="example-custom-input" onClick={onClick} ref={ref}>
                    {value}
                </button>
            ))


        return (
            <div className="d-flex content-justify-between mt-2">

                <div className="col-md-9">
                    <DatePicker
                        locale="el"
                        selected={startDate}
                        onCalendarOpen={() => this.setState({ datepicker: !datepicker })}
                        onChange={date => this.updateInjectTimes(date)}
                        minDate={dte}
                        maxDate={addDays(dte, 90)}
                        highlightDates={[this.props.dte]}
                        dateFormat="MMMM d, yyyy"
                        customInput={<ExampleCustomInput />}
                    />
                </div>

                <Input name="time" id="time" type="select" className="custom-select ml-2 col-md-3" value={selectedTime}
                    onChange={e => this.setState({ selectedTime: e.target.value })} disabled={!selectedTimes.length}>
                    {selectedTimes}
                </Input>

            </div>)
    }

    componentDidMount() {
        fetch(SESSIONS, {
            method: 'GET',
            headers: { 'Authorization': 'Bearer ' + localStorage.token }
        })
            .then(response => {
                if (response.status === 200)
                    return response.json();
                else
                    console.log("SESSIONS Error:", response.status)
            })
            .then(data => {
                if (!data)
                    return
                this.setState({ sessions: data })
            })

        fetch(BOOKINGS + '?rm=' + this.props.rm_id, {
            method: 'GET',
            headers: { 'Authorization': 'Bearer ' + localStorage.token }
        })
            .then(response => {
                if (response.status === 200)
                    return response.json();
                else
                    console.log("Error:", response.status)
            })
            .then(data => {
                if (!data)
                    return
                this.setState({ bookings: data })
            })

    }

    componentDidUpdate(prevState, prevProps) {
        !prevState.editted && this.props.editted && this.props.setEditFlag()
        !prevState.editted && this.props.editted && this.props.getDateTime(this.state.startDate, this.state.selectedTime)
    }

    updateInjectTimes = (date) => {
        var { bookings, sessions } = this.state
        var rm_id = this.props.rm_id, currentDay = date.getDay(), selectedTimes = [], selectedTime
        let ses = sessions && sessions[currentDay]
        let filteredSession = ses && ses.filter(ses => ses.rm_id === rm_id)
        let filteredBookings = bookings && bookings.filter(bkng => {
            let utcMonth1 = (date.getUTCMonth() + 1)
            if (utcMonth1 < 10) { utcMonth1 = "0" + utcMonth1 }
            let currDte = date.getUTCFullYear() + "-" + utcMonth1 + "-" + date.getUTCDate()

            let bkngDte = new Date(bkng.dte)
            let utcMonth2 = (bkngDte.getUTCMonth() + 1)
            if (utcMonth2 < 10) { utcMonth2 = "0" + utcMonth2 }
            let bkngDate = bkngDte.getUTCFullYear() + "-" + utcMonth2 + "-" + bkngDte.getUTCDate()

            return bkng.session_id === currentDay && bkngDate === currDte
        })

        if (filteredSession.length < 1 && filteredBookings.length < 1)
            return

        filteredSession.forEach(fses => {
            if (filteredBookings.length < 1)
                selectedTimes.push(<option key={fses.tmStarting} value={fses.tmStarting}>{fses.tmStarting}</option>)

            filteredBookings && filteredBookings.forEach(fbkng => {
                let bkngDate = new Date(fbkng.dte), bkngHours = bkngDate.getHours(), bkngMinutes = bkngDate.getMinutes()

                if (bkngMinutes < 10) bkngMinutes = "0" + bkngMinutes

                let bkngTme = bkngHours + ":" + bkngMinutes

                let utcMonth = (date.getUTCMonth() + 1)
                if (utcMonth < 10) utcMonth = "0" + utcMonth

                let currDte = date.getUTCFullYear() + "-" + utcMonth + "-" + date.getUTCDate()
                let fbkngDte = fbkng.dte.split(" ");

                if (currDte !== fbkngDte[0]) {
                    selectedTimes.push(<option key={fses.tmStarting} value={fses.tmStarting}>{fses.tmStarting}</option>)
                    selectedTime = fses.tmStarting
                } else
                    if (fses.tmStarting !== bkngTme) {
                        selectedTimes.push(<option key={fses.tmStarting} value={fses.tmStarting}>{fses.tmStarting}</option>)
                        selectedTime = fses.tmStarting
                    }
            })

        })

        // console.log("selectedTimes: ", selectedTimes, selectedTimes)

        this.setState({
            startDate: date,
            selectedTimes: selectedTimes,
            selectedTime: selectedTimes.length === 1 ? selectedTime : ""
        }, this.props.setEditFlag)
    }

}

export default EditBooking;