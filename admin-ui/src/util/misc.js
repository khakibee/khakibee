function capitalize(string) {
    if (typeof string !== 'string')
        return ''

    return string.charAt(0).toUpperCase() + string.slice(1)
}

function lowercase(string) {
    if (typeof string !== 'string')
        return ''

    return string.charAt(0).toLowerCase() + string.slice(1)
}

export { capitalize, lowercase };