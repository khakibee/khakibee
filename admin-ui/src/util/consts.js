const sessIDs = {
    0: [1, 120],
    1: [1, 28],
    2: [29, 57],
    3: [58, 85],
    4: [86, 120],
}

const buttonIcons = {
    0: { button: "btn-xing", icon: "fa fa-th-large" },
    1: { button: "btn-spotify", icon: "fa fa-home" },
    2: { button: "btn-youtube", icon: "fa fa-book" },
    3: { button: "btn-instagram", icon: "fa fa-music" },
    4: { button: "btn-flickr", icon: "fa fa-puzzle-piece" },
    5: { button: "btn-html5", icon: "fa fa-key" },
    6: { button: "btn-yahoo", icon: "fa fa-trophy" },
}

export { sessIDs, buttonIcons };