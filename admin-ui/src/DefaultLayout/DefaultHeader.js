import React, { Component } from 'react';
import { Badge, UncontrolledDropdown, DropdownItem, DropdownMenu, DropdownToggle, Nav } from 'reactstrap';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import classnames from 'classnames';
import Websocket from 'react-websocket';
import { AppNavbarBrand, AppSidebarToggler } from '@coreui/react';
import Logo from '../assets/img/brand/ppLogo.svg';
import Brandlogo from '../assets/img/brand/ppBrandLogo.svg';
import Loader from 'react-loader-spinner';
import { Button } from 'reactstrap';
import { capitalize } from '../util/misc';

const API = process.env.REACT_APP_API;
const WS = process.env.REACT_APP_WS;
const NOTIFICATIONS = API + '/api/notifications';

const propTypes = { children: PropTypes.node };
const defaultProps = {};

const notificationIcons = {
  1: { color: "villa", icon: "fa fa-home" },
  2: { color: "bookstore", icon: "fa fa-book" },
  3: { color: "conservatory", icon: "fa fa-music" },
  4: { color: "villa-paros", icon: "fa fa-puzzle-piece" }
}

class DefaultHeader extends Component {
  constructor(props) {
    super(props);

    this.state = {
      dirty: false,
      loadMore: false,
      offset: 0,
      notifications: [],
      count: 0,
      unviewedCount: 0
    };

  }

  render() {
    const { count, loadMore, notifications, unviewedCount } = this.state

    const { children, ...attributes } = this.props // eslint-disable-next-line
    const loggedIn = localStorage.token ? true : false
    const loginFunc = loggedIn ? () => { localStorage.removeItem("token"); this.props.logOut(); } : null

    var websocket = <div> <Websocket url={`${WS}/ws`} onMessage={this.handleNotifications} />  </div>

    return (
      <>
        {websocket}

        <AppSidebarToggler className="d-lg-none" display="md" mobile />
        <AppNavbarBrand
          full={{ src: Logo, width: 95, height: 30, alt: 'Paradox project Logo' }}
          minimized={{ src: Brandlogo, width: 32, height: 30, alt: 'Paradox project Logo' }}
        />
        <AppSidebarToggler className="d-md-down-none" display="lg" />

        <Nav className="ml-auto" navbar>
          <UncontrolledDropdown nav direction="left">
            <DropdownToggle nav>
              <i className="icon-bell fa-lg mr-1" /><Badge pill color="dark">{unviewedCount.length}</Badge>
            </DropdownToggle>
            <DropdownMenu className="pp-border-menu" right
              style={{ position: "absolute", willChange: "transform", top: "0px", left: "0px", transform: "translate3d(-210px, 25px, 0px)" }}>
              <div className="dropdown-menu notifications show" role="menu" aria-hidden="false"
                style={{ margin: "-20px", position: "absolute", inset: "0px auto auto 0px", transform: "translate3d(14px, 28px, 0px)", minWidth: "15rem" }}
                data-popper-placement="bottom-start">
                <div className="text-center dropdown-header bg-dark" tabIndex="-1">
                  <strong className="text-light">{notifications.length}/{count} ειδοποιήσεις</strong>
                </div>

                {this.notifications()}

                <div className="col-md-12 mt-2">
                  <div className="text-center">
                    {/* <p><small className="text-muted">7 από 25</small></p> */}
                    {!loadMore && <Button color="btn btn-outline-dark m-0 mb-2" onClick={(e) => this.setState({ loadMore: true }, this.handleNotifications)}>Περισσότερα</Button>}
                    <Loader className="mb-2" visible={loadMore} type="TailSpin" color="Black" height={35} width={35} />
                  </div>
                </div>

              </div>
            </DropdownMenu>
          </UncontrolledDropdown>

          <UncontrolledDropdown direction="down">
            <DropdownToggle nav className="mr-2">
              <i className="icon-user fa-lg"></i>
              <i className="ml-2 small icon-arrow-down"></i>
            </DropdownToggle>
            <DropdownMenu right>
              <DropdownItem onClick={loginFunc}>
                <div>
                  <i className="icon-lock" /> Αποσύνδεση
                </div>
              </DropdownItem>
            </DropdownMenu>
          </UncontrolledDropdown>
        </Nav>
      </>
    );
  }

  componentDidMount() {
    this.handleNotifications("DIRTY")
  }

  handleNotifications = (data) => {
    var body, { count, loadMore, offset, notifications } = this.state
    this.setState({ dirty: data === "DIRTY" })

    if (loadMore)
      body = { "offset": offset }

    if (loadMore || data === "DIRTY")
      fetch(NOTIFICATIONS, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + localStorage.token
        },
        body: JSON.stringify(body)
      })
        .then(response => {
          if (response.status === 200)
            return response.json();
          else
            console.log("Error:", response.status)
        })
        .then(data => {
          let offset = loadMore ? notifications.length + data.notifications.length : data.notifications.length

          let allNotifications = notifications
          allNotifications = loadMore ? allNotifications.concat(data.notifications) : data.notifications

          let unviewedCount = allNotifications.filter(ntf => { return !ntf.viewed })

          if (data.notifications) {
            this.setState({
              dirty: false,
              loadMore: false,
              notifications: allNotifications,
              count: loadMore && !data.count ? count : data.count,
              offset: offset,
              unviewedCount: unviewedCount
            })
          }
        })
  }

  notificationViewed = (bkng_id) => {
    var viewedParam

    const notificationIcons = this.state.notifications.map(ntf => {
      if (ntf.bkng_id === bkng_id) {
        ntf.viewed = !ntf.viewed
        viewedParam = ntf.viewed ? 1 : 0
      }
      return ntf
    })

    fetch(NOTIFICATIONS + "/update", {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.token
      },
      body: JSON.stringify({
        bkng_id: bkng_id,
        viewed: viewedParam
      })
    })
      .then(response => {
        if (response.status === 200) {
          this.setState({ notifications: notificationIcons })
        }
      })
  }

  notifications = () => {
    let notifications = []

    this.state.notifications && this.state.notifications.forEach(ntf => {
      let notificationsClasses = classnames('d-flex', 'justify-content-between', 'd-block', 'dropdown-item', 'clickable', { 'bg-secondary': ntf.viewed })
      let notBkngDateClasses = classnames({ 'text-muted': ntf.viewed }, { 'text-dark': !ntf.viewed })
      let notPlayerDataClasses = classnames('small', 'text-truncate', { 'text-muted': ntf.viewed }, { 'text-dark': !ntf.viewed })

      notifications.push(
        <a className={notificationsClasses} onClick={() => this.notificationViewed(ntf.bkng_id)} role="menuitem" key={ntf.bkng_id}>
          <div>
            <div className="d-flex flex-column">
              <small className={notBkngDateClasses}><strong>{ntf.dte}</strong>, {ntf.plr_num} Παίκτες</small>
              {!!ntf.src && <small><i className="fa fa-arrow-right" />Booked by {capitalize(ntf.src)}</small>}
            </div>
            <div className="text-truncate font-weight-bold"><span className={notificationIcons[ntf.rm_id].color + " " + notificationIcons[ntf.rm_id].icon}></span> {ntf.rm_nme}</div>
            <div className={notPlayerDataClasses}><strong>{ntf.nme}</strong>, {ntf.phone}</div>
          </div>

          {!ntf.viewed ?
            <span className="notification-status fa fa-circle" onClick={() => this.notificationViewed(ntf.bkng_id)} />
            : <span className="notification-status fa fa-check-circle" />}
        </a>
      )
    })

    return notifications
  }

}

function mapStateToProps(state) {
  return {
    user: state.userReducer,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    logIn: (user_id) => {
      dispatch({
        type: "SET_USER",
        payload: user_id,
      });
    },
    logOut: () => {
      dispatch({
        type: "LOG_OUT",
      });
    },
  }
}

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;

export default connect(mapStateToProps, mapDispatchToProps)(DefaultHeader);
