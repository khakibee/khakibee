import React, { Component } from 'react';
import PropTypes from 'prop-types';
import packageJSON from '../../package.json';

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

class DefaultFooter extends Component {
  render() {

    // eslint-disable-next-line
    const { children, ...attributes } = this.props;

    return (
      <>
        <span className="h7"><a className="cursor-pointer" href="http://paradoxproject.gr/" target="_blank" rel="noopener noreferrer">Paradox</a>&copy; 2020, v{packageJSON.version}</span>
        <span className="h7 ml-auto">Powered by <b>Khakibee</b></span>
      </>
    );
  }
}

DefaultFooter.propTypes = propTypes;
DefaultFooter.defaultProps = defaultProps;

export default DefaultFooter;
