import React, { Component } from 'react';
import Websocket from 'react-websocket';
import withTranslation from './withTranslation';
import BkngDate from './BkngDate';
import BkngForm from './BkngForm';
import classnames from 'classnames';
import { Button, Row, Col, CardImg, Card, CardHeader, CardBody, CardFooter, Alert } from 'reactstrap';

const API = process.env.REACT_APP_API;
const WS = process.env.REACT_APP_WS;

class Calendar extends Component {
  constructor(props, context) {
    super(props, context);

    let currentDate = new Date();
    currentDate.setHours(0, 0, 0, 0);

    this.state = {
      steps: getInitSteps(currentDate),
      selectedRegion: 0,
      rooms: [],
      step: 0,
      currentDate: currentDate,
      selectedMonth: { showingDate: "", days: {} },
      monthOffset: 0,
      sessions: [],
      info: '',
      dirty: false,
      selectedRoom: {},
      loading: true
    }

  }


  handleWS = (data) => {
    this.setState({ dirty: data === "DIRTY" });

    if (data === "DIRTY" && !!this.state.selectedRoom.room_id) {
      var room_id = this.state.steps[0].room_id
      fetch(API + "/api/if/bookings/" + room_id, {
        method: 'GET',
      })
        .then(response => {
          if (response.status === 200)
            return response.json();
          else
            console.log("Error:", response.status)
        })
        .then(data => {
          let bookings = data
          var monthOffset = this.state.monthOffset;
          var date = new Date(this.state.currentDate.getFullYear(), this.state.currentDate.getMonth() + monthOffset)
          var sessions = this.state.allSessions

          let chosenDate = this.state.steps[1].date

          let selectedMonth = getMonthSessions(date, room_id, sessions, bookings, this.props.lng)
          let selectedDay = Object.values(selectedMonth.days).find((day) => day.date.getTime() === chosenDate.getTime())

          this.setState({
            sessions: selectedDay ? selectedDay.sess : [],
            allBookings: bookings,
            selectedMonth: selectedMonth,
            dirty: false
          })
        })
    }
  }

  render() {
    const websocket = <Websocket url={`${WS}/ws`} onMessage={this.handleWS} />

    const { lng, tr } = this.props
    const { loading, selectedMonth, step, selectedRegion, regions, rooms } = this.state
    const steps = { ...this.state.steps }
    const showingDate = selectedMonth.showingDate;

    var monthDays = [], rgns = [], rms = [], sessions = []


    switch (step) {
      case 0:
        if (!loading)

          if (!selectedRegion)
            regions.forEach(rgn => {
              rgns.push(<Col sm="6" className={rgn.rgn_id == 2 ? 'img-rgn' : ''} key={rgn.rgn_id}>
                <img className="photos clickable rgn-width"
                  onClick={() => this.setState({ selectedRegion: rgn.rgn_id })}
                  src={rgn.img}
                />

                {rgn.rgn_id == 2 ?
                  <>
                    <div id="ribbon2" onClick={() => this.setState({ selectedRegion: rgn.rgn_id })}>
                      <span>{rgn.nme}</span>
                    </div>
                  </>
                  : <>
                    <div id="ribbon1" onClick={() => this.setState({ selectedRegion: rgn.rgn_id })}>
                      <span className="flavors">{rgn.nme}</span>
                    </div>
                  </>}

              </Col>)
            })
          else
            rooms.forEach(rm => {
              if (rm.rgn_id === selectedRegion)
                rms.push(<Col sm="4" key={rm.room_id}>
                  <Card onClick={() => this.selecteRoom(rm.room_id)}>
                    {rm.room_id > 2 && <div className="ribbon-online-cat"><span>{tr.ribbon}</span></div>}
                    <CardImg top className="photos clickable" width="100%" src={rm.img} />
                    <CardBody className="text-center">
                      <Col lg={12} className="text-center space-bottom">
                        <Button className={"btn btn-brand btn-client btn-block"}>
                          <span>{rm.nme}</span>
                        </Button>
                      </Col>
                      <div className="d-flex justify-content-between">
                        <small><i className="icon fa fa-hourglass-half" /> <b>{tr.dur} {rm.duration}</b></small>
                        <small><i className="icon fa fa-users" /> <b>{tr.numOfPlayers} {rm.plr_num}</b></small>
                      </div>
                    </CardBody>
                  </Card>
                </Col>)
            })

        break;
      case 1:
        sessions = this.state.sessions.map((sess, idx) => <Button key={idx} className="btn btn-vimeo" size="lg" block onClick={() => this.openBooking(sess)}>{sess.tm_start}</Button>)
        monthDays = Object.values(selectedMonth.days).map((day, idx) =>
          <BkngDate key={idx} date={day.date} sessions={day.sess} currentDate={this.state.currentDate} click={this.showSessions} selectedDay={steps[1].date} />)

        break;
    }

    const calendarCard = <div className="col-md-6">
      <Card>
        <CardHeader className="bg-paradox">
          <Row className="d-flex justify-content-between">
            <Col className="col-2 text-right"><a disabled={this.state.monthOffset === 0} onClick={() => this.handleMonthOffset("left")}><i className="fa fa-arrow-left clickable" /> </a></Col>
            <Col className="text-center col-8"><strong>{showingDate}</strong></Col>
            <Col className="col-2"><a disabled={this.state.monthOffset === 1} onClick={() => this.handleMonthOffset("right")}> <i className="fa fa-arrow-right clickable" />  </a></Col>
          </Row>
          <hr />
          <Row className="d-flex">
            <div className="weekday-col">{tr.week.mo}</div>
            <div className="weekday-col">{tr.week.tu}</div>
            <div className="weekday-col">{tr.week.we}</div>
            <div className="weekday-col">{tr.week.th}</div>
            <div className="weekday-col">{tr.week.fr}</div>
            <div className="weekday-col">{tr.week.sa}</div>
            <div className="weekday-col">{tr.week.su}</div>
          </Row>
        </CardHeader>
        <CardBody>
          <Row>
            {monthDays}
          </Row>
        </CardBody>
      </Card>
    </div>

    var sessionsCard = <div className="col-md-6">
      <Card>
        <CardHeader className="text-center bg-paradox">
          <strong>{steps[1].date && new Date(steps[1].date).toLocaleDateString(lng, { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' })}</strong>
        </CardHeader>
        <CardBody>
          {!sessions.length && <Alert color="danger">{tr.noRoomsAvail}</Alert>}
          {sessions}
        </CardBody>
      </Card>
    </div>

    var formCard = <BkngForm
      selectedDay={steps[1].date}
      event={steps[1].session}
      room={this.state.selectedRoom.room_id}
      onComplete={this.bookingComplete}
      handleBack={this.handleBack}
      tr={tr}
      lang={lng}
      title={steps[0].title} />

    var finishCard = <>
      <Card>
        <CardHeader className="text-center rounded-0">
          <h4>{tr.bookingInfo}</h4>
        </CardHeader>
        <Alert color="success">
          <p className="alert-heading"> <i className="fa fa-check-square icon"></i> {tr.checkEmail}</p>
          <p> <i className="fa fa-exclamation-circle icon"></i> <strong>{tr.important}</strong>
            {tr.infoAboutMail}
          </p>
        </Alert>
        <Alert color="danger">
          <p> <i className="fa fa-exclamation-triangle icon"></i> <strong>{tr.covidTitle}</strong>
            {tr.covidMsg}
          </p>
        </Alert>
        <CardBody>
          <div>
            <h5>{steps[0].title[lng]}, {tr.form.players} {steps[2].bkng.plr_num} {"  -  "}{steps[1].date.toLocaleString(lng, { weekday: 'long', day: "numeric", month: 'long', year: 'numeric', hour: 'numeric', minute: 'numeric', hour12: false }
            )}</h5>
            <a className="fa fa-map-pin bk-title icon clickable" href={steps[0].map} target="_blank" rel="noopener noreferrer">{tr.weAreHere}</a>
            <hr />
            <div>

              <Row>
                <Col md="8">
                  <Row>
                    <Col>{tr.address} </Col>
                    <Col className="h5"><b>{steps[0].address[lng]}</b></Col>
                  </Row>
                  <Row>
                    <Col>{tr.form.name} </Col>
                    <Col><b>{steps[2].bkng.nme}</b></Col>
                  </Row>
                  <Row>
                    <Col>{tr.form.email} </Col>
                    <Col><b>{steps[2].bkng.email}</b></Col>
                  </Row>
                  <Row>
                    <Col>{tr.form.mobile} </Col>
                    <Col><b>{steps[2].bkng.phone}</b></Col>
                  </Row>
                </Col>

                <Col md="4">
                  <img className="t-shirt" src="https://res.cloudinary.com/hwrkhvisl/image/upload/v1589025884/Paradox%20Project/cpuuzawnt72bshegn11h.jpg"></img>
                </Col>

              </Row>
            </div>
          </div>
        </CardBody>
        <CardFooter className="text-center"> <Button className="btn-paradox" onClick={() => this.handleBack(-1)}>{tr.newBooking}</Button></CardFooter>
      </Card>
    </>


    return (<>

      <div className="cl-app-body">
        <div className="cl-container">

          {/* <!-- Main content --> */}
          <main className="main">
            {/* <div className="justify-content-center">  
              <div className="col-lg-12 my-4">
                <div className="col text-center">
                  <h1 className="h3 font-weight-bold"><span className="badge badge-secondary bk-title">{tr.bkngSystem}</span></h1>
                </div>
              </div>
            </div> */}
            <div className="mt-4" />

            {/* <!-- Steppers --> */}
            <div className="justify-content-center">
              <div className="col-lg-12">
                <ul className="stepper stepper-horizontal">

                  {this.handleSteps()}

                </ul>
              </div>
            </div>
            {/* <!-- Steppers End --> */}

            {!this.state.loading && <div className="row">
              {step === 0 && !selectedRegion && rgns}
              {step === 0 && !!selectedRegion && rms}
              {step === 1 && calendarCard}
              {step === 1 && sessionsCard}
              {step === 2 && formCard}
              {step === 3 && finishCard}
            </div>}

            {websocket}
          </main>

        </div>
      </div>
    </>
    )
  }

  componentDidMount() {
    this.setState({ loading: true })
    let lng = this.props.lng

    const REGIONS_RESULT = fetch(API + "/api/if/regions?lng=" + lng, {
      method: 'GET',
    })
      .then(response => {
        if (response.status === 200)
          return response.json();
        else
          console.log("Regions Error:", response.status)
      })
      .then(data => {
        if (data) return data;
      })

    const ROOMS_RESULT = fetch(API + "/api/if/rooms?lng=" + lng, {
      method: 'GET',
    })
      .then(response => {
        if (response.status === 200)
          return response.json();
        else
          console.log("Rooms Error:", response.status)
      })
      .then(data => {
        if (data) return data;
      })


    Promise.all([REGIONS_RESULT, ROOMS_RESULT]).then((data) => {
      if (!data[0])
        return

      this.setState({
        loading: false,
        regions: data[0],
        rooms: data[1]
      })
    })
  }

  handleSteps = () => {
    var { step, steps } = this.state
    const tr = this.props.tr

    var step0 = classnames({ 'active': step === 0 }, { 'completed': step > 0 }, { 'todo': step < 0 })
    var step1 = classnames({ 'active': step === 1 }, { 'completed': step > 1 }, { 'todo': step < 1 })
    var step2 = classnames({ 'active': step === 2 }, { 'completed': step > 2 }, { 'todo': step < 2 })

    var disableSteps = [0, 1, 2].includes(step)
    var pencil = 'icon-pencil', check = 'fa fa-check', small = 'small'
    var step0Icon = classnames(small, { [pencil]: step <= 0 }, { [check]: step > 0 })
    var step1Icon = classnames(small, { [pencil]: step <= 1 }, { [check]: step > 1 })
    var step2Icon = classnames(small, { [pencil]: step <= 2 }, { [check]: step > 2 })

    return <>
      {/* <!-- Zero Step --> */}
      <li className={step0}>
        <span className="astep" onClick={() => disableSteps && this.handleBack(0)}>
          <span className="circle">
            <i className={step0Icon} />
          </span>
          <span className="label">{tr.tabs.room}</span>
          {!!steps[0].choice && <span className="choice"><br />{steps[0].choice}</span>}
        </span>
      </li>

      {/* <!-- First Step --> */}
      <li className={step1}>
        <span className="astep" onClick={() => disableSteps && this.handleBack(1)}>
          <span className="circle">
            <i className={step1Icon} />
          </span>
          <span className="label">{tr.tabs.date}</span>
          {!!steps[1].choice && <span className="choice"><br />{steps[1].choice}</span>}
        </span>
      </li>

      {/* <!-- Second Step --> */}
      <li className={step2}>
        <span className="astep" onClick={() => disableSteps && this.handleBack(2)}>
          <span className="circle">
            <i className={step2Icon} />
          </span>
          <span className="label">{tr.tabs.form}</span>
        </span>
      </li>
    </>
  }

  selecteRoom = (room_id) => {
    var fetchSessions = fetch(API + "/api/if/sessions", {
      method: 'GET',
    })
      .then(response => {
        if (response.status === 200)
          return response.json();
        else
          console.log("Error:", response.status)
      })

    var fetchBookings = fetch(API + "/api/if/bookings/" + room_id, {
      method: 'GET',
    })
      .then(response => {
        if (response.status === 200)
          return response.json();
        else
          console.log("Error:", response.status)
      })

    Promise.all([fetchSessions, fetchBookings]).then(([sessions, bookings]) => {
      var { currentDate, step, steps, rooms } = this.state
      let room = rooms.find(rm => rm.room_id === room_id)

      steps[0].title = {
        el: room.nme,
        en: room.nme,
      }
      steps[0].address = {
        el: room.addr,
        en: room.addr,
      }
      steps[0].map = room.map
      steps[0].choice = steps[0].title[this.props.lng]
      steps[0].room_id = room.room_id
      steps[1].choice = null

      let date = new Date(currentDate.getFullYear(), currentDate.getMonth())
      let selectedMonth = getMonthSessions(date, room_id, sessions, bookings, this.props.lng)

      this.setState({
        allSessions: sessions,
        allBookings: bookings,
        selectedMonth: selectedMonth,
        step: step + 1,
        steps: steps,
        selectedRoom: room,
        monthOffset: 0
      })
    })
  }

  handleBack = (currStep) => {
    var { region, step, steps } = this.state

    if (currStep >= step && currStep !== 0)
      return

    if (currStep === -1) {
      let currentDate = new Date()
      currentDate.setHours(0, 0, 0, 0)
      steps = getInitSteps(currentDate)
      step = 1
    }
    // else if (currStep == 0) {
    //   let step0 = { ...steps[step], choice: null }
    //   steps = { ...steps, [step]: step0 }
    // }

    this.setState({
      region: !!region ? "" : region,
      step: step > 0 ? step - 1 : 0,
      steps: steps
    })
  }

  bookingComplete = (bkng) => {
    let steps = this.state.steps
    steps[2] = { ...steps[2], bkng: bkng }
    this.setState({
      steps: steps,
      step: this.state.step + 1

    })
  }

  openBooking = (session) => {
    let { step, steps } = this.state

    let tm_start = session.tm_start.split(":")
    steps[step].date.setHours(tm_start[0], tm_start[1])

    let options = { day: 'numeric', month: 'numeric', year: 'numeric', hour: 'numeric', minute: 'numeric', hour12: false };
    steps[step].choice = steps[step].date.toLocaleString('el', options)

    steps[step].session = session

    this.setState({
      step: this.state.step + 1,
      steps: steps
    })
  }

  showSessions = (sessions, date) => {
    let { step, steps } = this.state

    steps[step].choice = null
    steps[step].date = date
    steps[step].session = null

    this.setState({
      sessions: sessions,
      steps: steps
    })
  }

  handleMonthOffset = (direction) => {
    var monthOffset = direction === "right" ? this.state.monthOffset + 1 : this.state.monthOffset - 1;
    var date = new Date(this.state.currentDate.getFullYear(), this.state.currentDate.getMonth() + monthOffset)
    var room_id = this.state.steps[0].room_id
    var sessions = this.state.allSessions
    var bookings = this.state.allBookings

    let selectedMonth = getMonthSessions(date, room_id, sessions, bookings, this.props.lng)
    this.setState({
      monthOffset: monthOffset,
      selectedMonth: selectedMonth
    });
  }
}

function getMonthSessions(date, room, sessions, bookings, lang) {
  const monthDatesObj = {};
  const showingDate = new Intl.DateTimeFormat(lang, { month: 'long', year: 'numeric' }).format(date);
  const daysInMonth = new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate();
  const daysInPrevMonth = new Date(date.getFullYear(), date.getMonth(), 0).getDate();
  var firstDayInMonth = new Date(date.getFullYear(), date.getMonth(), 1).getDay();
  firstDayInMonth = firstDayInMonth === 0 ? 7 : firstDayInMonth //making Sunday the last day of the week
  const curMonth = date.getMonth();
  const curYear = date.getFullYear();
  var day = 0;
  var dayIndx = 0;
  var dayObj = {};

  let threeHoursFromNow = new Date(Date.now() + 3 * 60 * 60 * 1000)
  let twoHoursFromNow = new Date(Date.now() + 2 * 60 * 60 * 1000)
  let threeWeeksTime = new Date(Date.now() + 21 * 24 * 60 * 60 * 1000)

  for (let i = 0; i < 6; i++) { //weeks
    if (dayIndx > daysInMonth) break;
    for (let j = 1; j <= 7; j++) { //days
      dayIndx++;
      let month = curMonth;
      let year = curYear;

      day = j + (i * 7) - firstDayInMonth + 1;
      if (i === 0 && j < firstDayInMonth) {
        day = daysInPrevMonth + day;
        if (curMonth === 0) year = curYear - 1;
        month = curMonth === 0 ? 11 : curMonth - 1;
      } else if (day > daysInMonth) {
        day = day - daysInMonth
        if (curMonth === 12) year = curYear + 1;
        month = curMonth === 12 ? 1 : curMonth + 1;
      }
      let wkday = j === 7 ? 0 : j
      let room_sessions = sessions[wkday].filter((session) => {
        if (session.room_id !== room)
          return false

        let date = `${year}-${month + 1 > 9 ? month + 1 : "0" + (month + 1)}-${day > 9 ? day : "0" + day} ${session.tm_start}`
        let is_open = !bookings.length || bookings.findIndex(booking => booking.session_id === session.session_id && booking.dte.slice(11, 16) === session.tm_start && booking.dte.slice(-19, -3) === date) < 0

        //formating date for date constructor
        date = new Date(date.replace(' ', 'T') + '+03:00')

        return is_open && date >= (room !== 3 ? threeHoursFromNow : twoHoursFromNow) && date <= threeWeeksTime
      })

      dayObj[`${day}/${month}/${year}`] = { date: new Date(year, month, day), sess: room_sessions }
    }
  }
  monthDatesObj.showingDate = showingDate;
  monthDatesObj.days = dayObj;
  return monthDatesObj;
}

function getInitSteps(currentDate) {
  var initSteps = {
    0: { descr: "room", title: {}, address: {}, map: "" },
    1: { descr: "date", date: currentDate },
    2: { descr: "form", bkng: {} },
    3: { descr: "booking", last: true }
  }

  return initSteps
}

export default withTranslation(Calendar)