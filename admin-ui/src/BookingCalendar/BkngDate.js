import React, { Component } from 'react';

export default class BkngDate extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {};
    if (this.props.date.getTime() === this.props.currentDate.getTime())
      this.props.click(this.props.sessions, this.props.date);
  }

  render() {
    const dateTime = this.props.date.getTime()
    const selectedDayTime = this.props.selectedDay.getTime()
    const currentDateTime = this.props.currentDate.getTime()
    const sessions = this.props.sessions

    var classes = ["btn", "btn btn-paradox"];
    if (dateTime < currentDateTime || !sessions.length)
      classes.push("disabled");
    else if (dateTime === selectedDayTime)
      classes.push("bg-warning");
    else if (dateTime === currentDateTime)
      classes.push("bg-dark");

    return (
      <div className="weekday-col">
        <div
          className={addClasses(classes)}
          disabled={classes.includes("disabled")}
          style={{ display: "block", padding: "0.375rem 0" }}
          onClick={() => !classes.includes("disabled") && this.props.click(sessions, this.props.date)}>
          {this.props.date.getDate()}
        </div>
      </div>
    )
  }

}

function addClasses(classes) {
  return classes.join(" ")
};
