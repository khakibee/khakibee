import React, { Component } from 'react';
import { PacmanLoader } from 'react-spinners';
import ValidationInput from '../components/Validation';
import CountUp from 'react-countup';
import { Alert, Button, Col, Row, Form, FormGroup, Input, Label } from 'reactstrap';

const API = process.env.REACT_APP_API;


export default class BkngForm extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      loading: false,
      alertPP1: false,
      errorFlag: false,
      players: new ValidationInput("", (value) => value !== ""),
      name: new ValidationInput("", (value) => /^[α-ωΑ-Ω-ωίϊΐόάέύϋΰήώa-zA-Z!-+='*&^$#@0-9 ]{2,75}$/.test(value)),
      email: new ValidationInput("", (value) => /\b[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}\b/.test(value)),
      mobile: new ValidationInput("", (value) => /^[0-9]{10,20}$/.test(value)),
      diff: new ValidationInput("", (value) => value !== ""),
      plrLvl: new ValidationInput("", (value) => value !== ""),
      learnedUs: new ValidationInput("", (value) => value !== ""),
      teamName: new ValidationInput("", (value) => /^[α-ωΑ-Ω-ωίϊΐόάέύϋΰήώa-zA-Z !^%,.(){}'@#&*-_+=:|?;0-9]{0,100}$/.test(value)),
      playRoom1: new ValidationInput("", (value) => value !== ""),
      escRoom1: new ValidationInput("", (value) => value !== ""),
      lang: new ValidationInput(this.props.lang === "el" ? "el" : "", (value) => value !== "0"),
      code: new ValidationInput("", (value) => /^[A-Z 0-9]{0,5}$/.test(value)),
      age: new ValidationInput(this.props.lang === "el" ? "s" : "", (value) => value !== "0"),
      notes: new ValidationInput("", (value) => /^[α-ωΑ-Ω-ωίϊΐόάέύϋΰήώa-zA-Z 0-9()!^%{}'@#&*-_+=:|?;,.?;]{0,500}$/.test(value)),
      tncs: new ValidationInput(false, (value) => value !== false, "boolean"),
      invalidInputs: [],
      manCode: new ValidationInput("", (value) => /^[0-9]{9}$/.test(value), "number"),
      manCodeCashIn: false
    };

  }

  render() {
    const tr = this.props.tr

    const manSave = this.state.manCodeCashIn && <CountUp
      start={0}
      end={5}
      decimals={2}
      delay={.5}
      duration={5}
    />;

    return (
      <div className="container">
        {!this.state.loading && !this.state.errorFlag && <>
          <Form className="row">

            <hr />

            <Col md={6}>

              <FormGroup>
                <Label for="players"><b>{tr.form.players}</b>(*)</Label>
                <Input type="select" id="players" value={this.state.players.value} onChange={(e) => this.handleInputsChange(e.target.value, e.target.id)} invalid={this.state.invalidInputs.includes("players")}>
                  <option value="">{tr.form.playersSelect}</option>
                  {this.props.room === 1 && <>
                    <option value="3">3 x 26€</option>
                    <option value="4">4 x 24€</option>
                    <option value="5">5 x 22€</option>
                    <option value="6">6 x 21€</option>
                    <option value="7">7 x 20€</option> </>}
                  {this.props.room === 2 && <>
                    <option value="4">4 x 30€</option>
                    <option value="5">5 x 30€</option>
                    <option value="6">6 x 27€</option>
                    <option value="7">7 x 27€</option> </>}
                  {this.props.room === 3 && <>
                    <option value="4">4 x 29€</option>
                    <option value="5">5 x 27€</option>
                    <option value="6">6 x 26€</option>
                    <option value="7">7 x 25€</option> </>}
                  {this.props.room === 4 && !this.state.manCodeCashIn && <>
                    <option value="4">4 x 28€</option>
                    <option value="5">5 x 28€</option>
                    <option value="6">6 x 25€</option>
                    <option value="7">7 x 25€</option> </>}
                  {this.props.room === 4 && this.state.manCodeCashIn && <>
                    <option value="4">4 x 23€</option>
                    <option value="5">5 x 23€</option>
                    <option value="6">6 x 20€</option>
                    <option value="7">7 x 20€</option> </>}

                </Input>
                {this.props.room === 3 && <small className="text-muted">{tr.form.playersNote}</small>}
                {this.props.room === 4 && this.props.lang === 'el' && <small className="text-muted">{tr.form.manInfo}</small>}
                {this.state.invalidInputs.includes("players") && <div className="invalid-feedback">{tr.form.playersInvalid}</div>}
              </FormGroup>

              <FormGroup>
                <Label for="name"><b>{tr.form.name}</b>(*)</Label>
                <Input type="text" id="name" value={this.state.name.value} onChange={(e) => this.handleInputsChange(e.target.value, e.target.id)} invalid={this.state.invalidInputs.includes("name")} />
                {this.state.invalidInputs.includes("name") && <div className="invalid-feedback">{tr.form.nameInvalid}</div>}
              </FormGroup>

              <FormGroup>
                <Label for="email"><b>{tr.form.email}</b>(*)</Label>
                <Input type="email" id="email" value={this.state.email.value} onChange={(e) => this.handleInputsChange(e.target.value, e.target.id)} invalid={this.state.invalidInputs.includes("email")} />
                {this.state.invalidInputs.includes("email") && <div className="invalid-feedback">{tr.form.emailInvalid}</div>}
              </FormGroup>

              <FormGroup>
                <Label for="mobile"><b>{tr.form.mobile}</b>(*)</Label>
                <Input type="tel" id="mobile" value={this.state.mobile.value} onChange={(e) => this.handleInputsChange(e.target.value, e.target.id)} invalid={this.state.invalidInputs.includes("mobile")} />
                {this.state.invalidInputs.includes("mobile") && <div className="invalid-feedback">{tr.form.mobileInvalid}</div>}
              </FormGroup>

              <FormGroup>
                <Label for="age"><b>{tr.form.age}</b>(*)</Label>
                <Input type="select" id="age" value={this.state.age.value} onChange={(e) => this.handleInputsChange(e.target.value, e.target.id)} invalid={this.state.invalidInputs.includes("age")}>
                  <option value="">{tr.form.optionSelect}</option>
                  <option value="m">{tr.form.yes}</option>
                  <option value="s">{tr.form.no}</option>
                </Input>
                {this.state.invalidInputs.includes("age") && <div className="invalid-feedback">{tr.form.optionInvalid}</div>}
              </FormGroup>

              <FormGroup>
                <Label for="lang"><b>{tr.form.lang}</b></Label>
                <Input type="select" id="lang" value={this.state.lang.value} onChange={(e) => this.handleInputsChange(e.target.value, e.target.id)} invalid={this.state.invalidInputs.includes("lang")}>
                  <option value="">{tr.form.langSelect}</option>
                  <option value="el">{tr.form.langOption1}</option>
                  <option value="en">{tr.form.langOption2}</option>
                </Input>
                {this.state.invalidInputs.includes("lang") && <div className="invalid-feedback">{tr.form.optionInvalid}</div>}
              </FormGroup>

            </Col>

            <Col md={6}>

              <FormGroup>
                <Label for="diff"><b>{tr.form.difficulty}</b>(*)</Label>
                <Input type="select" id="diff" value={this.state.diff.value} onChange={(e) => this.handleInputsChange(e.target.value, e.target.id)} invalid={this.state.invalidInputs.includes("diff")}>
                  <option value="">{tr.form.diffSelect}</option>
                  <option value="N">{tr.form.diffOption1}</option>
                  <option value="H">{tr.form.diffOption2}</option>
                </Input>
                {this.state.invalidInputs.includes("diff") && <div className="invalid-feedback">{tr.form.diffInvalid}</div>}
              </FormGroup>
              <FormGroup>
                <Label for="plrLvl"><b>{tr.form.playersLevel}</b>(*)</Label>
                <Input type="select" id="plrLvl" value={this.state.plrLvl.value} onChange={(e) => this.handleInputsChange(e.target.value, e.target.id)} invalid={this.state.invalidInputs.includes("plrLvl")}>
                  <option value="">{tr.form.plrLvlSelect}</option>
                  <option value={tr.form.plrLvlOption1}>{tr.form.plrLvlOption1}</option>
                  <option value={tr.form.plrLvlOption2}>{tr.form.plrLvlOption2}</option>
                  <option value={tr.form.plrLvlOption3}>{tr.form.plrLvlOption3}</option>
                  <option value={tr.form.plrLvlOption4}>{tr.form.plrLvlOption4}</option>
                  <option value={tr.form.plrLvlOption5}>{tr.form.plrLvlOption5}</option>
                  <option value={tr.form.plrLvlOption6}>{tr.form.plrLvlOption6}</option>
                  <option value={tr.form.plrLvlOption7}>{tr.form.plrLvlOption7}</option>
                  <option value={tr.form.plrLvlOption8}>{tr.form.plrLvlOption8}</option>
                  <option value={tr.form.plrLvlOption9}>{tr.form.plrLvlOption9}</option>
                </Input>
                {this.state.invalidInputs.includes("plrLvl") && <div className="invalid-feedback">{tr.form.optionInvalid}</div>}
              </FormGroup>

              <FormGroup>
                <Label for="us"><b>{tr.form.learnedUs}</b>(*)</Label>
                <Input type="select" id="learnedUs" value={this.state.learnedUs.value} onChange={(e) => this.handleInputsChange(e.target.value, e.target.id)} invalid={this.state.invalidInputs.includes("learnedUs")}>
                  <option value="">{tr.form.optionSelect}</option>
                  <option value="fb">{tr.form.learnOption0}</option>
                  <option value="go">{tr.form.learnOption1}</option>
                  <option value="ta">{tr.form.learnOption2}</option>
                  <option value="fr">{tr.form.learnOption3}</option>
                  {this.props.room !== 1 && <option value="mn">{tr.form.learnOption4}</option>}
                  {this.props.room !== 2 && <option value="bo">{tr.form.learnOption5}</option>}
                  {this.props.room !== 3 && <option value="co">{tr.form.learnOption6}</option>}
                  <option value="si">{tr.form.learnOption7}</option>
                  <option value="lf">{tr.form.learnOption8}</option>
                </Input>
                {this.state.invalidInputs.includes("learnedUs") && <div className="invalid-feedback">{tr.form.optionInvalid}</div>}
              </FormGroup>

              {this.props.room === 2 && <FormGroup>
                <Label for="playRoom1"><b>{tr.form.playRoom1}</b>(*)</Label>
                <select name="playRoom1" id="playRoom1" value={this.state.playRoom1.value} className="custom-select" onChange={(e) => { e.target.value === "false" && this.handleInputsChange("false", "escRoom1"); this.handleInputsChange(e.target.value, e.target.id) }} invalid={this.state.invalidInputs.includes("playRoom1")}>
                  <option value="">{tr.form.optionSelect}</option>
                  <option value={true}>{tr.form.yes}</option>
                  <option value={false}>{tr.form.no}</option>
                </select >
                {this.state.invalidInputs.includes("playRoom1") && <div className="invalid-feedback">{tr.form.optionInvalid}</div>}
              </FormGroup>}

              {this.props.room === 2 && this.state.playRoom1.value === "false" && <FormGroup>
                <Alert className="alert alert-danger" isOpen={this.state.playRoom1.value === "false"}>
                  <i className="fa fa-exclamation-circle fa-lg"></i>{tr.form.recommendPP1}
                </Alert>
              </FormGroup>}

              {this.props.room === 2 && <FormGroup>
                <Label for="escRoom1"><b>{tr.form.escRoom1}</b>(*)</Label>
                <Input type="select" name="escRoom1" id="escRoom1" value={this.state.playRoom1.value !== "false" ? this.state.escRoom1.value : "false"} className="custom-select" disabled={this.state.playRoom1.value === "false"} onChange={(e) => this.handleInputsChange(e.target.value, e.target.id)} invalid={this.state.invalidInputs.includes("escRoom1")}>
                  <option value="">{tr.form.optionSelect}</option>
                  <option value={true}>{tr.form.yes}</option>
                  <option value={false}>{tr.form.no}</option>
                </Input>
                {this.state.invalidInputs.includes("escRoom1") && <div className="invalid-feedback">{tr.form.optionInvalid}</div>}
              </FormGroup>}

              <FormGroup>
                <Label for="team_name"><b>{tr.form.teamName}</b></Label>
                <Input type="text" id="teamName" value={this.state.teamName.value} onChange={(e) => this.handleInputsChange(e.target.value, e.target.id)} invalid={this.state.invalidInputs.includes("teamName")} />
                {this.state.invalidInputs.includes("teamName") && <div className="invalid-feedback">{tr.form.teamInvalid}</div>}
              </FormGroup>

              {this.props.room !== 4 && <FormGroup>
                <Label for="code"><b>{tr.form.code}</b></Label>
                <Input type="text" name="code-select" id="code" value={this.state.code.value} onChange={(e) => this.handleInputsChange(e.target.value, e.target.id)} invalid={this.state.invalidInputs.includes("code")}></Input>
                {this.state.invalidInputs.includes("code") && <div className="invalid-feedback">{tr.form.codeInvalid}</div>}
              </FormGroup>}

              {this.props.room === 4 && <FormGroup>
                <Label for="manCode"><b>{tr.form.man}</b></Label>
                <div className="input-group">
                  <Input type="text" name="code-select" id="manCode" pattern="[0-9]+" value={this.state.manCode.value} onChange={(e) => this.handleManCode(e)} invalid={this.state.invalidInputs.includes("manCode")}></Input>
                  <div className="input-group-append">
                    <Button className="btn btn-xing" type="button" onClick={() => this.handleInputsChange(this.state.manCode.value, "manCode")}>{tr.form.checkCode}</Button>
                  </div>
                  {this.state.invalidInputs.includes("manCode") && <div className="invalid-feedback">{tr.form.codeInvalid}</div>}
                </div>
              </FormGroup>}

              {this.props.room === 4 && this.state.manCodeCashIn && <Alert className="alert alert-success" isOpen={this.state.manCodeCashIn}>
                <>{tr.form.discount1}<strong className="text-black">{manSave}€</strong>{tr.form.discount2}</>
              </Alert>}

            </Col>

            <Col md={12}>

              <FormGroup>
                <Label for="notes"><b>{tr.form.notes}</b></Label>
                <Input type="textarea" id="notes" value={this.state.notes.value} onChange={(e) => this.handleInputsChange(e.target.value, e.target.id)} invalid={this.state.invalidInputs.includes("notes")} />
                {this.state.invalidInputs.includes("notes") && <div className="invalid-feedback"></div>}
              </FormGroup>

              <FormGroup check>
                <Input type="checkbox" id="tncs" name="tncs" value={this.state.tncs.value} onChange={(e) => { this.handleInputsChange(e.target.value, e.target.id) }} checked={this.state.tncs.value} required />
                <Label check htmlFor="tncs">
                  {tr.form.gdpr1}&nbsp;<a className="bk-title" target='_blank' href="http://paradoxproject.gr/gdpr/">{tr.form.tncs}&nbsp;</a>{tr.form.gdpr2}&nbsp;<a className="bk-title" target='_blank' href="http://paradoxproject.gr/gdpr/">{tr.form.plc}&nbsp;</a>{tr.form.gdpr3}
                </Label>
                {this.state.invalidInputs.includes("tncs") && <div className="invalid-feedback d-block">{tr.form.tncsInvalid}</div>}
              </FormGroup>

            </Col>

          </Form>

          <br />

          <div className="row col mt-4">

            <div className="col">
              <div className="text-center">
                <Button className="btn btn-xing" type="button" onClick={this.handleCompleteBooking}>{tr.form.submitBtn}</Button>
              </div>
            </div>

            <div className="mt-2 offset-md-8 col-md-4">
              <div className="text-right">
                <small>{tr.required}</small>
              </div>
            </div>


          </div>

        </>}

        {
          this.state.loading && !this.state.errorFlag && <div className="mt-3">
            <Row>
              <Col className="text-center">
                <p className="h4 text-dark">{tr.process}</p>
              </Col>
            </Row>
            <Row className="centered">
              <PacmanLoader sizeUnit={"px"} size={50} color={'#0f8888'} />
            </Row>
            <Row>
              <Col className="text-center">
                <span className="h4 text-dark">{tr.loading}</span>
              </Col>
            </Row>
          </div>
        }

        {
          this.state.errorFlag && <div>
            <Row>
              <Col md={12}>
                <Alert color="danger">
                  <p><i className="h2 fa fa-radiation-alt">{tr.errorHeader}</i>
                    <br />{tr.errorMsg}</p>
                  <Button className={"btn btn-dark btn-admin mr-1"} key="error-btn" onClick={(e) => { this.setState({ errorFlag: false }); this.props.handleBack(0) }}>{tr.return}</Button>
                </Alert>
              </Col>
            </Row>
          </div>
        }

      </div >
    )
  }

  handleManCode = (e) => {
    let inputID = e.target.id, value = e.target.value, state = this.state

    state[inputID].value = value
    this.setState({ manCode: state[inputID] })
  }

  handleInputsChange = (inputValue, inputID) => {
    let state = { invalidInputs: this.state.invalidInputs }, manCodeCashIn = this.state.manCodeCashIn
    state[inputID] = this.state[inputID]

    if (state[inputID].type === "string") {
      state[inputID].value = inputValue;
    } else if (state[inputID].type === "number")
      state[inputID].value = inputValue
    else {
      inputValue = inputValue === 'true'
      state[inputID].value = !inputValue;
    }

    if (!state[inputID].validate()) {
      if (!state.invalidInputs.includes(inputID))
        state.invalidInputs.push(inputID)
    } else {
      state.invalidInputs = state.invalidInputs.filter(input => input !== inputID);

      if (inputID === "manCode")
        manCodeCashIn = true
    }

    this.setState({
      invalidInputs: state.invalidInputs,
      manCodeCashIn: manCodeCashIn
    });
  }

  handleCompleteBooking = () => {
    let invalidInputs = [];

    for (var key in this.state) //Validate all inputs before fetching
      if (this.state[key] && typeof this.state[key].validate === "function" && !this.state[key].validate()) { //checking if type is ValidationInput(by lookiing for validate function) and if value is valid
        if (!(key === "manCode" && this.state[key].value === ""))
          invalidInputs.push(key);
      }

    if (this.props.room !== 2) {
      invalidInputs = invalidInputs.filter((value) => { return (value !== "playRoom1" && value !== "escRoom1") });
    }

    if (invalidInputs.length) {
      this.setState({ invalidInputs: invalidInputs });
      return;
    }
    this.setState({ loading: true })

    let selectedDay = new Date(this.props.selectedDay);
    let hoursDiff = selectedDay.getHours() - selectedDay.getTimezoneOffset() / 60;
    selectedDay.setHours(hoursDiff)

    let selectedCode = this.props.room !== 4 ? this.state.code.value : this.state.manCode.value

    let body = {
      "sess_id": this.props.event.session_id,
      "rm_id": this.props.room,
      "roomName": this.props.title.el,
      "nme": this.state.name.value,
      "phone": this.state.mobile.value,
      "email": this.state.email.value,
      "plr_num": parseInt(this.state.players.value),
      "diff": this.state.diff.value,
      "plr_lvl": this.state.plrLvl.value,
      "learned_us": this.state.learnedUs.value,
      "team_nme": this.state.teamName.value,
      "lang": this.state.lang.value,
      "age": this.state.age.value,
      "notes": this.state.notes.value,
      "dte": this.getFormattedDate(selectedDay),
      "bkng_sts_id": 1,
      "code": selectedCode !== "" ? selectedCode : null,
      "playRoom1": this.props.room === 2 ? this.state.playRoom1.value === "true" ? 1 : 0 : 0,
      "escRoom1": this.props.room === 2 ? this.state.escRoom1.value === "true" ? 1 : 0 : 0,
      "gdpr": this.state.tncs.value ? 1 : 0,
      "src": "client"
    }

    fetch(API + `/api/if/booking`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(body)
    })
      .then(response => {
        this.setState({ loading: false })
        if (response.status === 200) {
          this.props.onComplete(body)
        } else {
          this.setState({ errorFlag: true })
          console.log("Error:", response.status)
        }
      })
  }

  getFormattedDate = (date) => {
    var str = date.getFullYear() + "-" + ('0' + (date.getUTCMonth() + 1)).slice(-2) + "-" + ('0' + date.getUTCDate()).slice(-2) + " " + ('0' + date.getUTCHours()).slice(-2) + ":" + ('0' + date.getUTCMinutes()).slice(-2) + ":" + ('0' + date.getUTCSeconds()).slice(-2);

    return str;
  }

}
